/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert', groups:['youtube','filebrowser','uploadimage'] },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

	config.extraPlugins = 'wordcount';
    
    config.wordcount = {
    
        // Whether or not you want to show the Word Count
        showWordCount: false,
    
        // Whether or not you want to show the Char Count
        showCharCount: true,
    
        // Whether or not to include Html chars in the Char Count
        countHTML: false,
        
        charLimit: 10
    };


	config.toolbar = 'TRiGCustom';
    config.forcePasteAsPlainText = true;
    config.forceSimpleAmpersand = true;
    config.resize_enabled = false;
    config.toolbarCanCollapse = false;
    config.scayt_autoStartup = true;
    config.language = 'en';
    // config.uiColor = '#76BC49';
    config.width = '97%';
    config.extraPlugins = 'maximize';
    config.allowedContent = true;

    config.removeButtons = 'Underline,Subscript,Superscript';
	config.extraPlugins = "youtube,filebrowser,uploadimage";
	// config.extraPlugins = 'filebrowser';

	config.format_tags = 'p;h1;h2;h3;pre';

	config.filebrowserBrowseUrl = '/salon-laravel-admin/public/ckeditor/ckfinder/ckfinder.html',
	config.filebrowserUploadUrl = '/salon-laravel-admin/public/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'

};
CKEDITOR.replace( 'editor2', {
	filebrowserBrowseUrl: '/salon-laravel-admin/public/ckeditor/ckfinder/ckfinder.html',
	filebrowserUploadUrl: '/salon-laravel-admin/public/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'
} );
