/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.toolbarGroups = [
		'/',
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert', groups:['youtube','filebrowser','uploadimage'] },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];
	
	config.toolbar = 'TRiGCustom';
	config.filebrowserBrowseUrl = '/salon-laravel-admin/public/ckeditor/plugins/imageuploader/imgbrowser.php';
	config.filebrowserUploadUrl = '/salon-laravel-admin/public/ckeditor/plugins/imageuploader/imgupload.php';
	config.filebrowserUploadMethod = 'form';

};
CKEDITOR.replace( 'editor1', {
  extraPlugins: 'imageuploader'
});
CKEDITOR.replace( 'editor2', {
  extraPlugins: 'imageuploader'
});

