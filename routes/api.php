<?php
use Illuminate\Http\Request;
Route::get('boards', 'BoardController@index');
Route::get('boards/{board}', 'BoardController@show');
Route::get('google_places', 'googlepacesController@index');
Route::post('boards', 'BoardController@store');
Route::get('google_places/{id}/{id2}', 'googlepacesController@getPlaces');
Route::put('boards/{board}', 'BoardController@update');
Route::delete('boards/{board}', 'BoardController@delete');
Route::get('addboards', 'AddBoardController@index');