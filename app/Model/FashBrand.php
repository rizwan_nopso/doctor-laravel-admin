<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FashBrand extends Model
{
    //
    protected $table = 'fash_brands';
    protected $primaryKey = 'fb_id';

    public function FashBrandCollection(){
    	return $this->hasMany('App\Model\FashBrandCollection','fb_id')->where('fbc_status',1);
    }
}
