<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\serviceCategory;

class serviceSubCategory extends Model
{
    //
    protected $table = 'service_sub_categories';
    protected $primaryKey = 'ssc_id';


    public function service_categories(){
        return $this->belongsTo('App\Model\serviceCategory','sc_id')->where('sc_status',1);
    }
    
}
