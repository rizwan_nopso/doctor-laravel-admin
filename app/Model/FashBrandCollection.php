<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FashBrandCollection extends Model
{
    //
	protected $table = 'fash_brand_collections';
    protected $primaryKey = 'fbc_id';

    public function FashBrand(){
    	$this->belongsTo('App\Model\FashBrand','fb_id');
    }
}
