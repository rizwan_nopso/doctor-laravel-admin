<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Salon extends Model
{
    protected $fillable = [
        'sal_name','sal_email','sal_phone','sal_city',
        'sal_address','sal_zip','sal_specialty','sal_facebook',
        'sal_instagram','sal_24clock','sal_website','sal_biography',
    ];
}
