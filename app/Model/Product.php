<?php

namespace App\Model;
use App\Model\ProductImage;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'products';
    protected $primaryKey = 'p_id';
    protected $guarded = [];
    public $timestamps = false;

    public function product_images(){
    	return $this->hasMany('App\Model\ProductImage','p_id');
    }
}
