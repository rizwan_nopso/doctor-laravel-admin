<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductBrand extends Model
{
    //

    protected $table = 'product_brands';
    protected $primaryKey = 'pb_id';

    public function productVendor(){
    	return $this->belongsTo('App\ProductVendor','pv_id');
    }
}
