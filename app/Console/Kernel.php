<?php
namespace App\Console;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
class Kernel extends ConsoleKernel
{
    ///https://scotch.io/@Kidalikevin/how-to-set-up-cron-job-in-laravel
    
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
         '\App\Console\Commands\InsertBoardsAuto',
         '\App\Console\Commands\UpdateAppData',
    ];
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('InsertBoardsAuto:insertPins')
                 ->hourly();

        $schedule->command('UpdateAppData:updateAppData')
                 ->everyThirtyMinutes();
        //  echo "hello";
    }
    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}