<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class InsertBoardsAuto extends Command
{
    //https://scotch.io/@Kidalikevin/how-to-set-up-cron-job-in-laravel
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'InsertBoardsAuto:insertPins';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert Into pins';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app('App\Http\Controllers\BoardController')->addPins();
        // app('App\Http\Controllers\AdminController')->update_app_data();
    }
}
