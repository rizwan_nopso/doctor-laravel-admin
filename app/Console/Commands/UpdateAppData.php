<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdateAppData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateAppData:updateAppData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update App Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        app('App\Http\Controllers\AdminController')->update_app_data();

        $this->info('Update App  Data Successfully!');
    }
}
