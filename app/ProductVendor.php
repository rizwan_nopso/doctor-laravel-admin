<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVendor extends Model
{
    //
    protected $table = 'product_vendors';
    protected $primaryKey = 'pv_id';

    public function productBrand(){
    	return $this->hasMany('App\ProductBrand','pv_id');
    }
}
