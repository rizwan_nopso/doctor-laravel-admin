<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class BoardController extends Controller {

    public function index() {
        $this->addPins();
    }

    public function addPins() {

        $datetime = date("Y-m-d H:i:s");
        DB::table('debug')->insert(
                ['datetime' => $datetime, 'log_msg' => 'Going to invoke cron job']);

        $cronLog = "";

        //$pinterest_boards = DB::select("SELECT * FROM pinterest_boards where pb_status=1 AND pb_id > (select pb_id from pinterest_pins order by pp_id desc limit 0,1) order by pb_id asc limit 0,3");

        $pinterest_boards = DB::select("SELECT * FROM pinterest_boards where pb_status=1 order by last_fetch_date asc limit 0,3");
        if (count($pinterest_boards) == 0) {
            //  dd('Its here');
            $pinterest_boards = DB::select("SELECT * FROM pinterest_boards WHERE pb_status=1 order by pb_id asc limit 0,3;");
        }




        //  dd($pinterest_boards);
        //echo "Created date is " .
        $inserted_date = date("Y-m-d h:i:sa");
        //dd('Ruk ja', $inserted_date);   
        //      dd($pinterest_boards);
        // dd($pinterest_boards);
        //this will run 3 time at max becuase we have set limit to 3
        //for cron job logs
        $p_b_pins = 0;
        $p_b_log = "";
        $pb_name = "";
        $pb_id = 0;
        $posts_count = 0;
        $bordsAll = "";
        $pinsAll = "";
        //end here
      //  dd($pinterest_boards);

        foreach ($pinterest_boards as $bords) {
            
            if ($p_b_pins != 0) {
                $bordsAll = $bordsAll . ", " . $pb_name;
                $pinsAll = $pinsAll . ", " . $p_b_pins;
                //  dd('if');
                //add data to api_log....
            } else {
                //dd('Else');
            }

            $pp_date_max = DB::select("SELECT max(pp_datetime) as pp_datetime FROM `pinterest_pins` WHERE pb_id =" . $bords->pb_id);
            $date_max_pp_datetime = $pp_date_max[0]->pp_datetime;




            //f
            //   dd($p_b_pins_to_insert);
            $inserted_date = date("Y-m-d h:i:sa");
            $p_b_pins = 0;
            $p_b_log = "";
            $pb_name = $bords->pb_name;
            $pb_id = $bords->pb_id;
            // dd($pb_name,$pb_id);

            for ($i = 0; $i < 3; $i++) {
                //fetch latest pin_board_url so that if it is updated during last time, we should have latest pin_url
                // echo '..time...';
                $board_pin_url = DB::table('pinterest_boards')
                        ->where('pb_id', $pb_id)
                        ->select('borad_pin_url')
                        ->get();
                foreach ($board_pin_url as $pin) {
                    $pinUrlDb = $pin->borad_pin_url;
                }
                if ($i > 0) {
                    if (empty($pinUrlDb)) {
                        echo 'going to break' . $bords->pb_id;
                        dd('break inner');
                        break;
                    }
                }
                //there are plenty of things to do yet,
                // dd('Hello', $bords->borad_pin_url);
                //  dd('here',$i); 
                $p_board_id = $bords->p_board_id;
                if ($i == 0) {
                
                    $url_board_pin = "https://api.pinterest.com/v1/boards/" . $p_board_id . "/pins/?access_token=AVUChBlf-sUV1v5qZqQvGu6_xtb4FSxiwd0Pz21E6NycygAsVwAAAAA&fields=id%2Clink%2Cnote%2Curl%2Cattribution%2Ccolor%2Ccounts%2Cboard%2Ccreated_at%2Ccreator%2Cimage%2Cmedia%2Cmetadata%2Coriginal_link";
                  //  dd($url_board_pin);
                    
                } else {
                    $url_board_pin = $pinUrlDb;
                }
                try {
                   
                    $response = file_get_contents($url_board_pin);
                     echo ''.$response;
                     
                    $datetime = date("Y-m-d H:i:s");
                } catch (\Exception $e) {
                    // $datetime=getDateAndTime();
                    $datetime = date("Y-m-d H:i:s");
                    DB::table('debug')->insert(
                            ['datetime' => $datetime, 'log_msg' => 'pinterest, problem in fetching records, It seems rate limit exceeded or network is not working']);
                    //   dd('Problem in fetching records, It seems rate limit exceeded or network is not working');

                    break;
                }
                // dd('Bus');
                // echo 'response: ' . $response;
                $apiData = json_decode($response, true);
                $page = $apiData['page'];
                $pins = $apiData['data'];
                // dd($apiData);
                //dd('lol', $apiData);
                //check if page exist

                if (isset($apiData['page'])) {
                    //  $currentPage=$apiData['page'];
                    //check if next page exist
                    if (isset($page['next'])) {
                        //dd('again set');
                        $Nextpage = $page['next'];
                        //  dd($currentPage);
                    } else {
                        $Nextpage = "";
                    }
                } else {
                    $Nextpage = "";
                }


                $result = DB::table('pinterest_boards')
                        ->where('pb_id', $pb_id)
                        ->update(['borad_pin_url' => $Nextpage, 'last_fetch_date' => $inserted_date]);
                //  dd($result,$pb_id);



                foreach ($pins as $pin) {
                    //dd($pins);
                    $pp_creator_id = $pin['creator']['id'];
                    $pp_datetime = $pin['created_at']; //check

                    $p_url = $pin['url'];
                    $p_link = $pin['original_link'];
                    $p_imageUrl = $pin['image']['original']['url'];
                    $p_pin_id = $pin['id'];
                    //   $p_metadata = $pin['metadata']['link']['description'];
                    $description = "";
                    $site_name = "";
                    if (isset($pin['metadata']['link'])) {
                        $description = $pin['metadata']['link']['description'];
                        $site_name = $pin['metadata']['link']['site_name'];
                    }
                    $pp_datetime = str_replace("T", " ", $pp_datetime);
                    $p_metadata = $description . $site_name;
                    $p_metadata = "";
                    
                       try {
                        if (!$date_max_pp_datetime == null) {
                            if (  $date_max_pp_datetime <= $pp_datetime) {
                                $datetime = date("Y-m-d H:i:s");
                                DB::table('debug')->insert(
                                        ['datetime' => $datetime, 'log_msg' => 'Date is exceeded api date' . $datetime . ' db date ' . $date_max_pp_datetime]);
                                echo 'its going to break';
                               
                                $result = DB::table('pinterest_boards')
                        ->where('pb_id', $pb_id)
                        ->update(['borad_pin_url' => '', 'last_fetch_date' => $inserted_date]);
                                
// break;
                            }
                        }
                        } catch (\Exception $e) {
                            echo 'exception in date comparisopn';
                        DB::table('debug')->insert(
                                        ['datetime' => $datetime, 'log_msg' => 'Error in date comparisopn']);
                       }

                    //SELECT max(pp_datetime) FROM `pinterest_pins` WHERE pb_id =442
                    //   dd($pb_id);
                    try {

                        $result = DB::statement("INSERT INTO pinterest_pins (pp_creator, pp_datetime, pb_id,  p_url, p_link, p_imageUrl, p_pin_id, last_fetch_date, p_metadata) VALUES"
                                        . " ('" . $pp_creator_id . "' , '" . $pp_datetime . "' , '" . $pb_id . "', '" . $p_url . "', '"
                                        . $p_link . "', '" . $p_imageUrl . "', '" . $p_pin_id . "', '" . $inserted_date . "', '" . $p_metadata . "') 
                                        ON DUPLICATE KEY UPDATE pp_creator = pp_creator ");
                        $posts_count = $posts_count + 1;
                        $p_b_pins = $p_b_pins + 1;
                    } catch (\Exception $e) {
                        $datetime = date("Y-m-d H:i:s");
                        DB::table('debug')->insert(
                                ['datetime' => $datetime, 'log_msg' => 'pinterest, Exception in inserting pin']);

                        //   echo 'Exception in inserting data to db';
                    }
                    // echo ','.$p_pin_id;
                }
                //  dd('fuck!');
                //  dd('Pins update successfully');
                //board pins update
                // dd($pb_id);
                //  dd('Pin inserted', $pb_id);
            }
        }
        //after end of for each get last time updeted value here

        $bordsAll = $bordsAll . ", " . $pb_name;
        $pinsAll = $pinsAll . ", " . $p_b_pins;

        $cron_log = "Boards updated " . $bordsAll . " Pins " . $pinsAll;
        $lastDateTime = date("Y-m-d h:i:sa");
        //  echo ''.$cron_log.' date1 '.$inserted_date.' date last '.$lastDateTime.' t_post'.$posts_count ;
        try {
            $result = DB::table('api_log')->insert(
                    ['start_datetime' => $inserted_date, 'end_datetime' => $lastDateTime, 'posts_count' => $posts_count, 'cron_log' => $cron_log]);
        } catch (\Exception $e) {
            $datetime = date("Y-m-d H:i:s");
            DB::table('debug')->insert(
                    ['datetime' => $datetime, 'log_msg' => 'pinterest, Exception in inserting into api_logs']);
        }

        //  dd($cron_log, $result);
    }

    public function store(Request $request) {
        $board = Board::create($request->all());

        return response()->json($board, 201);
    }

    public function update(Request $request, Board $board) {
        $board->update($request->all());

        return response()->json($board, 200);
    }

    public function delete(Board $baord) {
        $baord->delete();

        return response()->json(null, 204);
    }

}
