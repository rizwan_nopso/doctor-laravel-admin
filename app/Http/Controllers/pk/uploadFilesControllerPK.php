<?php

namespace App\Http\Controllers\pk;

use Illuminate\Support\Facades\Session;
use App\Model\salon_services_sa_import;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use App\Model\serviceCategory;
use App\app_requests_action;
use App\Helpers\Helper;
use Carbon\Carbon;
use DateTime;
use Config;
use Auth;
use App;
use DB;
use File;


class uploadFilesControllerPK extends Controller
{
    //

    public function __construct()
    {
            ini_set('memory_limit','512M');
            $this->middleware('auth');
            $dbc = new Helper();
            $dbc->setDBConnection();    
    }

    public function setUpSalon($id) 
    {
        $fixedSlots = "157&168,157&168,157&168,157&168,157&168,157&168,157&168";  //for tech_break_slots and tech_break_time 
        $salon = DB::table('salon')
                        ->where('sal_id', '=', $id)
                        ->get()[0];
        // dd($salon);
        $sal_hours = $salon->sal_hours;
        $isValidHours = $this->is_valid_weekly_time($sal_hours);
        // dd($isValidHours);
        //for technician table set these values 
        $tech_slots = ""; //searilized array of slots
        $tech_break_slots = $fixedSlots;
// dd($tech_break_slots);
        $tech_work_time = $salon->sal_hours;
        $tech_break_time = $fixedSlots;
        $tech_slots_new = $fixedSlots;
        $tech_weekly_offs = $salon->sal_weekly_offs; //same as salon weekly off;
        $tech_specialty = $salon->sal_specialty;
        $tech_work_slots = "";
        //same as salon specialty;

        if ($isValidHours) {
            // dd('Valid salHours');
            $slots = $this->weekly_time_ranges_to_slots($sal_hours);
            $tech_work_slots = $slots;
            $rangeArray = $this->weekly_ranges_to_array($slots, $fixedSlots);
            $tech_slots = serialize($rangeArray);
            // dd($slots,$fixedSlots, $tech_slots);
            // dd($serializeValeu,'Here');
        } 
        else 
        {
            $notification = array(
                'success' => 'Salon opening hours not valid!', 
                'alert-type' => 'success'
            );
            return back()->with($notification);
            return view('salon-magmt/managesalons', ['salons' => $salons, 'message' => 0, 'messageInfo' => 'Invalid salon hours']);
            //go back to salon and show message there invalid salon hours 
        }
        //insert all above variable to technicans table and then continue 
        //check all the return statement in else block 
        $salon_services = DB::table('salon_services')
                ->where('sal_id', '=', $id)
                ->where('sser_rate', '>', 0)
                // ->where('sser_time', '>', 0)
                // ->where('sser_enabled', '=', 1)
                ->get();
        // dd($salon_services);
        $total_salon_services = count($salon_services);
        //sser_enabled
        //
        // dd($salon_services);
        //check number of service 
        // dd(count($salon_services));
        if (count($salon_services) < 1) 
        {
            // dd('There is no service exist with that salon');
            //return from here 
            $salons = DB::select('select sal_name, sal_temp_enabled, sal_is_reviewed, sal_email, sal_city, sal_created_datetime, (SELECT count(*) from appointments a where a.sal_id = s.sal_id) as appointments, sal_status, sal_id from salon s LIMIT 4 ');
            // dd($salons);
            return back();
            // return view('salon-magmt/managesalons', ['salons' => $salons, 'message' => 0, 'messageInfo' => 'There are no services exist for this salon']);
        }
        //check if sal_hours n salon table they must be valid weekly hours,
        //insert into technicians if there is no tech
        // dd(' tech', $technicians);
        $result = DB::select('select * from tech_services where tech_id in (select tech_id from technicians WHERE sal_id= ' . $id . ' )');
        $totalTechServies = count($result);
        $techId = '';
        $technicians = DB::table('technicians')
                ->where('sal_id', '=', $id)
                ->where('tech_is_active', '=', 1)
                ->get();


        $InsertedTechIdId = 0;
        if (count($technicians) < 1) 
        {

            // dd('There is no tech exist');
            //here insert into tech table 
            $InsertedTechIdId = DB::table('technicians')->insertGetId(
                    array('tech_name' => 'Manager', 'sal_id' => $id, 'tech_pic' => '', 'tech_pic' => 'default.png'
                        , 'tech_slots' => $tech_slots, 'tech_phone' => '',
                        'tech_bio' => '',
                        'tech_specialty' => $tech_specialty, 'tech_weekly_offs' => $tech_weekly_offs,
                        'tech_work_slots' => $tech_work_slots, 'tech_break_slots' => $tech_break_slots, 'tech_work_time' => $tech_work_time
                        , 'tech_break_time' => $tech_break_time
                        , 'tech_slots_new' => $tech_slots_new, 'tech_is_active' => 1
            ));
            // dd($InsertedTechIdId, 'Last id');   
            $techId = $InsertedTechIdId;
        }
        else 
        {
            $techId = $technicians[0]->tech_id;
            $InsertedTechIdId = $techId;
            // dd(' tech exist', $techId);
        }

        if ($totalTechServies < 1) 
        {
            $result1 = DB::statement("INSERT INTO tech_services (tech_id,tser_name, tser_order, sser_id) 
             SELECT " . $InsertedTechIdId . ", sser_name, sser_order, sser_id FROM salon_services where sal_id=  " . $id . ""
                            . " and sser_rate > 0 and sser_time > 0 and sser_enabled = 1 ");
        }
        //salon info here 
        $sal_temp_enabled = DB::table("salon")->where("sal_id",$id)->first()->sal_temp_enabled;
        if($sal_temp_enabled===0)
        {
            $result = DB::table('salon')
                    ->Where('sal_id', $id)
                    ->update([
                        'sal_status'                => 1, 'sal_temp_enabled' => 1,
                        'sal_appointment_interval'  => 30,
                        'sal_modify_datetime'       => Carbon::now()->toDateTimeString()
            ]);
            $notification = array(
                'success' => 'Salon temp-enabled successfully!', 
                'alert-type' => 'success'
            );
        }
     
        $salons = DB::select('select sal_name, sal_temp_enabled,  sal_is_reviewed, sal_email, sal_city, sal_created_datetime, (SELECT count(*) from appointments a where a.sal_id = s.sal_id) as appointments, sal_status, sal_id from salon s LIMIT 40 ');
        
        $notification = array(
                'success' => 'Salon temp-enabled successfully!', 
                'alert-type' => 'success'
            );

        return back()->with($notification);
        return view('salon-magmt/managesalons',['salons' => $salons, 'message' => 1, 'messageInfo' => 'Salon updated successfully!'])->with($notification);
    }

    //save file salon services
    public function save_file_services(Request $request) 
    {
        // header('Content-Type: text/html; charset=UTF-8');

        $saldata = '';
        $linenum = '';

        $page_title = " File Upload ";
        $cat_data = array();
        $cat_data['sc_name'] = $request->input('cat_name');
        //cat_image=salon_service_file
        if (Input::hasFile('cat_image')) 
        {
            
            $cat_data['sc_image'] = time() . '.' . $request->cat_image->getClientOriginalExtension();
            $request->cat_image->move(public_path('category_images'), $cat_data['sc_image']);
            // dd($cat_data['sc_image']);
        } 
        else 
        {
            $cat_data['sc_image'] = '';
        }
        // dd($cat_data['sc_image']);
        $file_name = $cat_data['sc_image'];
        define('CSV_PATH', 'category_images/');
        $csv_file2 = CSV_PATH . $file_name;

        DB::statement("DELETE FROM salon_services_sa_import");

        $pdo = DB::connection()->getPdo();
        $exfile = $pdo->exec("LOAD DATA LOCAL INFILE '".$csv_file2."' INTO TABLE salon_services_sa_import FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n'");

        DB::statement("UPDATE salon_services_sa_import set 
            sser_rate = REPLACE(sser_rate,'£',''), sser_name = substring(sser_name,1,245)");
        
            $result = DB::statement("UPDATE `salon_services_sa_import` sss INNER JOIN services s on s.ser_name = substring(sss.sser_name,1,245) SET sss.ser_id = s.ser_id WHERE sss.ser_id = 0");
            // dd($result);
            //all salon_services_sa_import with zero id insert into service and again update salon_services_sa_import
            $result1 = DB::statement("INSERT INTO services (ser_name)
             SELECT DISTINCT substring(sser_name,1,245) FROM salon_services_sa_import as s_imp WHERE ser_id=0");
           // dd($result1, 'till now as');
            $results = DB::statement("UPDATE `salon_services_sa_import` sss INNER JOIN services s on s.ser_name = sss.sser_name SET sss.ser_id = s.ser_id WHERE sss.ser_id = 0");
            // dd("services table ");
            $result = DB::statement("UPDATE `salon_services_sa_import` sss INNER JOIN salon_ser_categories s on s.ssc_name = sss.ssc_name and s.sal_id = sss.sal_id SET sss.ssc_id = s.ssc_id WHERE sss.ssc_id = 0");
            // dd("salon_service category");
            //all salon_services_sa_import with zero id insert into service and again update salon_services_sa_import
            $result1 = DB::statement("INSERT INTO salon_ser_categories (ssc_name, sal_id)
             SELECT  distinct ssc_name, sal_id FROM salon_services_sa_import as s_imp WHERE ssc_id=0 ");

            $result = DB::statement("UPDATE `salon_services_sa_import` sss INNER JOIN salon_ser_categories s on s.ssc_name = sss.ssc_name and s.sal_id = sss.sal_id SET sss.ssc_id = s.ssc_id WHERE sss.ssc_id = 0");

       
                $result1 = DB::statement("INSERT INTO salon_services (ser_id,sser_name, sal_id, sser_rate, sser_time, ssc_id)
                SELECT ser_id, substring(sser_name,1,245), sal_id,sser_rate, sser_time, ssc_id FROM salon_services_sa_import  WHERE ser_id<>0
                ON DUPLICATE KEY UPDATE salon_services.ssc_id = salon_services.ssc_id");

                $salonasiport = DB::table("salon_services_sa_import")->DISTINCT('sal_id','sser_id')->get();

            $i = 0;
            $pres_id = 0;

            $salids  = '';
            $sserid = '';
            foreach($salonasiport as $salon_s)
            {   
                $salids  = $salids.','.$salon_s->sal_id;
                $sserid = $sserid.','.$salon_s->sser_id;
                if($salon_s->sal_id != $pres_id)
                {
                    $pres_id = $salon_s->sal_id;
                    $tech = $this->recal_setUpSalon($salon_s->sal_id);

                    if($tech == 'false')
                    {   
                        $i += 1; 
                        $saldata = $saldata.','.$salon_s->sal_id;
                        $linenum = $linenum.','.$i;
                        Session::flash('service_error_message', 'Some errors please check in salon opening hours ');
                    }
                }

            }

        $user_ids = \Auth::User()->id;

        $results = DB::statement("INSERT INTO salon_data_upload (user_id,type,sal_id,sal_service_id,created_at) VALUES('".$user_ids."','"."salon-service"."','".$salids."','".$sserid."','".\Carbon\Carbon::now()->toDateTimeString()."') ");

        Session::flash('service_flash_message', 'Salon Service successfully added.');
        Session::put('activemenu','save_file_services');   
        return view('service-mgmt/fileupload',compact('page_title','saldata','linenum'));
        //end services import
        //edn save data
        //for saving file to server side
    }
     // read file function
     public function get_hours($hours)
     {
         //     $hours = "TUESDAY 9am to 2pm, WEDNESDAY 9am to 2pm, THURSDAY 9am to 6pm, FRIDAY 9am to 6pm, SATURDAY 9am to 6pm";
         //        $hours1 = "MON 10.00   19.00, TUE 10.00   19.00, WED 10.00   19.00, THU 12.00   19.00, FRI 09.00   20.00, SAT 09.00   20.00";
         //        $hours2 = "Monday 11.00am 8.00pm, Tuesday 11.00am 8.00pm, Wednesday 11.00am 8.00pm, Thursday 11.00am 8.00pm, Friday 11.00am 8.00pm, Saturday 11.00am 8.00pm, Sunday 11.00am 6.00pm";
         //        $hours3 = "Monday 10am 7:30pm, Tuesday 10am 7:30pm, Wednesday 10am 7:30pm, Thursday 10am 7:30pm, Friday 10am 8:30pm, Saturday 9am 7pm, Sunday 11am 6:30pm";
         //        $hours4 = " Monday 8:30AM 7PM, Sunday Closed, Monday 8:30AM 7PM, Wednesday 8:30AM 7PM, Thursday 8:30AM 7PM, Friday 8:30AM 7PM, Saturday 8:30AM 7PM, Sunday Closed, Monday 8:30AM 7PM, Tuesday 8:30AM 7PM";
         //       
                 //$hours_new= explode(",", $hours);
         // dd('At save file', $hours_new);
          $hourss = explode(",", $hours);
         $string_hour = "";
         $hourss_cal="";
         foreach ($hourss as $key => $hour) 
         {
             $trms = preg_replace('!\s+!', ' ', $hour);
                 // dd($trm);
             // here split each value
             $split_val_arr = explode(" ",trim(str_replace(" to "," ", $trms)));
             // dd($split_val_arr);
             if(count($split_val_arr)==3)
             {
                 $withoutDay = array_splice($split_val_arr, 0, 1);
                 //   dd($withoutDay);
                 $string_hour = date("H:i", strtotime($split_val_arr[0])).'&'.date("H:i", strtotime($split_val_arr[1]));
                 // echo $string_hour."<br>";
                 $hourss_cal=$hourss_cal.','.$string_hour;
             }
             elseif(count($split_val_arr)==2)
             {
                 if($split_val_arr[1] == "Closed" OR $split_val_arr[1] == "closed")
                 {
                  $withoutDay = array_splice($split_val_arr, 0, 1);
                  // dd($withoutDay);
                  $string_hour = date("H:i", strtotime($split_val_arr[0])).'&'.date("H:i", strtotime($split_val_arr[0]));    
                  $hourss_cal=$hourss_cal.','.$string_hour;
                 }
                 else
                 {
                   $withoutDay = array_splice($split_val_arr, 0, 1);
                   $string_hour = date("H:i", strtotime($split_val_arr[0])).'&'.date("H:i", strtotime($split_val_arr[0]));
                   $hourss_cal=$hourss_cal.','.$string_hour;
                 }
             }
             else
             {
                 $hourss_cal = '00:00&00:00';
             }
         }
      
         $str2 = substr($hourss_cal,1);
         return $str2;
     }
     // end  read file function


public function save_file(Request $request) 
    {
        $salon_data = '';
        $page_title = "File Upload ";
        //temp
     $cat_data = array();
    $cat_data['sc_name'] = $request->input('cat_name');
    //cat_image=salon_service_file
    //temp
    if (Input::hasFile('cat_image')) 
    {
         // dd('here');
        $path = $request->file('cat_name');
        //dd($path);
        $cat_data['sc_image'] = time() . '.' . $request->cat_image->getClientOriginalExtension();
        $request->cat_image->move(public_path('category_images'), $cat_data['sc_image']);
        // dd($cat_data);
    } 
    else 
    {
        dd('Image not found');
        $cat_data['sc_image'] = '';
    }

// dd("end");
$i = 0;
$salon_linenum = '';
    $file_name = $cat_data['sc_image'];
    define('CSV_PATH', 'category_images/');
    $csv_file = CSV_PATH . $file_name;      

    if(($handle = fopen($csv_file, "r")) !== FALSE)
    {
        $data = DB::table("salon_sa_import")->truncate();
        //fgetcsv($handle);
        if((fgetcsv($handle, 1000, ",")) !== FALSE)
        {
            // dd($data);
            while(($data = fgetcsv($handle, 1000, ",")) !== FALSE)
            {
                //dd($data);
                $num = count($data);
                for($c = 0; $c < $num; $c++) 
                {
                    $col[$c] = $data[$c];
                }
                // dd(count($col));
                if(count($col) == 19 )
                {
                    $sal_id         		= $col[0];
                    $monday         		= $col[1]; 
                    $tuesday        		= $col[2];
                    $wednesday      		= $col[3];
                    $thursday       		= $col[4];
                    $friday         		= $col[5];
                    $saturday       		= $col[6];
                    $sunday         		= $col[7];
                    $sal_phone      		= $col[8];
                    $sal_pic        		= $col[9];
                    $sal_website    		= $col[10];
                    $sal_facebook   		= $col[11];
                    $sal_twitter    		= $col[12];
                    $sal_instagram  		= $col[13];
                    $sal_address    		= $col[14];
                    $sal_email      		= $col[15];
                    $sal_contact_person 	= $col[16];
                    $sal_lat        		= $col[17];
                    $sal_lng       			= $col[18];

                    $sal_hours = str_replace("-", "&", trim($col[1]) . "," . trim($col[2]) . "," . trim($col[3]) . "," . trim($col[4]) . "," . trim($col[5]) . "," . trim($col[6]) . "," . trim($col[7]) );
                    $sal_hours = str_replace('–','&', $sal_hours);
                    if($sal_hours==',,,,,,')
                    {
                        $sal_hours = '';
                    }
                    else
                    {
                        // $getHours       = $this->get_hours($sal_hours);   
                        $isValid_hours = $this->is_valid_weekly_time($sal_hours);
                        if($isValid_hours == false)
                        {
                           $salon_data = $salon_data.','.$sal_id;
                             $i += 1;
                            $salon_linenum = $salon_linenum.','.$i;
                            Session::flash('salon_error_message', 'Some errors please check in salon opening hours ');
                        }
                    }
                    try 
                    {
                        // echo '$sal_hours' . $sal_hours . '--------'."<br>";
                        $result = DB::statement("INSERT INTO salon_sa_import (sal_id, sal_hours, sal_phone,  sal_facebook,  sal_twitter, sal_instagram,sal_pic,monday,tuesday,wednesday,thursday,friday,saturday,sunday,sal_address,sal_email,sal_lat,sal_lng,sal_contact_person) VALUES "
                                        . "('" . $sal_id . "','" . $sal_hours . "','" . $sal_phone . "','" . $sal_facebook . "','" . $sal_twitter . "','" . $sal_instagram . "','".$sal_pic."','".$col[1]."','".$col[2]."','".$col[3]."','".$col[4]."','".$col[5]."','".$col[6]."','".$col[7]."','".$sal_address ."','".$sal_email."','".$sal_lat ."','".$sal_lng ."','".$sal_contact_person ."') ");
                        // echo 'If you see this, the number is 1 or below';
                    }
                    //catch exception
                    catch (Exception $e) {
                        dd('Exception');
                        //  echo 'Message: ' . $e->getMessage();
                    }

                }
                elseif(count($col) == 9)
                {
                    $sal_id         = $col[0];
                    $sal_hours      = $col[1]; 
                    $sal_phone      = $col[2];
                    $sal_facebook   = $col[3];
                    $sal_twitter    = $col[4];
                    $sal_instagram  = $col[5];
                    $sty_id         = $col[6];
                    $sal_pic        = $col[7];
                    $sal_address    = $col[8];
                    $sal_email      = $col[9];

                    $getHours       = $this->get_hours($sal_hours);  
                    // dd($getHours); 
                    $isValid_hours = $this->is_valid_weekly_time($getHours);
                    if($isValid_hours == false)
                    {
                       $salon_data = $salon_data.','.$sal_id;
                         $i += 1;
                        $salon_linenum = $salon_linenum.','.$i;
                        Session::flash('salon_error_message', 'Some errors please check in salon opening hours ');
                    }
                    try 
                    {
                        // echo '$sal_hours' . $sal_hours . '--------'."<br>";
                        $result = DB::statement("INSERT INTO salon_sa_import (sal_id, sal_hours, sal_phone,  sal_facebook,  sal_twitter, sal_instagram, sty_id,sal_pic,sal_address,sal_email) VALUES "
                                        . "('" . $sal_id . "','" . $getHours . "','" . $sal_phone . "','" . $sal_facebook . "','" . $sal_twitter . "','" . $sal_instagram . "','" . $sty_id ."','".$sal_pic."','".$sal_address ."','".$sal_email."') ");
                        // echo 'If you see this, the number is 1 or below';
                    }
                    //catch exception
                    catch (Exception $e) {
                        dd('Exception');
                        //  echo 'Message: ' . $e->getMessage();
                    }
                }
                else
                {
                    Session::flash("error_message","CSV file is not valid, please try again upload the correct CSV file ");
                    return back();
                }
            }
        }
        else
        {
            Session::flash("error_message","CSV file is not valid, please try again upload the correct CSV file ");
                    return back();
        }
        
        fclose($handle);
        }
        else
        {
            Session::flash("error_message","CSV file is not valid, please try again upload the correct CSV file ");
                        return back();
        }
            $userid = \Auth::user()->id;

                $get_salons = DB::select("SELECT sal_id FROM salon_sa_import WHERE salon_sa_import.sal_id NOT IN (SELECT s.sal_id FROM salon s inner join salon_sa_import s1 on s1.sal_id = s.sal_id )");

                // $result = DB::statement("INSERT INTO salon
                // Select * from salon_all sa WHERE sa.sal_id in (SELECT sal_id FROM salon_sa_import WHERE salon_sa_import.sal_id NOT IN (SELECT s.sal_id FROM salon s inner join salon_sa_import s1 on s1.sal_id = s.sal_id ) )");

                DB::statement("UPDATE `salon` s INNER JOIN salon_sa_import ssi on s.sal_id = ssi.sal_id SET s.sal_hours = if(ssi.sal_hours <> '',ssi.sal_hours,s.sal_hours), s.sal_zip = ssi.sty_id, s.sal_modify_datetime = now(), s.sal_appointment_interval = 30, s.sal_pic = ssi.sal_pic, s.sal_email = ssi.sal_email, s.sal_phone = if(ssi.sal_phone <> '',ssi.sal_phone,s.sal_phone) ");
        
            $salonid = DB::table("salon_sa_import")->get();
            $salon_id = '';
            foreach ($salonid as $key => $value) {
                $salon_id = $salon_id.','.$value->sal_id;
            }
            $salonsid = trim($salon_id,",");
            // dd($salonsid);
            $results = DB::statement("INSERT INTO salon_data_upload (user_id,type,sal_id,created_at)
                                        values('".$userid."','"."salon"."','".$salonsid."','".\Carbon\Carbon::now()->toDateTimeString()."')");

            $update_zipcode = DB::statement("update `salon` s inner JOIN postcodelatlng pc on s.sal_zip = pc.postcode set s.sal_lat = pc.latitude, s.sal_lng = pc.longitude where s.sal_lat = '' and s.sal_lng = '' ");

                DB::statement("update salon s JOIN postcodelatlng pc on s.sal_zip = pc.postcode set s.sal_lat = pc.latitude, s.sal_lng = pc.longitude where s.sal_lat = '' or s.sal_lng = '' ");
            

        Session::flash('salon_flash_message', 'Salon details successfully added.');

        Session::put("activemenu","save_file");

        return view('service-mgmt/fileupload',compact('page_title','salon_data','salon_linenum'));
    }


    function is_valid_weekly_time($weekly_time) {
        $error = 0;
        $valid_chars = "0123456789:&,";
        if (strlen($weekly_time) != 83) {
            $error = 1;
        }

        for ($i = 0; $i < strlen($weekly_time) - 1; $i++) {
            if (strpos($valid_chars, substr($weekly_time, $i, 1)) === false) {
                $error = 2;
            }
        }

        $arr_all_days = explode(",", $weekly_time);
        if (sizeof($arr_all_days) != 7) {
            $error = 3;
        }

        for ($i = 0; $i < sizeof($arr_all_days); $i++) {
            if (!$this->is_valid_time_range($arr_all_days[$i])) {
                return false;
            }
        }

        if ($error > 0) {
            return false;
        } else {
            return true;
        }
    }

    function is_valid_time_range($time_range) {
        $error = 0;
        if (strlen($time_range) != 11) 
        {
            $error = 4;
        } 
        else 
        {
            $arr_day = explode("&", $time_range);
            if (sizeof($arr_day) != 2) 
            {
                $error = 5;
            } 
            else 
            {

                if (strlen($arr_day[0]) != 5) 
                {
                    $error = 6;
                } 
                else 
                {
                    if (strlen($arr_day[1]) != 5) 
                    {

                        $error = 7;
                    } 
                    else 
                    {
                        $arr_start_time = explode(":", $arr_day[0]);
                        if (strlen($arr_start_time[0]) != 2 || strlen($arr_start_time[1]) != 2) 
                        {
                            $error = 8;
                        } 
                        else 
                        {

                            $arr_end_time = explode(":", $arr_day[1]);
                            if (strlen($arr_end_time[0]) != 2 || strlen($arr_end_time[1]) != 2) 
                            {
                                $error = 9;
                            }
                        }
                    }
                }
            }
        }

        if ($error > 0) {

            return false;
        } else {

            return true;
        }
    }

    function weekly_time_ranges_to_slots($weekly_time_ranges) 
    {
        if (strpos($weekly_time_ranges, ",") !== false) {
            $weekly_time_ranges = explode(",", $weekly_time_ranges);
            for ($i = 0; $i < sizeof($weekly_time_ranges); $i++) {
                $single_day_time = explode("&", $weekly_time_ranges[$i]);
                $weekly_slot_ranges[$i] = $this->time_to_slot($single_day_time[0], 1) . "&" . $this->time_to_slot($single_day_time[1], 0);
            }
        }
        return implode(",", $weekly_slot_ranges);
    }

    function time_to_slot($time, $time_flg = 1, $slot_duration = 5) {
        if ($time == "00:00" || $time == "" || $time == "0") {
            return "0";
        }
        $time = explode(":", $time);
        $hr = $time[0];
        $min = $time[1];
        $slot = $hr * (60 / $slot_duration);
        $slot += $time_flg;
        $slot += (int) ($min / $slot_duration);
        return $slot;
    }

    function weekly_ranges_to_array($include_range, $exclude_range = "") 
    {
        $include_range = explode(",", $include_range);
        $exclude_range = explode(",", $exclude_range);
        for ($i = 1; $i <= 7; $i++) {
            $include_range_current = explode("&", $include_range[$i - 1]);
            $exclude_range_current = explode("&", $exclude_range[$i - 1]);
            $include_range_current = range($include_range_current[0], $include_range_current[1]);
            $exclude_range_current = range($exclude_range_current[0], $exclude_range_current[1]);
            $result[$i] = array_diff($include_range_current, $exclude_range_current);
        }
        return $result;
    }


    public function temp_salon_file(Request $request)
    {
     
        $salon_data = '';
            $page_title = "File Upload ";
            //temp
         $cat_data = array();
        $cat_data['sc_name'] = $request->input('cat_name');
        //cat_image=salon_service_file
        //temp
        if (Input::hasFile('cat_image')) 
        {
             // dd('here');
            $path = $request->file('cat_name');
            //dd($path);
            $cat_data['sc_image'] = time() . '.' . $request->cat_image->getClientOriginalExtension();
            $request->cat_image->move(public_path('category_images'), $cat_data['sc_image']);
            // dd($cat_data);
        } 
        else 
        {
            dd('Image not found');
            $cat_data['sc_image'] = '';
        }

   // dd("end");
    $i = 0;
    $salon_linenum = '';
        $file_name = $cat_data['sc_image'];
        define('CSV_PATH', 'category_images/');
        $csv_file = CSV_PATH . $file_name;

        if (($handle = fopen($csv_file, "r")) !== FALSE) {
               $data = DB::table("temp_salon_sa_import")->truncate();
            fgetcsv($handle);
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                // dd($data);
                $num = count($data);
                for ($c = 0; $c < $num; $c++) {
                    $col[$c] = $data[$c];
                }
                
                if(count($col) == 21 )
                {
                    $sal_id         		= $col[0];
                    $sal_name       		= $col[1];
                    $monday         		= $col[2]; 
                    $tuesday        		= $col[3];
                    $wednesday      		= $col[4];
                    $thursday       		= $col[5];
                    $friday         		= $col[6];
                    $saturday       		= $col[7];
                    $sunday         		= $col[8];
                    $sal_phone      		= $col[9];
                    $sal_pic        		= $col[10];
                    $sal_website    		= $col[11];
                    $sal_facebook   		= $col[12];
                    $sal_twitter    		= $col[13];
                    $sal_instagram  		= $col[14];
                    $sal_address    		= $col[15];
                    $sal_email      		= $col[16];
                    $sal_contact_person 	= $col[17];
                    $sal_lat        		= $col[18];
                    $sal_lng       			= $col[19];
                    $sty_id                 = $col[20];

                    // dd($col[10]);
                    $sal_hours = str_replace("-", "&", trim($col[2]) . "," . trim($col[3]) . "," . trim($col[4]) . "," . trim($col[5]) . "," . trim($col[6]) . "," . trim($col[7]) . "," . trim($col[8]) );
                    $sal_hours = str_replace('–','&', $sal_hours);
                    // dd($sal_hours);
                    // $getHours       = $this->get_hours($sal_hours);   
                    $isValid_hours = $this->is_valid_weekly_time($sal_hours);
                    if($isValid_hours == false)
                    {
                       $salon_data = $salon_data.','.$sal_id;
                         $i += 1;
                        $salon_linenum = $salon_linenum.','.$i;
                        Session::flash('new_error_message', 'Some errors please check in salon opening hours ');
                    }
                    try 
                    {
                        // echo '$sal_hours' . $sal_hours . '--------'."<br>";
                        $result = DB::statement("INSERT INTO temp_salon_sa_import (temp_sal_id, sal_hours, sal_phone,  sal_facebook,  sal_twitter, sal_instagram,sal_pic,monday,tuesday,wednesday,thursday,friday,saturday,sunday,sal_address,sal_name,sal_email,sal_website,sal_contact_person,sal_lat,sal_lng,sty_id) VALUES "
                                        . "('" . $sal_id . "','" . $sal_hours . "','" . $sal_phone . "','" . $sal_facebook . "','" . $sal_twitter . "','" . $sal_instagram . "','".$col[10]."','".$col[2]."','".$col[3]."','".$col[4]."','".$col[5]."','".$col[6]."','".$col[7]."','".$col[8]."','".$sal_address."','".$sal_name."','".$sal_email."','".$sal_website."','".$sal_contact_person."','".$sal_lat."','".$sal_lng."','".$sty_id."')");
                        // echo 'If you see this, the number is 1 or below';
                       
                    }
                    //catch exception
                    catch (Exception $e) {
                        dd('Exception');
                        //  echo 'Message: ' . $e->getMessage();
                    }

                }
                elseif(count($col) == 9)
                {
                    $sal_id         = $col[0];
                    $sal_hours      = $col[1]; 
                    $sal_phone      = $col[2];
                    $sal_facebook   = $col[3];
                    $sal_twitter    = $col[4];
                    $sal_instagram  = $col[5];
                    $sty_id         = $col[6];
                    $sal_pic        = $col[7];
                    $sal_address    = $col[8];

                    $getHours       = $this->get_hours($sal_hours);  
                    // dd($getHours); 
                    $isValid_hours = $this->is_valid_weekly_time($getHours);
                    if($isValid_hours == false)
                    {
                       $salon_data = $salon_data.','.$sal_id;
                         $i += 1;
                        $salon_linenum = $salon_linenum.','.$i;
                        Session::flash('new_salon_error_message', 'Some errors please check in salon opening hours ');
                    }
                    try 
                    {
                        // echo '$sal_hours' . $sal_hours . '--------'."<br>";
                        $result = DB::statement("INSERT INTO temp_salon_sa_import (temp_sal_id, sal_hours, sal_phone,  sal_facebook,  sal_twitter, sal_instagram, sty_id,sal_pic,sal_address,sal_name,sal_city,sal_email) VALUES "
                                        . "('" . $sal_id . "','" . $getHours . "','" . $sal_phone . "','" . $sal_facebook . "','" . $sal_twitter . "','" . $sal_instagram . "','" . $sty_id ."','".$sal_pic."','".$sal_address ."','".$col[16]."','".$col[17]."','".$col[18]."') ");
                        // echo 'If you see this, the number is 1 or below';
                    }
                    //catch exception
                    catch (Exception $e) {
                        dd('Exception');
                        //  echo 'Message: ' . $e->getMessage();
                    }
                }
                else
                {
                    Session::flash("new_error_message","CSV file is not valid, please try again upload the correct CSV file ");
                    return back();
                }
            }
             
            fclose($handle);
        }
            $userid = \Auth::user()->id;
        

            DB::statement(" UPDATE salon as s INNER JOIN temp_salon_sa_import as tsai on tsai.temp_sal_id = s.temp_sal_id SET 
            		tsai.sal_hours=s.sal_hours,tsai.temp_sal_id=s.temp_sal_id,tsai.sal_facebook=s.sal_facebook,tsai.sal_instagram=s.sal_instagram,tsai.sal_twitter=s.sal_twitter,tsai.sal_address=s.sal_address,tsai.sal_phone=s.sal_phone,tsai.sal_name=s.sal_name,tsai.sal_email=s.sal_email,tsai.sal_lat=s.sal_lat,tsai.sal_lng=s.sal_lng,tsai.sal_website=s.sal_website,tsai.sal_contact_person=s.sal_contact_person ");

       $temp =  DB::statement(" INSERT INTO salon(sal_hours,temp_sal_id, sal_facebook, sal_instagram, sal_zip, sal_twitter, sal_address, sal_pic, sal_modify_datetime, sal_appointment_interval,sal_phone,sal_name,sal_city,sal_email,sal_profile_pic,sal_created_datetime,sal_status,admin_device_id,admin_device_type,sal_lat,sal_lng,sal_timing,sal_nearby_zip,sal_search_words,sal_biography,sal_weekly_offs,sty_id,sal_services,sal_review_datetime,sal_website,forgot_pass_identity,sal_contact_person) SELECT sal_hours,temp_sal_id, sal_facebook, sal_instagram,'', sal_twitter, sal_address,sal_pic,'".\Carbon\Carbon::now()->toDateTimeString()."','30',sal_phone,sal_name,'',sal_email,'','".\Carbon\Carbon::now()->toDateTimeString()."',0,'','',sal_lat,sal_lng,'','','','','',sty_id,'','".\Carbon\Carbon::now()->toDateTimeString()."',sal_website,'',sal_contact_person from temp_salon_sa_import as tmps  ON DUPLICATE KEY UPDATE salon.temp_sal_id = tmps.temp_sal_id,
            salon.sty_id = tmps.sty_id, salon.sal_pic = tmps.sal_pic ");

           
// dd("ok");
       
            $salonid = DB::table("salon_sa_import")->get();
            $salon_id = '';
            foreach ($salonid as $key => $value) {
                $salon_id = $salon_id.','.$value->sal_id;
            }
            $salonsid = trim($salon_id,",");
            // dd($salonsid);
            $results = DB::statement("INSERT INTO salon_data_upload (user_id,type,sal_id,created_at)
                                        values('".$userid."','"."salon"."','".$salonsid."','".\Carbon\Carbon::now()->toDateTimeString()."')");
            // DB::statement("update salon s JOIN postcodelatlng pc on s.sal_zip = pc.postcode set s.sal_lat = pc.latitude, s.sal_lng = pc.longitude where s.sal_lat = '' or s.sal_lng = '' ");

           Session::flash('new_salon_flash_message', 'Salon details successfully added.');

        Session::put("activemenu","temp_save_file");
        // return back();

        return view('service-mgmt/fileupload',compact('page_title','salon_data','salon_linenum'));
    }
    public function tem_salon_service_file(Request $request)
    {
        // header('Content-Type: text/html; charset=UTF-8');
        // dd("services ");

        $saldata = '';
        $linenum = '';

        $page_title = " File Upload ";
        $cat_data = array();
        $cat_data['sc_name'] = $request->input('cat_name');
        //cat_image=salon_service_file
        if (Input::hasFile('cat_image')) 
        {
            $cat_data['sc_image'] = time() . '.' . $request->cat_image->getClientOriginalExtension();
            $request->cat_image->move(public_path('category_images'), $cat_data['sc_image']);
            // dd($cat_data['sc_image']);
        } 
        else 
        {
            $cat_data['sc_image'] = '';
        }
        // dd($cat_data['sc_image']);
        $file_name = $cat_data['sc_image'];
        define('CSV_PATH', 'category_images/');
        $csv_file2 = CSV_PATH . $file_name;

        DB::statement("DELETE FROM temp_salon_service_sa_import");

        $pdo = DB::connection()->getPdo();
        $exfile = $pdo->exec("LOAD DATA LOCAL INFILE '".$csv_file2."' INTO TABLE temp_salon_service_sa_import FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n'");
    
        DB::statement("UPDATE temp_salon_service_sa_import set 
            sser_rate = REPLACE(sser_rate,'£',''), sser_name = substring(sser_name,1,245)");
        

            $result = DB::statement("UPDATE `temp_salon_service_sa_import` sss INNER JOIN services s on s.ser_name = substring(sss.sser_name,1,245) SET sss.ser_id = s.ser_id WHERE sss.ser_id = 0");

            // dd($result);
            //all salon_services_sa_import with zero id insert into service and again update salon_services_sa_import
            $result1 = DB::statement("INSERT INTO services (ser_name)
             SELECT DISTINCT substring(sser_name,1,245) FROM temp_salon_service_sa_import ON DUPLICATE KEY UPDATE services.ser_id = services.ser_id");
           // dd($result1, 'till now as');
            $results = DB::statement("UPDATE `temp_salon_service_sa_import` sss INNER JOIN services s on s.ser_name = sss.sser_name SET sss.ser_id = s.ser_id WHERE sss.ser_id = 0");
            // dd("services table ");
            
            // dd("salon_service category");
            //all salon_services_sa_import with zero id insert into service and again update salon_services_sa_import

            $result = DB::statement("UPDATE `temp_salon_service_sa_import` sss INNER JOIN salon s on s.temp_sal_id = sss.temp_sal_id SET sss.sal_id = s.sal_id");
            // dd($result);

            $result1 = DB::statement("INSERT INTO salon_ser_categories (ssc_name, sal_id)
             SELECT  distinct ssc_name, sal_id FROM temp_salon_service_sa_import as s_imp WHERE ssc_id=0 ON DUPLICATE KEY UPDATE salon_ser_categories.ssc_name = salon_ser_categories.ssc_name ");
           
           $result = DB::statement("UPDATE `temp_salon_service_sa_import` sss INNER JOIN salon_ser_categories s on s.ssc_name = sss.ssc_name and s.sal_id = sss.sal_id SET sss.ssc_id = s.ssc_id WHERE sss.ssc_id = 0");
       // dd($result1);
        $result1 = DB::statement("INSERT INTO salon_services (ser_id,sser_name,sal_id, sser_rate, sser_time, ssc_id)
        SELECT ser_id, substring(sser_name,1,245),sal_id,sser_rate, sser_time, ssc_id FROM temp_salon_service_sa_import as tssi  WHERE tssi.ser_id<>0
                ON DUPLICATE KEY UPDATE salon_services.ssc_id = tssi.ssc_id, salon_services.sser_rate = tssi.sser_rate, salon_services.sser_time = tssi.sser_time ");

        $salonasiport = DB::table("temp_salon_service_sa_import")->DISTINCT('sal_id','sser_id')->get();

       $i = 0;
    $pres_id = 0;

    $salids  = '';
    $sserid = '';
        foreach($salonasiport as $salon_s)
        { 

            $salids  = $salids.','.$salon_s->sal_id;
            $sserid = $sserid.','.$salon_s->sser_id;

            if($salon_s->sal_id != $pres_id)
            {
                $pres_id = $salon_s->sal_id;
                $tech = $this->recal_setUpSalon($salon_s->sal_id);
                
                if($tech == 'false')
                {   
                    $i += 1; 
                    $saldata = $saldata.','.$salon_s->sal_id;
                    $linenum = $linenum.','.$i;
                    Session::flash('new_service_error_message', 'Some errors please check in salon opening hours ');
                }
            }

        }

        $user_ids = \Auth::User()->id;

        $results = DB::statement("INSERT INTO salon_data_upload (user_id,type,sal_id,sal_service_id,created_at) VALUES('".$user_ids."','"."salon-service"."','".$salids."','".$sserid."','".\Carbon\Carbon::now()->toDateTimeString()."') ");

        Session::flash('new_service_flash_message', 'Salon Service successfully added.');
        Session::put('activemenu','temp_save_file_services');   
        return view('service-mgmt/fileupload',compact('page_title','saldata','linenum'));
        //end services import
        //edn save data
        //for saving file to server side
    }

     // recal setup salons import excel files
    public function recal_setUpSalon($id) 
    {
        global $i;
        $fixedSlots = "157&168,157&168,157&168,157&168,157&168,157&168,157&168";  //for tech_break_slots and tech_break_time 
        $salon = DB::table('salon')->where('sal_id', '=', $id)
                        ->first();
            
        $sal_hours = $salon->sal_hours;
        
        $isValidHours = $this->is_valid_weekly_time($sal_hours);

        $tech_slots = ""; //searilized array of slots
        $tech_break_slots = $fixedSlots;
        $tech_work_time = $salon->sal_hours;
        $tech_break_time = $fixedSlots;
        $tech_slots_new = $fixedSlots;
        $tech_weekly_offs = $salon->sal_weekly_offs; //same as salon weekly off;
        $tech_specialty = $salon->sal_specialty;
        if(empty($tech_specialty))
        {
            $tech_specialty = "";
        }
        $tech_work_slots = "";
        //same as salon specialty;
        if ($isValidHours) 
        {
            // echo "(if condition here )";
            $slots = $this->weekly_time_ranges_to_slots($sal_hours);
            $tech_work_slots = $slots;
            $rangeArray = $this->weekly_ranges_to_array($slots, $fixedSlots);
            $tech_slots = serialize($rangeArray);

            // dd($tech_work_slots);
            // dd("in if condition here");
        } 
        else 
        {
            
            return 'false';   
        }
      
        $salon_services = DB::table('salon_services')
                ->where('sal_id', '=', $id)
                ->where('sser_rate', '>', 0)
                // ->where('sser_time', '>', 0)
                // ->where('sser_enabled', '=', 1)
                ->get();
        $total_salon_services = count($salon_services);
        // dd($total_salon_services);
        if (count($salon_services) < 1) 
        {
            return true;
        }
        //check if sal_hours n salon table they must be valid weekly hours,
        //insert into technicians if there is no tech
        // dd(' tech', $technicians);
        $result = DB::select('select * from tech_services where tech_id in (select tech_id from technicians WHERE sal_id= ' . $id . ' )');
        $totalTechServies = count($result);
        $techId = '';
        $technicians = DB::table('technicians')
                ->where('tech_name','Manager')
                ->where('sal_id', '=', $id)
                ->where('tech_is_active', '=', 1)
                ->get();
        

        $InsertedTechIdId = 0;
        if(count($technicians) < 1) 
        {
            // dd('There is no tech exist');
            //here insert into tech table 
            $InsertedTechIdId = DB::table('technicians')->insertGetId(
                    array('tech_name' => 'Manager', 'sal_id' => $id, 'tech_pic' => '', 'tech_pic' => 'default.png'
                        , 'tech_slots' => $tech_slots, 'tech_phone' => '',
                        'tech_bio' => '',
                        'tech_specialty' => $tech_specialty, 'tech_weekly_offs' => $tech_weekly_offs,
                        'tech_work_slots' => $tech_work_slots, 'tech_break_slots' => $tech_break_slots, 'tech_work_time' => $tech_work_time
                        , 'tech_break_time' => $tech_break_time
                        , 'tech_slots_new' => $tech_slots_new, 'tech_is_active' => 1
            ));

            // dd('technicians ',$InsertedTechIdId);
            $techId = $InsertedTechIdId;
        }
        else 
        {
            $techId = $technicians[0]->tech_id;
            // dd($id);
            // dd("work slot",$tech_work_slots," break slot",$tech_break_slots);
            $updat = DB::table("technicians")->where("tech_name","Manager")
                                             ->where("sal_id",$id)
                                             ->update(array(
                                                'tech_work_slots'   => $tech_work_slots,
                                                'tech_break_slots'  => $tech_break_slots,

                                                'tech_slots'        => $tech_slots, 
                                                'tech_specialty'    => $tech_specialty, 
                                                'tech_weekly_offs'  => $tech_weekly_offs,
                                                'tech_work_time'    => $tech_work_time, 
                                                'tech_break_time'   => $tech_break_time,
                                                'tech_slots_new'    => $tech_slots_new, 
                                                'tech_is_active'    => 1
                                             ));
                        // dd($id,$updat);
            $InsertedTechIdId = $techId;
        }
        if ($totalTechServies < 1) 
        {
            $result1 = DB::statement("INSERT INTO tech_services (tech_id,tser_name, tser_order, sser_id) 
             SELECT " . $InsertedTechIdId . ", sser_name, sser_order, sser_id FROM salon_services where sal_id=  " . $id . ""
                            . " and sser_rate > 0 and sser_time > 0 and sser_enabled = 1 ON DUPLICATE KEY UPDATE tech_id = 
                      tech_id ");
        }

        //salon info here 
        $result = DB::table('salon')
                ->Where('sal_id', $id)
                ->update(['sal_status' => 0, 'sal_temp_enabled' => 1,
            'is_active' => 1,
            'sal_appointment_interval' => 30
        ]);

        // dd($result);        
        
    }


}
