<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use File;
use Config;
use Auth;
use App;
use App\Helpers\Helper;

class ServiceSubCatController extends Controller {

	public function __construct()
    {

           $this->middleware('auth');
          $dbc = new Helper();
          $dbc->setDBConnection();
      
    }    

      public function EditServiceSubCat($id) {
          //dd($id);
        $category = DB::table('service_categories')->where('sc_id', $id)->get();
      // dd($category);
                            return view('service-mgmt/edit-sub-category', ['category' => $category[0]]);
    }
}
