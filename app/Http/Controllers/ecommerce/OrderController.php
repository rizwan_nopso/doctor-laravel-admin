<?php

namespace App\Http\Controllers\ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use Carbon\Carbon;
use Session;
use Input;
use DB;
use URL;

class OrderController extends Controller
{
    public function __construct()
    {        
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }

    public function orders(){
    	$page_title = "Orders ";
    	// $orders = DB::table('orders')
    	// 			->join('customers','customers.cust_id','=','orders.cust_id')->paginate(10);

        $orders = DB::SELECT("SELECT orders.*, products.p_id,customers.cust_name,customers.cust_pic,customers.cust_phone, sum(od_cost) as od_cost from orders INNER JOIN customers ON orders.cust_id=customers.cust_id
                        INNER JOIN order_details ON orders.o_id=order_details.o_id 
                        INNER JOIN products ON order_details.p_id=products.p_id group by orders.o_id");
        // dd($orders);
    	return view('ecommerce/orders/order_listing',compact('orders','page_title'));
    }
    public function order_detail($id){
    	$page_title = "Order Details";
        $order = DB::table('orders')
                    ->join('customers','customers.cust_id','=','orders.cust_id')->first();

        $order_details = DB::table('order_details')
                        ->join('orders','orders.o_id','=','order_details.o_id')
                        ->join('products','products.p_id','=','order_details.p_id')
                        ->where('order_details.o_id',$id)->get();
        // dd($order_details);
    	return view('ecommerce/orders/order_details',compact('order','order_details','page_title'));
    }

}
