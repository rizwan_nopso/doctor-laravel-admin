<?php

namespace App\Http\Controllers\ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Model\Product;
use App\Model\ProductImage;
use App\Helpers\Helper;
use Session;
use DB;
use URL;
use File;
use Response;
use Input;
use App\ProductVendor;

class ProductController extends Controller
{
     public function __construct()
    {        
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }

    public function products($id){
        $p_images = Product::where('pc_id',$id)->get();
    	$page_title = "Manage Products";
        $pc_id = $id;
        $pc_name = DB::table('product_categories')->where('pc_id',$id)->first()->pc_name;
        $product = DB::table('products')->where('pc_id',$id)->get();
    	return view('ecommerce/products/products',compact('pc_name','p_images','product','pc_id','page_title'));
    }
    public function add_product($id){
         $pvpb = productVendor::with('productBrand')
                ->where('pv_status',1)
                ->get()->toArray();
            // dd($pvpb);
        $p_id = '';
    	$return_back = URL::previous();
    	$page_title = "Add Products ";
        $pc_id = $id;
        $pc = DB::table('product_categories')->get();
        $pb = DB::table('product_brands')->where('pb_status','1')->get();
        $pv = DB::table('product_vendors')->where('pv_status','1')->get();
        $p_name = '';
    	return view('ecommerce/products/add_products',compact('pvpb','pv','p_name','p_id','pb','pc','pc_id','page_title','return_back'));
    }
    public function store_pproduct(Request $request){
    	// dd($request->all());
        $return_back = $request->return_back;

        $pvpb = explode(',', $request->pvpb);
        $data['pv_id'] = $pvpb[0];
        $data['pb_id'] = $pvpb[1];
        // dd($data);

    	$data['p_name']             = $request->p_name;
        $data['p_description']      = $request->p_description;
        $data['p_status']           = $request->p_status;
        $data['p_cost']             = $request->p_cost;
        $data['p_rrp']              = $request->p_rrp;
        $data['p_price']            = $request->p_price;
        $data['p_weight']           = $request->p_weight;
        $data['p_dimention']        = $request->p_dimention;
        $data['pc_id']              = $request->pc_id;
        $data['p_vendor_name']      = $request->p_vendor_name;
        // $data['pb_id']              = $request->pb_id;
        // $data['pv_id']              = $request->pv_id;
        $data['pv_code']            = $request->pv_code;
        $data['p_sales_tax']        = $request->p_sales_tax;
        $data['p_created_at']       = Carbon::now()->toDateTimeString();
        $data['p_modified_date']    = Carbon::now()->toDateTimeString();
        $helper = new Helper();
        if($request->hasFile('p_image'))
            {
                // $files = DB::table('salon')->where('sal_id' , $sal_id)->first();
                // $file = $files->sal_pic; 
                // $filename = $helper->salonImages()."/".$file;
                // if (file_exists($filename)) 
                // {   
                //     \File::delete($filename);                         
                // }
                $image              = Input::file('p_image');
                $destinationPath    = $helper->pc_image_store();
                $filename           = strtotime("now").$image->getClientOriginalName();
                // if(Session::get('change_db_connection')=="3"){
                //     $helper->uploadimageFTP('/products_images/'.$filename,Input::file('p_image'));
                // } 
                $image->move($destinationPath, $filename);            
                $data['p_image']    = $filename;
            }

        DB::table('products')->insert($data);
        $p_id = DB::getPdo()->lastInsertId();
        
        return redirect()->to($return_back);
    }
    public function get_product_id(Request $request){
        $p_id = $request->product_id;
        return "product id =  ".$p_id;
    }
    public function save_product_images(Request $request){
            Session::put('active_tabs','store_product_images');
            $file = $request->file('file');
            if (\File::exists($file)) {
                $product['p_id'] = $_POST['p_id'];
                $helper = new Helper();
                $product['pi_datetime'] = Carbon::now()->toDateTimeString();
                $product['pi_created_at'] = Carbon::now()->toDateTimeString();
                $product['pi_modified_date'] = Carbon::now()->toDateTimeString();
                $product['pi_name'] = 'pi_'.time().'.'. $file->getClientOriginalExtension();
                $destinationPath = $helper->pc_image_store();   
                // if(Session::get('change_db_connection')=="3"){
                //     $helper->uploadimageFTP('/products_images/'.$product['pi_name'],$file);
                // } 
                $file->move($destinationPath, $product['pi_name']); 
                DB::table('product_images')->insert($product);   
           }
        echo "data saved successfully ";
        return Response::json([
            'message' => 'Image saved Successfully'
        ], 200);
        
    }
    public function edit_product($id,$pc_id){
        $pvpb = productVendor::with('productBrand')
                ->where('pv_status',1)
                ->get()->toArray();

        Session::put('active_tabs','edit_products');
        $p_id = $id;
        $page_title  = "Update Product ";
        $return_back = URL::previous();
        $products = DB::table('products')->where('pc_id',$pc_id)->get();
        $product = DB::table('products')->where('p_id',$id)->first();
        $p_name = $product->p_name;
        $pc_id = $pc_id;
        $p_colors = DB::table('product_colors')->where('p_id',$id)->get();
        $pcl_images = DB::table('product_images')->where('p_id',$id)->get();
        // dd($pcl_images);
        $pc = DB::table('product_categories')->get();
        $pb = DB::table('product_brands')->get();
        $pv = DB::table('product_vendors')->get();
        return view('ecommerce/products/add_products',compact('pvpb','pv','p_colors','pcl_images','products','p_id','p_name','product','pb','pc','pc_id','page_title','return_back'));
    }
    public function update_product(Request $request, $id){
        $return_back = $request->return_back;

        $pvpb = explode(',', $request->pvpb);
        $data['pv_id'] = $pvpb[0];
        $data['pb_id'] = $pvpb[1];
        
        $data['p_name']             = $request->p_name;
        $data['p_description']      = $request->p_description;
        $data['p_status']           = $request->p_status;
        $data['p_cost']             = $request->p_cost;
        $data['p_rrp']              = $request->p_rrp;
        $data['p_price']            = $request->p_price;
        $data['p_weight']           = $request->p_weight;
        $data['p_dimention']        = $request->p_dimention;
        $data['pc_id']              = $request->pc_id;
        $data['p_vendor_name']      = $request->p_vendor_name;
        // $data['pb_id']              = $request->pb_id;
        // $data['pv_id']              = $request->pv_id;
        $data['pv_code']            = $request->pv_code;
        $data['p_sales_tax']        = $request->p_sales_tax;
        $data['p_created_at']       = Carbon::now()->toDateTimeString();
        $data['p_modified_date']    = Carbon::now()->toDateTimeString();
        $helper = new Helper();
        if($request->hasFile('p_image'))
            {
                $files = DB::table('products')->where('p_id',$id)->first();
                $file = $files->p_image; 
                $filename = $helper->pc_image_store()."/".$file;
                if (file_exists($filename)) 
                {   
                    \File::delete($filename);                         
                }
                $image              = Input::file('p_image');
                $destinationPath    = $helper->pc_image_store();
                $filename           = strtotime("now").$image->getClientOriginalName();
                // if(Session::get('change_db_connection')=="3"){
                //     $helper->uploadimageFTP('/products_images/'.$filename,Input::file('p_image'));
                // } 
                $image->move($destinationPath, $filename);            
                $data['p_image']    = $filename; 
            }

        DB::table('products')->where('p_id',$id)->update($data);
        $p_id = $id;
        
        return redirect()->to($return_back);
    }
    public function delete_product($id){
        DB::table('products')->where('p_id',$id)->delete();
        // $images = DB::table('')->where('p_id',$id)->get();
        // if(!empty($images)){
        //     foreach ($images as $key => $value) {
                
        //     }
        // }
        return back()->with('message','record deleted successfully ');
    }

    public function store_pcl(Request $request, $id){
        $arr = [];
        $pcl_images = $request->pcl_image;
        $pcl_codes = $request->pcl_code;
        $pcl_values = $request->pcl_value;
        $product_ids = $request->p_id;
        foreach ($product_ids as $key => $value) {
            $pcl_code   = $pcl_codes[$key];
            $p_id = $value;
            if(isset($pcl_values[$key])){
                $pcl_value = $pcl_values[$key];
            }else{
                $pcl_value = '';
            }
            if(isset($pcl_images[$key])){
                $pcl_image  = $pcl_images[$key];
            }else{
                $pcl_image   = '';
            }
            $filename = '';
            if(\File::exists($pcl_image)) {                    
                    $helper = new Helper();
                    $filename = $pcl_image;
                    $filename = 'pcl_'.str_random(5).'.'.$filename->getClientOriginalExtension();
                    $destinationPath = $helper->pc_image_store();
                    // if(Session::get('change_db_connection')=="3"){
                    //     $helper->uploadimageFTP('/products_images/'.$filename,$pcl_image);
                    // } 
                    $pcl_image->move($destinationPath, $filename); 
                }
            $pcl_created_at     = Carbon::now()->toDateTimeString();
            $pcl_modified_date  = Carbon::now()->toDateTimeString();
        array_push($arr, ['pcl_image' => $filename,'pcl_value'=>$pcl_value,'pcl_code'=>$pcl_code,'p_id'=>$p_id,'pcl_created_at'=>$pcl_created_at,'pcl_modified_date'=>$pcl_modified_date]);
        }

        try {
            $result = DB::table('product_colors')->insert($arr);
        } catch (Exception $e) {
            return back()->withError($e->getMessage())->withInput();
        }
        return redirect()->back()->with('message', 'Data Saved Successfully!!');

        // return back()->with('message','Data Saved Successfully!');
    } 

    public function p_search(Request $request, $id){
        $page_title = "Manage Products";
        $pc_id = $id;
        $p_name = '';
        $p_status = '';

        $pc_name = DB::table('product_categories')->where('pc_id',$id)->first()->pc_name;
        
        $data = DB::table('products')->where('pc_id',$id);
        if($request->p_name){
            $p_name = $request->p_name;
            $data = $data->where('p_name','LIKE','%'.$p_name.'%');
        }
        if($request->p_status){
            $p_status = $request->p_status;
            if($p_status !== 'all'){
                if($p_status == 1){
                    $data = $data->where('p_status','1');
                }
                elseif($p_status == 2){
                    $data = $data->where('p_status','0');
                }
            }
        }
        $product = $data->paginate(10);
        // dd($result);
        // $product = DB::table('products')->where('pc_id',$id)->get();
        return view('ecommerce/products/products',compact('pc_name','p_images','product','pc_id','page_title'));
    }
    public function delete_images($id){
        DB::table('product_images')->where('pi_id',$id)->delete();
        return back();
    }

    public function product_data_import(){
        $page_title = "Products Data Import ";
        return view('ecommerce/products/products_data_import',compact('page_title'));
    }
    // add products data 
    public function store_products_import_data(Request $request) 
    {
        $page_title = " File Upload ";
        $cat_data = array();
        $cat_data['pdi_file_name'] = $request->input('pdi_file_name');
        if (Input::hasFile('pdi_file_name')) 
        {
            $cat_data['pdi_file_name'] = time() . '.' . $request->pdi_file_name->getClientOriginalExtension();
            
            $request->pdi_file_name->move(public_path('category_images'), $cat_data['pdi_file_name']);
        } 
        else 
        {
            $cat_data['pdi_file_name'] = '';
        }
        $file_name = $cat_data['pdi_file_name'];
        define('CSV_PATH', 'category_images/');
        $csv_file2 = CSV_PATH . $file_name;

        DB::statement("DELETE FROM product_data_import");

        $pdo = DB::connection()->getPdo();
        $exfile = $pdo->exec("LOAD DATA LOCAL INFILE '".$csv_file2."' INTO TABLE product_data_import FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\r\\n'");
            
        $result = DB::statement("UPDATE `product_data_import` pdi INNER JOIN product_categories pc on pc.pc_name = pdi.pdi_category SET pdi.pc_id = pc.pc_id WHERE pc.pc_status = 1 and pdi.pc_id = 0");

            DB::statement("INSERT INTO product_categories (pc_name) 
                select distinct pdi_category from product_data_import where pc_id = 0 ");

        $result = DB::statement("UPDATE `product_data_import` pdi INNER JOIN product_categories pc on pc.pc_name = pdi.pdi_category SET pdi.pc_id = pc.pc_id WHERE pdi.pc_id = 0");    

        DB::statement("INSERT INTO products(p_name,p_description,p_image,p_status,p_cost,p_rrp,p_price,p_weight,p_dimention,pc_id,pv_id,pb_id,p_quantity,p_created_at,p_modified_date,pv_code,p_sales_tax,temp_p_id) SELECT pdi_name,pdi_short_description,pdi_image,1,REPLACE(pdi_tp,',',''),REPLACE(pdi_mrp,',',''),REPLACE(p_price,',',''),'','',pc_id,pv_id,pb_id,1,'".\Carbon\Carbon::now()->toDateTimeString()."','".\Carbon\Carbon::now()->toDateTimeString()."',pdi_code,REPLACE(p_sales_tax,',',''),temp_p_id from product_data_import as pdi  ON DUPLICATE KEY UPDATE products.temp_p_id=products.temp_p_id, products.p_name = pdi.pdi_name,products.p_description = pdi.pdi_short_description, products.p_image = pdi.pdi_image, products.p_cost = REPLACE(pdi.pdi_tp,',',''),products.p_rrp = REPLACE(pdi.pdi_mrp,',',''),products.p_price = REPLACE(pdi.p_price,',',''),products.pc_id = pdi.pc_id,products.pv_code = pdi.pdi_code,products.p_sales_tax = REPLACE(pdi.p_sales_tax,',',''),products.pv_id = pdi.pv_id,products.pb_id = pdi.pb_id");
        $total_record = $exfile;

        $page_title = "Products Data Import ";
        Session::flash('success', ' <strong style="font-size:20px; margin-right:10px;">'.$total_record.'</strong>  Products Added Successfully.');

        return view('ecommerce/products/products_data_import',compact('total_record','page_title'));

    }
    
    public function product_images(){
        $page_title = '';
        $id = null;
        $main_pro = DB::table('product_categories')->orderBy('pc_name','ASC')->get();
        return view('products_images',compact('main_pro','id','page_title'));
    }
    public function get_product_images($id){
        // dd($id);
        $page_title = '';
        $main_pro = DB::table('product_categories')->orderBy('pc_name','ASC')->get();
        $pro = DB::table('products')->where('pc_id',$id)->get();
        // dd($pro);
        return view('products_images',compact('pro','main_pro','id','page_title'));
    }

    public function ajaxMethod(Request $request){
        $vendor_id = $request->vendor_id;
        $variable = DB::table('product_brands')->where('pv_id',$vendor_id)->where('pb_status','1')->get();
        echo '<option value="">Please Select Brand</option>';
        foreach ($variable as $key => $value) {
            echo ' <option value="'.$value->pb_id.'">'.$value->pb_name.'</option>';
        }
        die();
    }

}
