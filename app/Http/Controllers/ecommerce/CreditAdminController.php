<?php

namespace App\Http\Controllers\ecommerce;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use App\Helpers\Helper;
use Carbon\Carbon;
use Session;
use Input;
use DB;
use URL;

class CreditAdminController extends Controller
{
    //
    public function __construct()
    {        
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }
    public function store_credit(Request $request){
    	$page_title = " ";
        $customers = '';
        $find_user = $request->find_user;

        if(!empty($find_user)){
            $customers = DB::table('customers')->orWhere('cust_name','LIKE',$find_user.'%')->orWhere('cust_phone','LIKE',$find_user.'%')->orderBy('cust_name','ASC')->get();
            // dd($customers);
        }
    	return view('ecommerce/credit_admin/store_credit',compact('page_title','customers'));
    }
    public function storecredits(Request $request, $id){
        $page_title = " ";
        $cust = DB::table('customers')->where('cust_id',$id)->first();
        // dd($cust);
        $orders = DB::table('orders')->where('cust_id',$id)->get();
        $cs = DB::table('store_credit')->where('cust_id',$id)->orderBy('csc_id','DESC')->get();
        // dd($cs);
        return view('ecommerce/credit_admin/storecredits',compact('orders','id','page_title','cust','cs'));
        dd($orders);
    }
    public function store_cust_credits(Request $request){
        $helper = new Helper();
        $cust_phone = $request->cust_phone;
        $data['csc_credits'] = $request->csc_credits;
        $data['csc_comments'] = $request->csc_comments;
        $data['csc_send_sms'] = $request->csc_send_sms == null ? '0' : $request->csc_send_sms;
        $data['cust_id']      = $request->cust_id;
        $data['o_id']       = $request->o_id;
        $data['csc_datetime']   = Carbon::now()->toDateTimeString();
        if($data['csc_send_sms'] == 1){
            // echo $data['csc_send_sms']."<br>";
            $helper->send_sms($cust_phone,$data['csc_comments']);
               // $this->send_sms($cust_phone',$data['csc_comments']); 
        }
        // dd($data);
        DB::table('store_credit')->insert($data);
        $cust_store_credit = DB::table('customers')->where('cust_id',$data['cust_id'])->first()->cust_store_credit; 
        $cust_store_credit = $cust_store_credit + $data['csc_credits'];
        // dd($cust_store_credit);
        DB::table('customers')->where('cust_id',$data['cust_id'])->update([
                    'cust_store_credit'=> $cust_store_credit ]);
        // Session::forget('change_db_connection');
            // Session::put('flash_message','Credit Store Successfully ');
        return back()->with('flash_message','Credit Store Successfully ');
        dd($request->all());
    }

    public function coupons(){
         // $decrypted = Crypt::decryptString('$2y$10$ocHlGVmhAmY6i0k9Ydqa6.P8qITeTef8tVpIHnKmQHEweW9Ka2u4K');
        // dd($decrypted);
        // dd(str_random(8));
        $page_title = '';
            
        // $query->whereBetween('age', [$ageFrom, $ageTo]);

        // $coupons = DB::select( DB::raw("SELECT * FROM promo_codes WHERE  date_add(pc_expiry, INTERVAL 30 DAY) >= now() ORDER BY pc_expiry DESC ") ); 
        // dd($coupons);
        // $passed = Deployment::where('created_at', '>=', Carbon::now()->subDays(14)->toDateTimeString());

        $coupons = DB::table('promo_codes')
                            // ->where(DB::raw("date_add(pc_expiry, INTERVAL 30 DAY)"),'>=',Carbon::now()->toDateTimeString())
                            ->where('pc_expiry','>=',Carbon::now()->toDateTimeString())
                            ->where('pc_balance','!=','0')
                            ->orderBy('pc_expiry','DESC')->paginate(50);
                            // dd($coupons);
        return view('ecommerce/credit_admin/coupons',compact('page_title','coupons'));
    }

    public function add_coupons(){
        $page_title = '';

        return view('ecommerce/credit_admin/add_coupons',compact('page_title'));
    }
    public function store_coupons(Request $request){
        // $encrypted = Crypt::encryptString(str_random(8));
        // $decrypted = Crypt::decryptString($encrypted);
        // dd($encrypted);
        // dd(str_random(8));
        $pc_title = [];
        $pc_balance = [];
        $pc_percent = [];
        $pc_expiry = [];
        $pc_code = [];
        $hm = $request->m_coupons;
        if(!empty($hm)){
            for($i=1; $i<=$hm; $i++) {
                $data[$i]['pc_title'] = $request->pc_title;
                $data[$i]['pc_code'] = str_random(8);
                $data[$i]['pc_max_value'] = $request->pc_max_value;
                $data[$i]['pc_percent'] = $request->pc_percent;
                $data[$i]['pc_expiry'] = $request->pc_expiry;
                $data[$i]['pc_total'] = 1;
                $data[$i]['pc_used'] = 0;
                $data[$i]['pc_balance'] = $request->m_coupons;
                $data[$i]['pc_description'] = '';
                $data[$i]['cust_id'] = null;
            }
            DB::table('promo_codes')->insert($data);
            // dd('data inserted successfully ');
            // $result = DB::statement("INSERT INTO promo_codes(pc_title, pc_code, pc_max_value,pc_expiry, pc_total,pc_used,pc_balance,pc_description,cust_id) VALUES ('".$pc_title."','".$pc_code."','0','".$pc_expiry."','1','0','".$pc_balance."','0','0')");
        }
        return redirect()->to('admin/coupons');
        return back()->with('message','Coupon added successfully');


        // echo "<br>";
        // print_r($pc_title);
        // echo "<br><br><br>";
        // print_r($pc_balance);
        // echo "<br><br><br>";
        // print_r($pc_percent);
        // echo "<br><br><br>";
        // print_r($pc_expiry);
        // echo "<br><br><br>";
        // print_r($pc_code);
        // echo "<br>";
       
    }

    public function send_coupon($id){
        $page_title = '';
        return view('ecommerce/credit_admin/send_coupon',compact('page_title','id'));
    }
    public function send_coupons(Request $request,$id){
        $helper = new Helper();
        $current = Carbon::now();
        $pc_code = DB::table('promo_codes')->where('pc_id',$id)->first()->pc_code;
        $string = $request->send_description;
        $phone = $request->sal_phone;
        $descriptions = str_replace("{CouponNo}",$pc_code, $string);
        
        $helper->send_sms($phone,$descriptions);

        // dd($descriptions);
        return back()->with('message','SMS send successfull');
    }

    

}
