<?php

namespace App\Http\Controllers\fashion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use App\Helpers\Helper;
use Carbon\Carbon;
use DateTime;
use Config;
use File;
use Auth;
use App;
use DB;

class FashCollController extends Controller
{
    //
    public function __construct()
    {        
        ini_set('memory_limit','512M');
        $this->middleware('auth');
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }
    public function addFashCollection(Request $request) {
        $returnback = \URL::previous();
        $helper = new Helper();
        $cate_images = $helper->fashion_image_display();
        $page_title = "Add New Colection";
        return view('fashions/fash_collection/add_fash_collection',compact('returnback','page_title','cate_images'));   
    }
    public function store_factionColletion(Request $request){
        $helper = new Helper();
    	$data['fc_title'] 	= $request->fc_title;
    	$data['fc_status'] 	= $request->fc_status;
        $data['fc_gender']  = $request->fc_gender;
    	if (Input::hasFile('fc_image')) {
            $data['fc_image'] = "fc".time() . '.' . $request->fc_image->getClientOriginalExtension();
            $destinationPath    = $helper->fashion_image_store();  
            // if(Session::get('change_db_connection')=="3"){
            //     $helper->uploadimageFTP('/fashion_images/'.$data['fc_image'],$request->fc_image);
            // }     
            $request->fc_image->move($destinationPath, $data['fc_image']);    
        }
    	DB::table('fash_collections')->insert($data);
    	// dd("data inserted now ");
    	return redirect()->to('admin/fashionCollection');
    }
    public function update_collection(Request $request, $id){
        $returnback = \URL::previous();
    	$helper = new Helper();
        $cate_images = $helper->fashion_image_display();
        $page_title = "";
    	$fc = DB::table('fash_collections')->where('fc_id',$id)->first();
    	 return view('fashions/fash_collection/add_fash_collection',compact('returnback','fc','page_title','cate_images'));   
    }
    public function updatedCollection(Request $request,$id){
        $helper = new Helper();
    	$data['fc_title'] 	= $request->fc_title;
    	$data['fc_status'] 	= $request->fc_status;
        $data['fc_gender']  = $request->fc_gender;
    	if (Input::hasFile('fc_image')) {
            $data['fc_image'] = "fc".time().'.'.$request->fc_image->getClientOriginalExtension();
            $fb = DB::table('fash_collections')->where('fc_id',$id)->first();
            if($fb){
                $fc_image = $helper->fashion_image_display()."/".$fb->fc_image;   
                // dd($fb_image);     
                if (File::exists($fc_image) && $fc->fc_image != '') 
                { 
                    unlink($fc_image);
                }
            }
            $destinationPath    = $helper->fashion_image_store();
            // if(Session::get('change_db_connection')=="3"){
            //     $helper->uploadimageFTP('/fashion_images/'.$data['fc_image'],$request->fc_image);
            // }       
            $request->fc_image->move($destinationPath, $data['fc_image']);    
        }
    	DB::table('fash_collections')->where('fc_id',$id)->update($data);
    	return redirect()->to('admin/fashionCollection');
    }
    public function fac_collections(Request $request){
    	// dd($request->all());
    	$helper = new Helper();
        $cate_images = $helper->fashion_image_display();
        $page_title = "";
        $fc_status = $request->fc_status;
        $fc_title = $request->fc_title;
        // dd($fc_status);
        $data = DB::table('fash_collections');
        if(!empty($fc_title) && !empty($fc_status)){
            if($fc_status == "all_active")
                {
                    $data = $data->where('fc_title','LIKE','%'.$fc_title.'%');
                }else{
                    if($fc_status == "1")
                    {
                        $data = $data->where('fc_title','LIKE','%'.$fc_title.'%')->where('fc_status','1');  
                    }
                    elseif($fc_status == "2")
                    {
                        $data = $data->where('fc_title','LIKE','%'.$fc_title.'%')->where('fc_status','0'); 
                    }
                }
        }elseif(!empty($fc_title) && empty($fc_status)){
            $data = $data->where('fc_title','LIKE','%'.$fc_title.'%');
        }elseif(empty($fc_title) && !empty($fc_status)){
            if($fc_status == "all_active")
                {
                    $data = $data->where('fc_status',0)->orWhere('fc_status',1);
                }else{
                    if($fc_status == "1")
                    {
                        $data = $data->where('fc_status','1');  
                    }
                    elseif($fc_status == "2")
                    {
                        $data = $data->where('fc_status','0'); 
                    }
                }
        }
        
            // dd($fc_title);
        $fc = $data->orderBy('fc_id','DESC')->paginate(10);
        // dd($fc);
        return view('fashions/fash_collection/collections',compact('fc_status','fc_title','page_title','fc'));
    }
    public function deleteCollection($id){
    	// dd($id);
    	DB::table('fash_collections')->where('fc_id',$id)->delete();
    	return back()->with('message','Recored Deleted Successfully ');
    }

}
