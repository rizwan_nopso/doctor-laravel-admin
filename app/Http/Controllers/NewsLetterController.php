<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Newsletter;

class NewsLetterController extends Controller
{
    
public function newsletter()
{
    return view('newsletter/letter');
}
public function subscribe(Request $request)
{
    $email = $request->email;
    // $re = Newsletter::subscribe($email);  
    $re = Newsletter::unsubscribe($email);
    
}

// Newsletter::subscribe('rincewind@discworld.com');

// Newsletter::unsubscribe('the.luggage@discworld.com');

// Merge variables can be passed as the second argument
// Newsletter::subscribe('sam.vines@discworld.com', ['firstName'=>'Sam', 'lastName'=>'Vines']);

// Subscribe someone to a specific list by using the third argument:
// Newsletter::subscribe('nanny.ogg@discworld.com', ['firstName'=>'Nanny', 'lastName'=>'Ogg'], 'Name of your list');

// Subscribe someone to a specific list and require them to confirm via email:
// Newsletter::subscribePending('nanny.ogg@discworld.com', ['firstName'=>'Nanny', 'lastName'=>'Ogg'], 'Name of your list');

// Subscribe or update someone
// Newsletter::subscribeOrUpdate('sam.vines@discworld.com', ['firstName'=>'Foo', 'lastName'=>'Bar']);

// Change the email address of an existing subscriber
// Newsletter::updateEmailAddress('rincewind@discworld.com', 'the.luggage@discworld.com');

// Get some member info, returns an array described in the official docs
// Newsletter::getMember('lord.vetinari@discworld.com');

// Get the member activity, returns an array with recent activity for a given user
// Newsletter::getMemberActivity('lord.vetinari@discworld.com');

// Get the members for a given list, optionally filtered by passing a second array of parameters
// Newsletter::getMembers();

// Check if a member is subscribed to a list
// Newsletter::isSubscribed('rincewind@discworld.com');

// Returns a boolean
// Newsletter::hasMember('greebo@discworld.com');

// If you want to do something else, you can get an instance of the underlying API:
// Newsletter::getApi();
}
