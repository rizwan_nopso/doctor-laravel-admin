<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Session;
use App\Model\salon_services_sa_import;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use App\Model\serviceCategory;
use App\app_requests_action;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Carbon\Carbon;
use DateTime;
use Config;
use Auth;
use App;
use DB;
use File;


class CustomerController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
        $dbc = new Helper();
        $dbc->setDBConnection(); 
    }

    public function customers(Request $request){
    	$cust_name = null;
    	$helper = new Helper();
        $display_cust_img = $helper->display_cust_img();
        $page_title = "";
        if(!empty($request->cust_name)){
        	$cust_name = $request->cust_name;
        	$customers = DB::table('customers')
                            ->where('cust_name','like','%'.$cust_name.'%')
                            ->orWhere('cust_phone','like','%'.$cust_name.'%')
                            ->orderBy('cust_datetime','DESC')
                            ->paginate(50);
        	// dd($customers);
    	}else{
    		$customers = DB::table('customers')->where('cust_name','<>','')->orderBy('cust_datetime','DESC')->paginate(50);
    	}
    	// dd($customers);
    	return view('customers/customers',compact('customers','page_title','cust_name','display_cust_img'));
    }

    public function enable_cust($id){
            DB::table('customers')->where('cust_id',$id)->update(['cust_status' => '1']);

            return back();
    }
    public function disable_cust($id){
         DB::table('customers')->where('cust_id',$id)->update(['cust_status' => '0']);
            return back();
    }
}
