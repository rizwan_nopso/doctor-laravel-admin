@extends('salon-magmt.base') 
@section('action-content')
-<section class="content">

    <div class="row">
        <div class="col-sm-12 col-lg-12">
            <div class="box">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">

                        <li id="one" class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Salon</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Salon Services</a></li>

                        <!--                        <li id="three" class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Salon Services</a></li>-->
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-header">
                                <h3 class="box-title"> 
                                    Import Salon data 
                                </h3>
                                <!--<p>All fields are required* </p>-->

                            </div>
                            <div class="box-body">
                                <div>

                                    <div class="">

                                        <form action="{{ url('save_file') }}" method="POST" role="form" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">File Name</label>
                                                    <input required="" type="text" name="cat_name" class="form-control" id="exampleInputEmail1" placeholder="Enter file Name">
                                                </div>
                                                <div class="form-group">

                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputFile">Add File</label>
                                                    <input required="" name="cat_image" type="file" id="exampleInputFile" class="form-control">
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                            @if(Session::has('flash_message'))
                                            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                                            @endif
                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                <a hidden="" class="btn btn-primary" style="margin-left: 10px;" href="{{ url('') }}" >Cancel</a>
                                            </div>
                                        </form>

                                    </div>
                                    <?php
//                                $attributes = array("name" => "services", "id" => "service_form");
//                                echo form_open("admin/update_services/", $attributes);
                                    ?>

                                    <form role="form" method="POST" action="{{ url('type_services_update') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->

                        <div class="tab-pane" id="tab_2">
                             <div class="box-header">
                                <h3 class="box-title"> 
                                    Import Salon Services data 
                                </h3>
                                <!--<p>All fields are required* </p>-->

                            </div>
                            <form action="{{ url('save_file_services') }}" method="POST" role="form" enctype="multipart/form-data">
                                {{ csrf_field() }} 
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">File Name</label>
                                        <input required="" type="text" name="cat_name" class="form-control" id="exampleInputEmail1" placeholder="Enter file Name">
                                    </div>
                                    <div class="form-group">

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Add File</label>
                                        <input required="" name="cat_image" type="file" id="exampleInputFile" class="form-control">
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                @if(Session::has('flash_message'))
                                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                                @endif
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a hidden="" class="btn btn-primary" style="margin-left: 10px;" href="{{ url('') }}" >Cancel</a>
                                </div>
                            </form>
                        </div>

                        <!-- close form here -->



                        <!-- /.tab-pane -->

                        <!-- /.tab three  -->

                        <!--                        <div class="tab-pane" id="tab_3">
                                                    <div class="box-header">
                                                        <h3 class="box-title">sal_name  </h3>
                                                        <p>All fields are required* </p>
                                                    </div>
                                                </div>-->
                        <!-- end tab three -->
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>


    -</section>
-@endsection