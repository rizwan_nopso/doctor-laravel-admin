@extends('salon-magmt.base')
@section('action-content')
<section class="content">

    @if(isset($message))
    @if($message==1)
    <div class="alert alert-success">
        <strong>Success!</strong> {{$messageInfo}}
    </div>
    @endif

    @if($message==0)
    <div class="alert alert-danger">
        <strong>Failed!</strong> {{$messageInfo}}
    </div>
    @endif
    @endif
      <h3>{{$title}}</h3>
    <div>
        <form role="form" method="get" action="#" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div style="margin:0 auto;
                 width:100%;
                 height:50px;
                 " class="div1">
                <div style="margin:0 auto;
                     height:50px;
                     " class="div1">
                     <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                            
                            Search salon
                            
                          </label>
                          <input placeholder="Search salon"  type="text" class="form-control" value="@isset($sal_search) {{$sal_search}}  @endisset" name="sal_search" maxlength="128">
                        </div>
                      </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="is_active" id="is_active">
                          <option value="all_status">All</option>
                          <option value="1">Active</option>
                          <option value="2">In-Active</option>
                        </select>
                      </div>
                    </div>
                    {{-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>Reviewed</label>
                        <select class="form-control" name="sal_is_reviewed" id="sal_is_reviewed">
                          <option value="all_isreviewed">All</option>
                          <option value="1">Yes</option>
                          <option value="2">No</option>
                        </select>
                      </div>
                    </div> --}}

                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;" name="submit"  class="btn btn-info" >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>

{{-- @if(!empty($sal_search) OR !empty($is_reviewed) OR !empty($is_active))
                    @php  
                      if(is_null($is_reviewed))
                        {
                          $is_reviewed = "";
                        }

                      if(empty($sal_search))
                        {
                          $sal_search = "";
                        }
                      if(is_null($is_active))
                        {
                          $is_active = "";
                        }

                        $urls = ('sarch_salons?'.'sal_search'.'='.$sal_search.'&'.'is_active'.'='.$is_active.'&'.'sal_is_reviewed'.'='.$is_reviewed.'&'.'page='.$salons->currentPage());
                    @endphp
                  @else
                    @php
                       $urls = ('managesalons?'.'page='.$salons->currentPage());
                    @endphp
                  @endif --}}
  
    <div class="box" style=" margin-top: 40px; margin-left: 0px; padding-left: 5px;">
        <form role="form" method="POST" action="{{ url('typeservices') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div   class="form-group">
                <table id="salon_tabel" class="table table-hover">
                    <tr>
                      <th>
                        # 
                      </th>
                        <th>
                          Name   
                        </th>
                        <th> Created At</th>
                        <th> Description</th>
                        <th>
                          Service Category    
                        </th>
                        <th>
                          Number Of Pins
                        </th>
                        <th> Status</th>
                        <th style=" display: block;
                            margin-left: auto;
                            text-align: center;
                            margin-right: auto;"><u> Actions </u> </th>
                    </tr>
                  {{-- @php dd("ok"); @endphp --}}
                   
                    @foreach($pb as $key => $row)
                            @php $key += 1; @endphp 
                        <tr>
                            <td>
                              {{ $key }}
                            </td>
                            <td>
                              {{ $row->pb_name }}
                            </td>
                            
                            <td>
                              {{ date('Y-m-d' , strtotime($row->pb_created_at)) }} 
                            </td>
                            <td>
                                {{ $row->pb_description }}
                            </td>
                            <td>
                              <select style="width: 150px; padding: 2px;" class="ssc_id">
                                @php $sscs = DB::table("salon_ser_categories")->get();  @endphp
                                @foreach($sscs as $key => $ssc)
                                  <option value="{{$ssc->ssc_id}}" @if($row->ssc_id == $ssc->ssc_id) selected="" @endif>{{$ssc->ssc_name}}</option>
                                @endforeach
                              </select>
                            </td>
                            <td style=" text-align:center;">
                                {{ $row->pb_description }}
                            </td>
                            <td style=" text-align:center; ">
                              @if($row->pb_status == 1)
                                Active
                              @else
                                In-Active
                              @endif
                            </td>
                             
                            <td>
                              
                              @if($row->pb_status==0)
                                    <a 
                                      href="{{ url('admin/ajax_activate'.'/'.$row->pb_id) }}" 
                                      style="border: none;
                                       color: red;
                                       background: #52C1F0;
                                       padding: 5px 10px;
                                       font-size: 20px;
                                       cursor: pointer;" 
                                       onclick="Activate({{ $row->pb_id }})"
                                       class="btn" title="Active">
                                        <i class="fa fa-close fa-lg"></i>
                                    </a>
                                    @endif
                                    @if($row->pb_status==1)
                                  <a
                                    href="{{ url('admin/ajax_deactivate'.'/'.$row->pb_id) }}" 
                                    style="border: none;
                                       color: white;
                                       background: #52C1F0;
                                       padding: 5px 10px;
                                       font-size: 20px;
                                       cursor: pointer;"
                                       disabled="disabled"
                                       class="btn" title="De-Active">
                                        <i class="fa fa-close fa-lg"></i>
                                    </a>
                                    @endif

                                    {{-- <button onclick="Activate({{ $row->pb_id }})" class="btn btn-xs btn-default" title="Activate"><i style="color: white" class="fa fa-user-plus fa-lg" aria-hidden="true"></i></button> --}}

                                   {{--  <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                                      Launch Default Modal
                                    </button>   --}}
                            </td>

                                  <div class="modal fade" class="modal-default">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Status Change</h4>
                                          </div>
                                          <div class="modal-body">
                                            <p>Are you sure you want to update status&hellip;</p>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                          </div>
                                        </div>
                                        <!-- /.modal-content -->
                                      </div>
                                      <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal --> 

                        </tr>
                    @endforeach
                </table>
                  @php 
                    $urls = url(''.'/'.'pinterest-board?'.'page='.$pb->currentPage());
                  @endphp
                
                   {{-- <script type="text/javascript">
                          function Activate(pb_id){
                              alert(pb_id);
                            document.getElementById("wait").style.display = "block";
                            $.post('{{ url('admin/ajax_activate') }}' , {pb_id: pb_id , _token: '{{ csrf_token() }}'}  , function(data){
                                document.getElementById("wait").style.display = "none";
                                window.location.href = "{{ $urls }}";
                            });
                        }
                  
// DeActive status
                        function Activate(pb_id){
                              alert(pb_id);
                            document.getElementById("wait").style.display = "block";
                            $.post('{{ url('admin/ajax_deactivate') }}' , {pb_id: pb_id , _token: '{{ csrf_token() }}'}  , function(data){
                                document.getElementById("wait").style.display = "none";
                                window.location.href = "{{ $urls }}";
                            });
                        }
                </script>  --}}
                
                {{-- {!! $salons->appends(['sal_search'=>isset($sal_search)? $sal_search:'','is_active'=>isset($is_active)? $is_active:'','sal_is_reviewed'=>isset($is_reviewed)? $is_reviewed:'','page'=>$salons->currentPage()])->links() !!} --}}
                   {{$pb->links()}}
                  
            </div>
        </form>
    </div>
</section>
@endsection
@push("script")
      <script type="text/javascript">
          jQuery("#is_active").on("change",function(){
          var id = jQuery(this).val();
          // alert(id);
        });
      </script>
    @isset($is_reviewed)
      <script type="text/javascript">
        $("#sal_is_reviewed").val("{{$is_reviewed}}");
      </script>
    @endisset

    @isset($is_active)
      <script type="text/javascript">
        $("#is_active").val("{{$is_active}}");
      </script>
    @endisset

    
    <script type="text/javascript">
          $(".ssc_id").on("change",function(){
            alert("Are you sure you want to change salon service sub-category ");
              // $(".modal-default").show();
          });
    </script>

@endpush