<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('/', 'DashboardController@index')->middleware('auth');
Auth::routes();
Route::get('/dashboard', 'DashboardController@index');
// Salon routes
Route::get('/managesalons', 'AdminController@manage_salons');

Route::get('/sarch_salons', 'AdminController@sarch_salons');

Route::get('/editsalon/{id}/{type_id}/{urls}', 'AdminController@edit_salon');

Route::get('/setUpSalon/{id}', 'AdminController@setUpSalon');
Route::get('/setSalonInfo/{id}/', 'AdminController@setSalonInfo');

Route::get('/editsalonservices/{id}/{salId}', 'AdminController@edit_salon_services');

//Route::get('/editsalon/{id}', 'AdminController@edit_salon');

//me 
Route::get('/salon_types', 'AdminController@salon_types');



Route::get('/editsalontype/{id}', 'AdminController@editsalontype');

Route::get('/updatesalontype/{id}/{id2}', 'AdminController@updatesalontype');

//Route::get('/updatesalontype/{id}/{id2}', 'AdminController@updatesalontype');

Route::post('/updatesalontype', 'AdminController@updatesalontype');

Route::post('/updatesalontypeservices', 'AdminController@updatesalontypeservices');

Route::post('/updateServiceSubCat', 'AdminController@updateServiceSubCat');


Route::get('/editsalontypedetail/{id}/{salon_name}', 'AdminController@editsalontypedetail');
//end me
Route::post('/typeservices', 'AdminController@type_services');
Route::post('/type_services_update', 'AdminController@type_services_update');



// Services Categories routes
Route::get('/servicecategories', 'AdminController@service_categories');


Route::get('/manage_subcategores/{id}', 'AdminController@manage_subcategores');

Route::get('/manageservices/{sc_id}/{ssc_id}', 'AdminController@manage_services');



Route::get('/addcategory', 'AdminController@add_category');

Route::get('/fileupload', 'AdminController@fileupload');

Route::get('/addtypeservice', 'AdminController@add_category');
Route::get('/edit_category/{id}', 'AdminController@edit_category');

Route::get('/edit_sub_category/{id}/{sc_id}', 'AdminController@edit_sub_category');



Route::get('/EditServiceSubCat/{id}', 'ServiceSubCatController@EditServiceSubCat');


Route::get('/EditServiceSubCatagory/{id}', 'ServiceSubCatogryController@EditServiceSubCatagory');



Route::get('/add_subcategory/{sc_id}', 'AdminController@add_subcategory');
Route::post('/save_subcategory', 'AdminController@save_subcategory');

Route::post('/save_category', 'AdminController@save_category');

Route::post('/save_file', 'AdminController@save_file');

Route::post('/save_file_services', 'AdminController@save_file_services');

Route::post('/update_category', 'AdminController@update_category');

Route::post('/update_sub_category', 'AdminController@update_sub_category');

Route::get('/profile', 'ProfileController@index');
Route::post('user-management/search', 'UserManagementController@search')->name('user-management.search');
Route::resource('user-management', 'UserManagementController');
Route::resource('employee-management', 'EmployeeManagementController');
Route::post('employee-management/search', 'EmployeeManagementController@search')->name('employee-management.search');
Route::get('name}', 'EmployeeManagementController@load');

//Route::get('/testFun}'  ,  'AdminController@testFun');

Route::get('/allServices', 'ServiceTagsController@allServices');

Route::get('/deleteService/{id}', 'ServiceTagsController@deleteService');

Route::post('/addNewService', 'ServiceTagsController@addNewService');

Route::get('/editserviceinfo/{id}', 'ServiceTagsController@editserviceinfo');

Route::get('/managetags/{id}/{ser_name}', 'ServiceTagsController@managetags');

Route::post('/updatetagservices', 'ServiceTagsController@updatetagservices');

Route::post('/updateservicename', 'ServiceTagsController@updateservicename');

Route::post('/InsertNewService', 'ServiceTagsController@InsertNewService');

//Route::get('/salon_types', 'AdminController@salon_types');

Route::get('/AddnewTypeService', 'ServicesManagementController@AddnewTypeService');
Route::post('/saveNewTypeService', 'ServicesManagementController@saveNewTypeService');
Route::post('/updateSalon', 'SalonManagementController@updateSalon');
Route::post('/updateSalonServices', 'AdminController@updateSalonServices');

Route::get('/pinterest-board','AdminController@pinterest_board');
Route::get("admin/ajax_activate/{pb_id}","AdminController@ajax_activate");
Route::get("admin/ajax_deactivate/{pb_id}","AdminController@ajax_deactivate");
