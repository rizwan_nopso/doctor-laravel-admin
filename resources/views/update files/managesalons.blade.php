@extends('salon-magmt.base')
@section('action-content')
<section class="content">

    @if(isset($message))
    @if($message==1)
    <div class="alert alert-success">
        <strong>Success!</strong> {{$messageInfo}}
    </div>
    @endif

    @if($message==0)
    <div class="alert alert-danger">
        <strong>Failed!</strong> {{$messageInfo}}
    </div>
    @endif
    @endif
      
    <div>
        <form role="form" method="get" action="{{ url('sarch_salons') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div style="margin:0 auto;
                 width:100%;
                 height:50px;
                 " class="div1">
                <div style="margin:0 auto;
                     height:50px;
                     " class="div1">
                     <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                            
                            Search salon
                            
                          </label>
                          <input placeholder="Search salon"  type="text" class="form-control" value="@isset($sal_search) {{$sal_search}}  @endisset" name="sal_search" maxlength="128">
                        </div>
                      </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="is_active" id="is_active">
                          <option value="all_status">All</option>
                          <option value="1">Active</option>
                          <option value="2">In-Active</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>Reviewed</label>
                        <select class="form-control" name="sal_is_reviewed" id="sal_is_reviewed">
                          <option value="all_isreviewed">All</option>
                          <option value="1">Yes</option>
                          <option value="2">No</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;" name="submit"  class="btn btn-info" >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>


@if(!empty($sal_search) OR !empty($is_reviewed) OR !empty($is_active))
                    @php  
                      if(is_null($is_reviewed))
                        {
                          $is_reviewed = "";
                        }

                      if(empty($sal_search))
                        {
                          $sal_search = "";
                        }
                      if(is_null($is_active))
                        {
                          $is_active = "";
                        }

                        $urls = ('sarch_salons?'.'sal_search'.'='.$sal_search.'&'.'is_active'.'='.$is_active.'&'.'sal_is_reviewed'.'='.$is_reviewed.'&'.'page='.$salons->currentPage());

                        // $urls = ('sarch_salons?'.'sal_search'.'page='.$salons->currentPage());
                        
                          // echo $urls;
                    @endphp
                  @else
                    @php
                       $urls = ('managesalons?'.'page='.$salons->currentPage());
                    @endphp
                  @endif
  
    <div class="box" style=" margin-top: 40px; margin-left: 0px; padding-left: 5px;">
        <form role="form" method="POST" action="{{ url('typeservices') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div   class="form-group">
                <table id="salon_tabel" class="table table-hover">
                    <tr>
                        <th>
                         Name   
                        </th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Phone</th>
                          <th>Appointments</th>
                        <th>Status</th>
                         <th>IsReviewd</th>
                        <th>Available</th>
                        <th style=" display: block;
                            margin-left: auto;
                            text-align: center;
                            margin-right: auto;"><u> Actions </u> </th>
                    </tr>
                    <?php
                    foreach ($salons as $salon) {
                        ?>


                        <tr>
                            <td>
                              {{ $salon->sal_name }}
                            </td>
                            <td>
                              {{ $salon->sal_email }}
                            </td>
                            
                            <td>
                              {{ $salon->sal_address }} 
                            </td>
                            <td>
                                {{ $salon->sal_phone }}
                            </td>
                            <td style="text-align:center;">
                                {{ $salon->count }}
                            </td>
                              
                                  <td style=" text-align:center; ">
                                    @if($salon->sal_status == 1)
                                      Active
                                    @else
                                      In-Active
                                    @endif
                                  </td>
                                  <td style=" text-align:center; ">
                                    @if($salon->sal_is_reviewed == 1)
                                      Yes
                                    @else
                                      No
                                    @endif
                                  </td>
                                  <td style=" text-align:center;">
                                      {{ $salon->sal_temp_enabled }}
                                  </td>
                              <td>
                                <div class="btn-group">
                                    <input type="hidden" value="{{ '-1' }}" name="salon_type">
                                    <input type="hidden" value="{{ $salon->sal_id }}" name="salon_type">

                                    <!--                                 <button name="submit" type="submit" class="btn btn-primary" >Edit</button>-->


                                    @if($salon->sal_temp_enabled==0)
                                    <a style="border: none;
                                       color: red;
                                       background: #52C1F0;
                                       padding: 5px 10px;
                                       font-size: 20px;
                                       cursor: pointer;" 
                                       href="{{ url('setUpSalon', ['id' => $salon->sal_id ]) }}"
                                       class="btn">
                                        <i class="glyphicon glyphicon-ok-sign"> </i>
                                    </a>
                                    @endif
                                    @if($salon->sal_temp_enabled==1)
                                    <a style="border: none;
                                       color: white;
                                       background: #52C1F0;
                                       padding: 5px 10px;
                                       font-size: 20px;
                                       cursor: pointer;" 

                                       disabled="disabled"
                                       class="btn">
                                        <i class="glyphicon glyphicon-ok-sign"  rel="tooltip" title="Salon Reviewed"> </i>
                                    </a>
                                    @endif


                                    @if( $salon->sal_is_reviewed ==0)
                                    <a title="Mark Reviewed"
                                       style=" border: none;
                                       color: red;
                                       background: #52C1F0;
                                       padding: 5px 10px;
                                       margin-left:10px;
                                       font-size: 20px;
                                       cursor: pointer;" 
                                       href="{{ url('setSalonInfo', ['id' => $salon->sal_id]) }}"
                                       class="btn"><i    rel="tooltip" title="Salon Reviewed" class="glyphicon glyphicon-sunglasses"></i></a>
                                    @endif
                                    @if( $salon->sal_is_reviewed ==1)
                                    <a title="Mark Reviewed"
                                       style=" border: none;
                                       color: white;
                                       background: #52C1F0;
                                       padding: 5px 10px;
                                       margin-left:10px;
                                       font-size: 20px;
                                       cursor: pointer;" 
                                       href="{{ url('setSalonInfo', ['id' => $salon->sal_id]) }}"
                                       class="btn"><i    rel="tooltip" title="Mark Salon isReviewed" class="glyphicon glyphicon-sunglasses"></i></a>
                                    @endif
                                    <a style=" border: none;
                                       color: white;
                                       background: #52C1F0;
                                       padding: 5px 10px;
                                       font-size: 20px;
                                       margin-left:10px;
                                       cursor: pointer;"
                                       href="{{ url('editsalon', ['id' => $salon->sal_id , 'type_id' => '-1','urls'=>$urls]) }}"
                                       class="btn">   <i class="glyphicon glyphicon-edit"></i> </a>
                                        <!--                        <td><div class="btn-group"><a href="{{ url('editsalon', ['id' => $salon->sal_id]) }}" class="btn btn-info" style="width:100%; margin-right:100px;">Edit</a>
                                         {{-- <a href="" class="btn btn-danger">Delete</a>
                                         <a href="" class="btn btn-warning">Send Email</a> --}}
                                        </div></td>-->
                        </tr>
                    <?php } ?>
                </table>
                
                {!! $salons->appends(['sal_search'=>isset($sal_search)? $sal_search:'','is_active'=>isset($is_active)? $is_active:'','sal_is_reviewed'=>isset($is_reviewed)? $is_reviewed:'','page'=>$salons->currentPage()])->links() !!}
                {{-- {{ $salons->links() }} --}}
                  
                  
            </div>
        </form>
    </div>
</section>
@endsection
@push("script")
      <script type="text/javascript">
          jQuery("#is_active").on("change",function(){
          var id = jQuery(this).val();
          // alert(id);
        });
      </script>
    @isset($is_reviewed)
      <script type="text/javascript">
        $("#sal_is_reviewed").val("{{$is_reviewed}}");
      </script>
    @endisset

    @isset($is_active)
      <script type="text/javascript">
        $("#is_active").val("{{$is_active}}");
      </script>
    @endisset

@endpush