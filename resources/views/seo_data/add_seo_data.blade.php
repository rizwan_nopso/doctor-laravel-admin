@php
    
    use App\Helpers\Helper;

    $helper = new Helper();
    $display_cust_img = $helper->display_cust_img();

  
@endphp

@extends('service-mgmt.base')
@section('action-content')
<style type="text/css">
    .checkbox label:after, 
.radio label:after {
    content: '';
    display: table;
    clear: both;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
</style>

<section class="content" style="margin-top: -40px;">
  <div class="row">
        <div class="col-sm-12">
            <h2>Add SEO Data</h2><br>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h2 class="">SEO Data </h2>
                </div>
                <div class="col-sm-4">
                    {{-- <a class="btn btn-info" href="">Add new Collection</a> --}}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                 @if (\Session::has('flash_message'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 23px !important; margin: -tio; margin-top: -12px; margin-right: -14px;"><span aria-hidden="true">&times;</span></button> 
                        {{ \Session::get('flash_message') }}
                    </div> 
                @endif
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            
            <div class="row" style="padding:20px;">
                @if(!empty($seo))
                                <form method="post" action="{{url('admin/update_seo'.'/'.$seo->seo_id)}}" enctype="multipart/form-data">
                            @else
                                <form method="post" action="{{url('admin/store_seo')}}" enctype="multipart/form-data">
                            @endif
                                {{csrf_field()}}

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                               <select name="seo_page_type" id="seo_page_type" class="form-control">
                                                    <option @isset($seo) @if($seo->seo_page_type == 'Home') selected="" @else @endif @endisset value="">Please Select Type</option>
                                                    <option @isset($seo) @if($seo->seo_page_type == 'Home') selected="" @else @endif @endisset value="Home">Home</option>
                                                    <option @isset($seo) @if($seo->seo_page_type == 'Product') selected="" @else @endif @endisset value="Product">Product</option>
                                                    <option @isset($seo) @if($seo->seo_page_type == 'Product_detail') selected="" @else @endif @endisset value="Product_detail">Product Detail</option>
                                                    <option @isset($seo) @if($seo->seo_page_type == 'Salon') selected="" @else @endif @endisset value="Salon">Salon</option>
                                                    <option @isset($seo) @if($seo->seo_page_type == 'Salon_detail') selected="" @else @endif @endisset value="Salon_detail">Salon Detail</option>
                                                    <option @isset($seo) @if($seo->seo_page_type == 'Beautytip') selected="" @else @endif @endisset value="Beautytip">Beautytip</option>
                                                    <option @isset($seo) @if($seo->seo_page_type == 'Beautytip_detail') selected="" @else @endif @endisset value="Beautytip_detail">Beautytip Detail</option>
                                                    <option @isset($seo) @if($seo->seo_page_type == 'Beautytrend') selected="" @else @endif @endisset value="Beautytrend">Beautytrend</option>
                                                    <option @isset($seo) @if($seo->seo_page_type == 'Beautytrend_detail') selected="" @else @endif @endisset value="Beautytrend_detail">Beautytrend Detail</option>
                                                    <option @isset($seo) @if($seo->seo_page_type == 'Fashiontrend') selected="" @else @endif @endisset value="Fashiontrend">Fashiontrend</option>
                                                    <option @isset($seo) @if($seo->seo_page_type == 'Fashiontrend_detail') selected="" @else @endif @endisset value="Fashiontrend_detail">Fashiontrend Detail</option>

                                                    <option @isset($seo) @if($seo->seo_page_type == 'salon_admin') selected="" @else @endif @endisset value="salon_admin">Salon Admin</option>
                                                    <option @isset($seo) @if($seo->seo_page_type == 'salon_login') selected="" @else @endif @endisset value="salon_login">Salon in</option>
                                               </select>
                                                 
                                            </div>
                                        </div>
                                    </div>

                                    {{-- <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="seo_main_title" required="" class="form-control" placeholder="Main Title" @isset($seo) value="{{$seo->seo_main_title}}" @endisset>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="seo_title" required="" class="form-control" placeholder="Ttile" @isset($seo) value="{{$seo->seo_title}}" @endisset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="seo_keywords" required="" class="form-control" placeholder="Keywords" @isset($seo) value="{{$seo->seo_keywords}}" @endisset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="seo_canonical" required="" class="form-control" placeholder="Canonical" @isset($seo) value="{{$seo->seo_canonical}}" @endisset>
                                            </div>
                                        </div>
                                    </div>
                                 

                                {{-- <div class="row clearfix"> --}}
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea rows="4" name="seo_description" required="" class="form-control " placeholder="Description" required="">@isset($seo){{$seo->seo_description}}@endisset</textarea>
                                            </div>
                                        </div>
                                    </div>
                                {{-- </div> --}}
                                
                                
                               
                                <div class="col-lg-12 p-t-20 text-center">
                                    {{-- @isset('faq') --}}
                                    <button type="submit" class="btn btn-primary waves-effect m-r-15">
                                        @if(!empty($seo))
                                            Update
                                        @else
                                            Submit
                                        @endif
                                    </button>
                                    <button type="clear" class="btn btn-danger waves-effect">Cancel</button>
                                </div>
                            </form>
            </div>

            <div class="row" style="margin-top: 50px; padding: 20px;">
                
            </div>
        </div>
    </div>
</section>
@endsection
@push('script')


@endpush