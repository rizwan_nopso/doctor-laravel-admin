@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $display_cust_img = $helper->display_cust_img();
  
@endphp

@extends('service-mgmt.base')
@section('action-content')

<section class="content" style="margin-top: -40px;">
  <div class="row">
        <div class="col-sm-12">
            <h3>SEO Data</h3><br>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">SEO Data Details </h3>
                </div>
                <div class="col-sm-4" style="text-align: right;">
                    <a class="btn btn-info" href="{{url('admin/add-seo')}}">Add new SEO Data</a>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover js-basic-example contact_list">
                                    <thead>
                                        <tr>
                                            <th class="center">#</th>
                                            <th class="center"> Title </th>
                                            <th class="center"> Canonical </th>
                                            <th class="center"> Keyword </th>
                                            <th class="center"> Page Type </th>
                                            <th class="center"> Description </th>
                                            <th class="center"> Date </th>
                                            <th class="center"> Action </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($seo))
                                        @foreach($seo as $key => $value)
                                        <tr class="odd gradeX">
                                            <td class="table-img center">
                                                {{$key+1}}
                                            </td>
                                            <td class="center">{{$value->seo_title}}</td>
                                            <td class="center">{{$value->seo_canonical}}</td>
                                            <td class="center">{{$value->seo_keywords}}</td>
                                            <td class="center">{{$value->seo_page_type}}</td>
                                            <td class="center">{{$value->seo_description}}</td>
                                            <td class="center">{{$value->seo_updated_at}}</td>
                                            <td class="center">
                                                <a style=" border: none;
                                             color: white;
                                             background: #52C1F0;
                                             cursor: pointer;" href="{{url('admin/edit_seo'.'/'.$value->seo_id)}}" class="btn">   <i class="glyphicon glyphicon-edit"></i> </a>
                                             <a href="{{url('admin/delete_seo'.'/'.$value->seo_id)}}" class="btn btn-danger incons" title="Delete">   
                                                  <i class="glyphicon glyphicon-remove"></i> 
                                              </a>
                                               
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                       
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th class="center">#</th>
                                            <th class="center"> Title </th>
                                            <th class="center"> Canonical </th>
                                            <th class="center"> Keyword </th>
                                            <th class="center"> Page Type </th>
                                            <th class="center"> Description </th>
                                            <th class="center"> Date </th>
                                            <th class="center"> Action </th>                                        </tr>
                                    </tfoot>
                                </table>

            {{-- {{$coupons->links()}} --}}

            
        </div>
    </div>
</section>
@endsection