@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $pc_image_path = $helper->pc_image_display();
   
@endphp

@extends('salon-magmt.base')
@section('action-content')

@push("css")
    <style type="text/css">
        .incons
        {
            border: none; 
            color: white; 
            padding: 5px 10px;
            font-size: 20px;
            margin-left:10px;
            cursor: pointer;"
        }
      .navbar {
        position: initial !important;
        display: block !important;
      }
      
    </style>
@endpush

<section class="content">

 <div class="row">
    <div class="col-sm-12">
      <h3 style="padding: 30px;">Add Salon Area</h3>
      <div class="row">
        <div class="col-sm-12">
             @if (Session::has('flash_message'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 23px !important; margin: -tio; margin-top: -12px; margin-right: -14px;"><span aria-hidden="true">&times;</span></button> 
                    {{ Session::get('flash_message') }}
                </div> 
            @endif
        </div>
    </div>
    </div>
  </div>

    <div class="row">
            <div class="col-sm-12 col-lg-12">
                <div class="box">
                    <div class="nav-tabs-custom">
                      <div class="box-header">
                          <h3 class="box-title"> 
                              Salon Area
                          </h3> 
                          <br>
                          <br>                                                     
                      </div>
                      <div class="box-body">
                          <div>
                              <div class="">
                                  @if(!empty($salon_area))
                                  <form role="form" method="post" action="{{url('update_area'.'/'.$salon_area->sal_a_id)}}" enctype="multipart/form-data">
                                @else
                                  <form role="form" method="post" action="{{url('store_area')}}" enctype="multipart/form-data">
                                @endif
                                      {{ csrf_field() }}
                                  <div style="margin:0 auto;
                                   width:100%;
                                   height:50px;
                                   " class="div1">
                                    <div style="margin:0 auto;
                                       height:50px;
                                       " class="div1">
                                      <div class="row">

                                        <div class="col-sm-6">
                                          <div class="form-group">
                                            <label>Cities</label>
                                            <select required="" name="sal_a_city" id="sal_a_city" class="form-control">
                                              <option value="">Please Select City</option>
                                              @foreach($city as $key => $value)
                                              @php $get_city = $value['get_city']; @endphp
                                              <optgroup label="{{$value['name']}}">
                                                @isset($get_city)
                                                @foreach($get_city as $key => $v)
                                                  <option @isset($salon_area) @if($salon_area->sal_a_city == $v['name']) selected="selected" @else @endif @endisset value="{{$v['name']}}">{{$v['name']}}</option>
                                                @endforeach
                                                @endisset
                                              @endforeach
                                            </select>
                                          </div>
                                        </div>

                                       {{--  <div class="col-sm-6" style="padding-right: 0px;">
                                          <div class="form-group">
                                            <label>
                                              City 
                                            </label>
                                            <input  type="text" class="form-control"  name="sal_a_city" @isset($sal_area) value="{{$sal_area->sal_a_city}}" @endisset placeholder="Enter City Name" required="">
                                          </div>
                                        </div> --}}

                                        <div class="col-sm-6" style="padding-right: 0px;">
                                          <div class="form-group">
                                            <label>
                                              Area 
                                            </label>
                                            <input  type="text" class="form-control"  name="sal_a_name" @isset($salon_area) value="{{$salon_area->sal_a_name}}" @endisset placeholder="Enter Area Name" required="">
                                          </div>
                                        </div>
                                        <div class="col-sm-6">
                                          <div class="form-group">
                                            <label>Status</label>
                                            <select required="" name="sal_a_status" id="sal_a_status" class="form-control">
                                              <option value="">Please Status</option>
                                              <option @isset($salon_area) @if($salon_area->sal_a_status == 1) selected="selected" @else @endif @endisset value="1">Active</option>
                                              <option @isset($salon_area) @if($salon_area->sal_a_status == 0) selected="selected" @else @endif @endisset value="0">Inactive</option>
                                            </select>
                                          </div>
                                        </div>
                                        <div class="col-sm-11" style="padding-right: 0px;">
                                          <div class="form-group">
                                            <label>
                                              Description 
                                            </label>
                                            <textarea class="form-control"  name="sal_a_description" rows="8" placeholder="Salon Area Desciption" required="">@isset($salon_area){{$salon_area->sal_a_description}} @endisset</textarea>
                                          </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="padding-right: 0px;">
                                          <div class="form-group">
                                              <label></label>
                                              <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;" name="submit"  class="btn btn-info" >
                                                @if(!empty($salon_area))
                                                  Update
                                                @else
                                                  Submit
                                                @endif

                                              </button>
                                          </div>
                                        </div>
                                      </div>       
                                    </div>
                                  </div>
                                </form>

                          </div>
                      </div>
                  </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
      </div> 
</section>
@endsection
@push("script")
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
<script>
  @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
  @endif
  @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
  @endif
  @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
  @endif
  @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
  @endif
</script>


@endpush



