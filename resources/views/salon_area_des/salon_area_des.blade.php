@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $pc_image_path = $helper->display_cust_img();
   
@endphp

@extends('salon-magmt.base')
@section('action-content')

@push("css")
    <style type="text/css">
        .incons
        {
            border: none; 
            color: white; 
            padding: 5px 10px;
            font-size: 20px;
            margin-left:10px;
            cursor: pointer;"
        }
        body{
          font-size: 1.5rem !important;
      }
      .btn-group-vertical>.btn-group:after, .btn-group-vertical>.btn-group:before, .btn-toolbar:after, .btn-toolbar:before, .clearfix:after, .clearfix:before, .container-fluid:after, .container-fluid:before, .container:after, .container:before, .dl-horizontal dd:after, .dl-horizontal dd:before, .form-horizontal .form-group:after, .form-horizontal .form-group:before, .modal-footer:after, .modal-footer:before, .modal-header:after, .modal-header:before, .nav:after, .nav:before, .navbar-collapse:after, .navbar-collapse:before, .navbar-header:after, .navbar-header:before, .navbar:after, .navbar:before, .pager:after, .pager:before, .panel-body:after, .panel-body:before, .row:after, .row:before {
            display: table;
            content: normal !important;
        }
    </style>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

@endpush

<section class="content">
  <div class="container" style="margin-top: 80px;">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    {{-- <hr class="my-5"> --}}

                    <div class="row pb-5 p-5">
                        <div class="col-6">
                            Salon Areas
                        </div>
                        <div class="col-6 text-right">
                            <a href="{{url('add-area')}}" class="btn btn-primary">Add Salon Area</a>
                        </div>
                    </div>

                    <div class="row p-5">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>  
                                   @if(!empty($salon_areas))
                                    <tr>
                                        <th class="">#</th>
                                        <th class="">Area</th>
                                        <th class="">City</th>
                                        <th class="">Description</th>
                                        <th class="">Status</th>
                                        <th class="">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    @foreach($salon_areas as $key => $value)
                                    <tr>
                                        <td>{{$key += 1}}</td>
                                        <td>{{$value->sal_a_name}}</td>
                                        <td >{{$value->sal_a_city}}</td>
                                        <td >{{$value->sal_a_description}}</td>

                                        <td> @if($value->sal_a_status == 1) Active @else Inactive @endif</td>
                                       
                                        <td style="text-align: center;">
                                            <a href="{{url('edit_area'.'/'.$value->sal_a_id)}}" class="btn btn-primary">Edit</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="5">Record Not Found !</td>
                                    </tr>
                                    @endif
                                    
                                </tbody>
                            </table>
                            {!! $salon_areas->links() !!}
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>
    </div>
    
</div>


</section>
@endsection
@push("script")

@endpush