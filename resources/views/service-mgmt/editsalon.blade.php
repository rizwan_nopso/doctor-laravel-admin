@extends('salon-magmt.base')

@section('action-content')
<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="box">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li id="one" class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Services</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Salon Info</a></li>
                    <li id="three" class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Add Service</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="box-header">
                            <h3 class="box-title">Edit Salon</h3>
                            <!--<p>All fields are required* </p>-->

                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('typeservices') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Salon Type</label>
                                            <select id="salon_type" name="salon_type" class="form-control">
                                                <?php foreach ($salon_types as $salon_type) { ?>
                                                    <option  value="<?php echo $salon_type->sty_id; ?>"> <?php echo $salon_type->sty_name; ?></option>;
                                                <?php }
                                                ?>
                                            </select>
                                            <input style="display: none;" type="submit" value="submit">
                                        </div>
                                    </form>
                                </div>
                                <?php
//                                $attributes = array("name" => "services", "id" => "service_form");
//                                echo form_open("admin/update_services/", $attributes);
                                ?>
                                <div class="">
                                    <div class="form-group">
                                        <label>Services</label>

                                        <div class="row">
                                            <?php
                                            foreach ($type_services as $value) {
//               
                                                ?>
                                                <div class="col-md-3">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="hidden" value="<?php echo $salon->sal_id; ?>" name="sal_id">
                                                            <input type="hidden" value="<?php echo $salon->sal_name; ?>" name="sal_name">
                                                            <input name="services[]" value=" <?php echo $value->ser_id . '-' . $value->ser_name; ?>" type="checkbox">
                                                            <?php echo $value->ser_name; ?>
                                                        </label>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button name="submit" type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <table class="table table-hover">
                            <tbody><tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>City</th>
                                </tr>
                                <tr>
                                    <td><?php echo $salon->sal_name; ?></td>
                                    <td><?php echo $salon->sal_email; ?></td>
                                    <td><?php echo $salon->sal_phone; ?></td>
                                    <td>
                                        <?php if ($salon->is_active == 1) { ?>
                                            <span class="label label-success">Active</span>
                                        <?php } else { ?>
                                            <span class="label label-danger">InActive</span>  
                                        <?php } ?>
                                    </td>
                                    <td><?php echo $salon->sal_city; ?></td>
                                </tr>
                            </tbody></table>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_3">

                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
@endsection
