@extends('layouts.app-template')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-md-9">
                <h2>
                    {{-- Services Categories Management --}}
                    {{$page_title}}
                </h2>
            </div>
        </div>
        <ol class="breadcrumb">
          <!-- li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li-->

        </ol>
    </section>
    @yield('action-content')
    <!-- /.content -->
</div>
@endsection