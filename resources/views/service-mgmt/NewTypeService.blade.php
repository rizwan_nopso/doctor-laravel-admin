
@extends('service-mgmt.base')
@section('action-content')


<section class="content">
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Add New type Service</h3>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!-- <form action="{{ url('AddnewTypeService') }}" method="POST" role="form" enctype="multipart/form-data">
                {{ csrf_field() }} -->

<form action="{{ url('saveNewTypeService')}}" method="POST" role="form" enctype="multipart/form-data">
           {{ csrf_field() }}

                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Salon Type Name</label>
                        <input required="" type="text" name="sty_name" class="form-control" id="exampleInputEmail1" placeholder="Enter Salon Name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Status</label>
                        <select name="sty_status" class="form-control">
                            <option value="1">Active</option>
                            <option value="0">In Active</option>
                        </select>
                    </div>
                    <!-- <div class="form-group">
                        <label for="exampleInputFile">Add Image</label>
                        <input name="cat_image" type="file" id="exampleInputFile">
                    </div> -->
                </div>
                <!-- /.box-body -->
                @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                @endif
                <div  style="border:1pt;" class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                      <a style=" margin-left: 15px; margin-right: 8px;" class="btn btn-primary" href="{{ url('salon_types') }}">Cancel</a>
                     
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection