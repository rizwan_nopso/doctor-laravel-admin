@extends('salon-magmt.base') 
@section('action-content')

@push("css")
    <style type="text/css">

            input[type="file"]{
                    /*color: transparent;*/
                }
                .optionGroup {
                    font-weight: bold;
                    font-style: italic;
                }
                    
                .optionChild {
                    padding-left: 15px !important;
                }
    </style>
@endpush
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Manage Category Association</h3><br>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-lg-12">
            <div class="box">
                <!-- <div class="nav-tabs-custom"> -->
                
                    <!-- <div class="tab-content"> -->
                        
                            <div class="box-header">
                                <h3 class="box-title"> 
                                Select base category for association of {{$sc_name}}


                                </h3> 
                                <!--<p>All fields are required* </p>-->
                            </div>
                            <div class="box-body">
                                <div>
                                    <div class="">
                                        <form action="{{ url('admin/updateSalonSerSubCate') }}" method="POST" role="form" enctype="multipart/form-data">
                                            <input type="hidden" name="sscids" value="{{$ssc_id}}">
                                            <input type="hidden" name="returnback" value="{{$returnback}}">
                                            {{ csrf_field() }}
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <!-- <label for="exampleInputEmail1">Select base category for association</label> -->
                                                    <!-- <input required="" type="text" name="cat_name" class="form-control" id="exampleInputEmail1" placeholder="Enter file Name"> -->
                                                    <select class="form-control" name="ssc_id">
                                                       
                                                            @foreach($category_association as $key => $value)
                                                            <option class="optionGroup"  value="sc_id,{{$value->sc_id}}" @if($value->sc_id == $ssc_id ) selected="checked" @endif >{{$value->sc_name}}</option>
                                                                    <!-- <optgroup label=""> -->
                                                                        @foreach($value->service_sub_category as $key => $ssc)
                                                                            <option class="optionChild"  value="ssc_id,{{$ssc->ssc_id}}" @if($value->ssc_id == $ssc_id ) selected="checked" @endif > &nbsp;&nbsp;&nbsp;{{$ssc->ssc_name}}</option>
                                                                        @endforeach
                                                                    <!-- </optgroup> -->
                                                               
                                                            @endforeach
                                                    </select>
                                                
                                                </div>
                                                <div class="form-group">
                                                </div>
                                               {{-- <div class="form-group">
                                                    <label for="exampleInputFile">Add File</label>
                                                    <input required="" name="cat_image" type="file" id="exampleInputFile" class="form-control" onclick="fileClicked(event)" onchange="fileChanged(event)">


                                                </div>  --}}
                                            </div>
                                            <!-- /.box-body -->
                                            @if(Session::has('message'))
                                            <div class="alert alert-success" id="message"><span class="glyphicon glyphicon-ok"></span><em> {!! session('message') !!}</em></div>
                                            <br>
                                            <br>
                                            @endif

                                            @if(Session::has('error_message'))
                                                <div class="alert alert-danger">
                                                    <span class="glyphicon glyphicon-crox"></span>
                                                    <em> {!! session('error_message') !!}</em>
                                                </div>
                                                <br>
                                                <br>
                                            @endif


                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                <a hidden="" class="btn btn-primary" style="margin-left: 10px;" href="{{ $returnback }}" >Cancel</a>
                                            </div>
                                        </form>

                                    </div>
                                    <?php
//                                $attributes = array("name" => "services", "id" => "service_form");
//                                echo form_open("admin/update_services/", $attributes);
                                    ?>

                                    <!-- <form role="form" method="POST" action="{{ url('type_services_update') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                    </form> -->

                                </div>
                            </div>
                        
                        <!-- /.tab-pane -->


                    <!-- </div> -->
                    <!-- /.tab-content -->
                <!-- </div> -->
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    </section>
@endsection

@push("script")
    <script type="text/javascript">
        // $(function() {
        //     $('a[data-toggle="tab"]').on('click', function(e) {
        //         window.localStorage.setItem('activeTab', $(e.target).attr('href'));
        //     });
        //     var activeTab = window.localStorage.getItem('activeTab');
        //     if (activeTab) {
        //         $('#myTab a[href="' + activeTab + '"]').tab('show');
        //         window.localStorage.removeItem("activeTab");
        //     }
        // });

            

    </script>


                                  {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
<script>
    //This is All Just For Logging:
    var debug = true;//true: add debug logs when cloning
    var evenMoreListeners = true;//demonstrat re-attaching javascript Event Listeners (Inline Event Listeners don't need to be re-attached)
    if (evenMoreListeners) {
        var allFleChoosers = $("input[type='file']");
        addEventListenersTo(allFleChoosers);
        function addEventListenersTo(fileChooser) {
            fileChooser.change(function (event) { console.log("file( #" + event.target.id + " ) : " + event.target.value.split("\\").pop()) });
            fileChooser.click(function (event) { console.log("open( #" + event.target.id + " )") });
        }
    }
    (function () {
        var old = console.log;
        var logger = document.getElementById('log');
        console.log = function () {
            for (var i = 0; i < arguments.length; i++) {
                if (typeof arguments[i] == 'object') {
                    logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
                } else {
                    logger.innerHTML += arguments[i] + '<br />';
                }
            }
            old.apply(console, arguments);
        }
    })();

    var clone = {};

    // FileClicked()
    function fileClicked(event) {
        var fileElement = event.target;
        if (fileElement.value != "") {
            // if (debug) { console.log("Clone( #" + fileElement.id + " ) : " + fileElement.value.split("\\").pop()) }
            clone[fileElement.id] = $(fileElement).clone(); //'Saving Clone'
        }
        //What ever else you want to do when File Chooser Clicked
    }

    // FileChanged()
    function fileChanged(event) {
        var fileElement = event.target;
        if (fileElement.value == "") {
            // if (debug) { console.log("Restore( #" + fileElement.id + " ) : " + clone[fileElement.id].val().split("\\").pop()) }
            clone[fileElement.id].insertBefore(fileElement); //'Restoring Clone'
            $(fileElement).remove(); //'Removing Original'
            if (evenMoreListeners) { addEventListenersTo(clone[fileElement.id]) }//If Needed Re-attach additional Event Listeners
        }
        //What ever else you want to do when File Chooser Changed
    }



        $(function () {
             $('input[type="file"]').change(function () {
                  if ($(this).val() != "") {
                         $(this).css('color', '#333');
                  }else{
                         $(this).css('color', 'transparent');
                  }
             });
        })
    </script>

    <script type="text/javascript">
        $(function() {
          $('input[type=file]').change(function(){
            var t = $(this).val();
            var labelText = 'File : ' + t.substr(12, t.length);
            $(this).prev('label').text(labelText);
          })
        });


// active tabs keep selected tabs bootstraps
        $('a[data-toggle="tab"]').click(function (e) {
          e.preventDefault()
          $(this).tab('show')
        });
        $(function () {
          // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
          $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('lastTab', $(this).attr('href'));
          });
          // go to the latest tab, if it exists:
          var lastTab = localStorage.getItem('lastTab');
          if (lastTab) {
            $('#myTab li.active,.tab-pane.active').removeClass('active');
            $('#myTab a[href="' + lastTab + '"]').closest('li').addClass('active');
            $(lastTab).addClass('active');
          }
        });


        setTimeout(function(){$('#message').fadeOut();}, 3000);
        setTimeout(function(){ @php session()->forget('message'); @endphp }, 3000);

    </script>



@endpush

