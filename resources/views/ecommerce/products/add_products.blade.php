@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $pc_image_path = $helper->pc_image_display();
     // dd($pvpb); 

   
@endphp

@extends('salon-magmt.base') 
@section('action-content')
@push("css")
    <style type="text/css">
        .incons{
            border: none; 
            color: white; 
            padding: 5px 10px;
            font-size: 20px;
            margin-left:10px;
            cursor: pointer;"

        }

        input[type="file"]{
                    /*color: transparent;*/
                }
    .collapsing {
          transition: none !important;
          -webkit-transition: none;
            transition: none;
            display: none;
        }
    .remove-me{
        margin-left: 13px !important;
    }
    .add_more{
        width: 89% !important;
    }
    .submit{
        margin-left: 18px;
        margin-top: 30px;
    }
    </style>

@endpush
<section class="content">
<div id="success"></div>
    <div class="row">
        <div class="col-lg-12" style="margin-bottom: 20px;">
            <h2>Edit Product</h2>
        </div>
        <div class="col-sm-12 col-lg-12">
            <div class="box">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" id="myTab">
                        
                        <li class="@if(Session::get('active_tabs')=='edit_products') active @endif">
                            <a href="#tab_1" data-toggle="tab" role="tab" aria-controls="tab_1" aria-expanded="false">
                                Product Info
                            </a>
                        </li>
                        @isset($product)
                        <li id="three" class="">
                            <a href="#tab_2" data-toggle="tab" class="@if(Session::get('active_tabs')=='store_product_images') active @endif" role="tab" aria-controls="tab_2" aria-expanded="false">
                                Product Images
                            </a>
                        </li>

                        <li id="one" class="@if(Session::get('active_tabs')=='product_color_images') active @endif">
                            <a href="#tab_3" data-toggle="tab" role="tab" aria-controls="tab_3" aria-expanded="true">
                                Product Colors 
                            </a>
                        </li>
                        @endisset
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane @if(Session::get('active_tabs')=='product_color_images') active @endif" id="tab_3">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs" id="myTab">
                                    
                                    <li class="active">
                                        <a href="#tab_cl_img" data-toggle="tab" role="tab" aria-controls="tab_cl_img" aria-expanded="false">
                                            Product Color Images
                                        </a>
                                    </li>
                                   
                                    <li id="three" class="">
                                        <a href="#tab_cl_value" data-toggle="tab" class="" role="tab" aria-controls="tab_cl_value" aria-expanded="false">
                                            Product Color Values
                                        </a>
                                    </li>
                                </ul>
                                 <div class="tab-content">
                                        <div class="tab-pane active" id="tab_cl_img"> 
                                            <div class="box-header">
                                                <h3 class="box-title"> 
                                                    
                                                </h3>
                                                <div class="col-lg-12">
                                                    @if(session()->has('message'))
                                                        <div class="alert alert-success">
                                                            {{ session()->get('message') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="box-body">
                                                <form id="color_images" method="post" enctype="multipart/form-data">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="p_id" @isset($p_id) value="{{$p_id}}" @endisset>
                                                     <div class="col-sm-12">
                                                        <div class="form-group gallery">
                                                          <label>Color Images</label>
                                                            <div class="form-group">      
                                                                <div class="form-group dropzone" id="dropzone1">
                                                                </div>       
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <div class="form-group">
                                                            <input type="submit" name="submit" value="Submit" class="btn btn-primary" id="submits">
                                                        </div>
                                                    </div>
                                                </form>
                                           
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        @isset($p_colors)
                                                            @foreach($p_colors as $key => $value)
                                                                <div class="col-xs-3">
                                                                    <div class="form-group">
                                                                        <img src="{{$pc_image_path}}{{$value->pcl_image}}" width="200px" height="200px"> <br>
                                                                        <a href="{{url('admin/delete_color_images'.'/'.$value->pcl_id)}}" >Remove Image</a>
                                                                        {{-- <a href=""> --}}
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @endisset
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_cl_value">
                                             <div class="box-header">
                                            <h3 class="box-title"> 
                                                
                                            </h3>
                                            <div class="col-lg-12">
                                                @if(session()->has('message'))
                                                    <div class="alert alert-success">
                                                        {{ session()->get('message') }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="box-body">
                                            <form name="myForm" onsubmit="return validateForm()"  method="post" action="{{route('store_pcl',['id'=>$p_id])}}" enctype="multipart/form-data">
                                                {{csrf_field()}}

                                                <div class="col-xs-12">
                                                    <div id="education_fields">
                      
                                                    </div>
                                                <div class="input-group">
                                                    <div class="input-group-btn">
                                                    <input type="hidden" name="p_id[]" @isset($p_id) value="{{$p_id}}" @endisset>
                                                    <div class="col-sm-3 nopadding">
                                                      <div class="form-group">
                                                        <input type="text" class="form-control" id="pcl_code_0" required="" name="pcl_code[]" value="" placeholder="Product Color Code" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Product Color Code'">
                                                      </div>
                                                    </div>
                                                    <div class="col-sm-4 nopadding">
                                                      <div class="form-group"> 
                                                        
                                                        <input type="text" class="form-control" id="pcl_value_0" name="pcl_value[]"  value="" placeholder="Product Color Value (optional)" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Product Color Value (optional)'">
                                                      </div>
                                                    </div>
                                                    <div class="col-sm-4 nopadding">
                                                      <div class="form-group">
                                                        
                                                      </div>
                                                    </div>
                                                    <button class="btn btn-success" type="button"  onclick="education_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
                                                    </div>
                                                </div>

                                                </div>
                                                <br><br>
                                                <div class="col-lg-12">
                                                    <input type="submit" class="submit btn btn-primary bnt-lg" value="Submit">
                                                </div>
                                            </form>
                                        </div>

                                         </div>
                                 </div>

                            </div>
                        </div>
                        <!-- /.tab-pane -->

                        <div class="tab-pane @if(Session::get('editsalon')=='salon-info') active @endif" id="tab_1">
                            <!-- /.box-header -->
                            <div class="box-body">
                                @if(isset($product))
                                     <form action="{{route('update_product',['id' => $product->p_id])}}" method="POST" role="form" enctype="multipart/form-data">
                                @else
                                    <form action="{{route('store_pproduct')}}" method="POST" role="form" enctype="multipart/form-data">
                                @endif
                                    {{ csrf_field() }}
                                    <input type="hidden" name="return_back" @isset($return_back) value="{{$return_back}}" @endisset>

                                    <input type="hidden" name="pid" id="pid" @isset($products->p_id) value="{{$product->p_id}}" @endisset>
                                    <div class="box-body">

                                        <div class="col-sm-6">
                                             <div class="form-group">
                                                <label for="exampleInputPassword1">Name</label>
                                                <input required="" type="text" placeholder="Product Name" class="form-control" name="p_name" @isset($product->p_name) value="{{$product->p_name}}" @endisset>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Status</label>
                                                <select name="p_status" class="form-control" required="">
                                                    <option value="1" @isset($product->p_status) @if($product->p_status == 1) selected="checked" @endif @endisset >Active</option>
                                                    <option value="0" @isset($product->p_status) @if($product->p_status == 0) selected="checked" @endif @endisset >Inactive</option>
                                                </select>
                                            </div>
                                        </div> 
                                        <div class="col-sm-6">
                                             <div class="form-group">
                                                <label for="exampleInputPassword1">Cost</label>
                                                <input required="" type="text" min="0" placeholder="Product Cost" class="form-control" name="p_cost" id="p_cost" @isset($product->p_cost) value="{{$product->p_cost}}" @endisset>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                             <div class="form-group">
                                                <label for="exampleInputPassword1">Retail Price</label>
                                                <input required="" type="number" min="0" placeholder="Product Retail Price" class="form-control" id="p_rrp" name="p_rrp" @isset($product->p_rrp) value="{{$product->p_rrp}}" @endisset>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                             <div class="form-group">
                                                <label for="exampleInputPassword1">Price</label>
                                                <input required="" type="number" min="0" id="p_price" placeholder="Product Price" class="form-control" name="p_price" @isset($product->p_price) value="{{$product->p_price}}" @endisset>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                             <div class="form-group">
                                                <label for="exampleInputPassword1">Weight</label>
                                                <input required="" type="text" placeholder="Product Weight" class="form-control" name="p_weight" @isset($product->p_weight) value="{{$product->p_weight}}" @endisset>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                             <div class="form-group">
                                                <label for="exampleInputPassword1">Dimention</label>
                                                <input required="" type="text" placeholder="Product Dimention" class="form-control" name="p_dimention" @isset($product->p_rrp) value="{{$product->p_dimention}}" @endisset>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                             <div class="form-group">
                                                <label for="exampleInputPassword1">Vendor Code</label>
                                                <input required="" type="text" placeholder="Product vendor code" class="form-control" name="pv_code" @isset($product->pv_code) value="{{$product->pv_code}}" @endisset>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                             <div class="form-group">
                                                <label for="exampleInputPassword1">Sales Tax</label>
                                                <input required="" type="text" placeholder="Product sales tax" class="form-control" name="p_sales_tax" @isset($product->p_sales_tax) value="{{$product->p_sales_tax}}" @endisset>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Category</label>
                                                <select name="pc_id" class="form-control">
                                                    @isset($pc)
                                                        @foreach($pc as $key => $value)
                                                            <option value="{{$value->pc_id}}" @if($value->pc_id == $pc_id) selected="selected" @endif>{{$value->pc_name}}</option>
                                                        @endforeach
                                                    @endisset
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Vendors</label>
                                                <select name="pvpb" id="pvpb" class="form-control">
                                                    {{-- @php dd($pvpb); @endphp --}}
                                                    @isset($pvpb)
                                                        @foreach($pvpb as $key => $value)
                                                            <optgroup label="{{$value['pv_name']}}">
                                                                @if(isset($value['product_brand']))
                                                                @foreach($value['product_brand'] as $key => $pvendors)
                                                                    <option value="{{$value['pv_id'].','.$pvendors['pb_id']}}" @isset($product) @if(($value['pv_id'].','.$pvendors['pb_id']) == ($product->pv_id.','.$product->pb_id)) selected="selected" @endif @endisset>{{$pvendors['pb_name']}}</option>
                                                                @endforeach
                                                                @endisset
                                                            </optgroup>
                                                        @endforeach
                                                    @endisset
                                                </select>
                                            </div>
                                        </div>
                                       {{--  @isset($product)
                                        @push('script')
                                            <script type="text/javascript">
                                                $("#pvpb").val("{{$product->pv_id,$product->pb_id}}");
                                            </script>
                                        @endpush
                                        @endisset --}}


                                        {{-- <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Vendors</label>
                                                <select name="pv_id" class="form-control" id="vendor_id" required="">
                                                    <option value="">Please Select Vendor</option>
                                                    @isset($pv)
                                                        @foreach($pv as $key => $value)
                                                            <option @isset($product->pv_id) @if($value->pv_id === $product->pv_id) selected="" @endif @endisset value="{{$value->pv_id}}">{{$value->pv_name}}</option>
                                                        @endforeach
                                                    @endisset
                                                </select>
                                            </div>
                                        </div> --}}

                                        {{-- <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Brands</label>
                                                <select name="pb_id" class="form-control" id="pb_id">
                                                    <option value="">Please Select Brand</option>
                                                    @isset($product->pb_id)
                                                        @foreach($pb as $key => $value)
                                                            <option @isset($product->pb_id) @if($value->pb_id === $product->pb_id) selected="" @endif @endisset value="{{$value->pb_id}}">{{$value->pb_name}}</option>
                                                        @endforeach
                                                    @endisset
                                                </select>
                                            </div>
                                        </div> --}}

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="">Image</label>
                                                <input type="file" name="p_image" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="">Vendor Name</label>
                                                <input type="text" required="" name="p_vendor_name" class="form-control" @isset($product) value="{{$product->p_vendor_name}}" @endif>
                                            </div>
                                        </div>
                                       
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Description</label>
                                                <textarea required="" placeholder="Product Description" rows="7" class="form-control" id="summary-ckeditor" name="p_description">@isset($product->p_description) {{$product->p_description}} @endisset</textarea>
                                            </div>
                                        </div>
                                         

                                    </div>
                                    <input type="hidden" name="returnback" value="{{$return_back}}">
                                    @if(Session::has('flash_message'))
                                    <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                                    @endif
                                    <div class="box-footer">
                                        
                                        @if(isset($product->p_id))
                                            <input type="submit" name="save" id="submit" class="btn btn-primary" value="Update">
                                        @else
                                            <input name="save" type="submit" id="submit" class="btn btn-primary" value="Submit">
                                        @endif
                                        <input name="save" type="reset" class="btn btn-primary" value="Clear">
                                        <a style="margin-left: 10px;" href="{{url($return_back)}}" class="btn btn-primary" >Cancel</a>
                                    </div>
                                </form>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="box-header">
                                <h3 class="box-title"> 
                                </h3>
                                <br><br>
                            </div>
                            <div class="box-body">
                                <form class="" method="post" enctype="multipart/form-data" id="addform">
                                    {{csrf_field()}}
                                     <input type="hidden" name="p_id" id="p_id">
                                    <input type="hidden" name="product_id" id="product_id" @isset($product) value="{{$product->p_id}}" @endisset>

                                    <div class="col-sm-12">
                                        <div class="form-group gallery">
                                          <label>Images</label>
                                            <div class="form-group">      
                                            <div class="form-group dropzone" id="dropzone">
                                            </div>       
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-primary" value="Submit" id="submit">
                                        </div>
                                    </div>
                                </form>

                                 <div class="col-xs-12">
                                         <p>
                                            @isset($pcl_images)
                                            @foreach($pcl_images as $key => $value)
                                            <div class="col-lg-2">
                                                <img src="{{$pc_image_path}}{{$value->pi_name}}" width="150px" height="100px"><br>
                                                <a href="{{route('delete_images',['id'=>$value->pi_id])}}" style="text-align: center;">Remove Image</a>
                                            </div>
                                            @endforeach
                                            @endif
                                        </p>
                                    </div>

                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    

       
       
    </section>
@endsection

@push('script')

<link rel="stylesheet" type="text/css" href="{{asset('css/dropzone.css')}}">
<script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>


<script type="text/javascript">

    $("#vendor_id").on('change',function(){
        var vendor_id = $(this).val();
                $.ajax({
                    url:"{{url('ajaxMetho')}}",
                    method:"GET",
                    data:{vendor_id:vendor_id},
                    cache:false,
                    success:function(data){
                        $("#pb_id").empty();
                        $("#pb_id").append(data);
                    }
                });
            });

    $(document).ready(function(){
        var vendor_id = $("#vendor_id").val();
        if(vendor_id != ''){
            // alert(vendor_id);
            $.ajax({
                    url:"{{url('ajaxMetho')}}",
                    method:"GET",
                    data:{vendor_id:vendor_id},
                    cache:false,
                    success:function(data){
                        $("#pb_id").empty();
                        $("#pb_id").append(data);
                    }
                });
        }
    })



    $('#addform').submit(function(e){
        var button = $('#submit');
        button.prop('disabled', true);

        e.preventDefault();
        formData=$(this).serialize();
        //alert('data got ');    
          $.ajax({
            type:"POST",
            url: "{{url('/get_product_id/')}}",
          data:new FormData(this),
          contentType:false,
          cache:false,
          processData:false,
          success:function(data)
          {
            document.getElementById("p_id").value = data;
            // alert(data);
            //document.getElementById('hidden_one').value = data;
            myDropzone.processQueue();
          }
        });
      });
       if(typeof Dropzone != 'undefined')
       {
        
           Dropzone.autoDiscover = false;
           var myDropzone = new Dropzone("#dropzone", {
             url: "{{url('admin/save_product_images/')}}",
             method:"POST",
             autoProcessQueue:false,
             required:true,
             acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
             addRemoveLinks: true,
             maxFiles:8,
             parallelUploads : 100,
             maxFilesize:5,
             init: function() {
                // Update the total progress bar
                this.on("totaluploadprogress", function(progress) {
                   // document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
                 });
                 this.on("queuecomplete", function(progress) {
                   // document.querySelector("#total-progress").style.opacity = "0";
                 });
                this.on("sending", function(file, xhr, formData)
                 {
                  // document.querySelector("#total-progress").style.opacity = "1";
                  formData.append("p_id",document.getElementById('product_id').value); 
                  formData.append('_token', '{{ csrf_token() }}'); 
                 });

               this.on('success', function(file, responseText)
                {                   
                    // alert(responseText);
                });

             this.on("complete", function (file)
                         {
                            // alert(file);
                      if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) 
                          {   
                            alert("image saved successfully");
                            location.reload();
                          }
                        });   
            }

        });
      };
// add color images in databse 
    $('#color_images').submit(function(e){
        var button = $('#submits');
        button.prop('disabled', true);

        e.preventDefault();
        formData=$(this).serialize();
        // alert('data got ');    
          $.ajax({
            type:"POST",
            url: "{{url('/get_product_id_for_colors/')}}",
          data:new FormData(this),
          contentType:false,
          cache:false,
          processData:false,
          success:function(data)
          {
            document.getElementById("p_id").value = data;
            // alert(data);
            //document.getElementById('hidden_one').value = data;
            myDropzoneTheFirst.processQueue();
          }
        });
      });
       if(typeof Dropzone != 'undefined')
       {
        
           Dropzone.autoDiscover = false;
           var myDropzoneTheFirst = new Dropzone("#dropzone1", {
             url: "{{url('admin/save_product_colors_images/')}}",
             method:"POST",
             autoProcessQueue:false,
             required:true,
             acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
             addRemoveLinks: true,
             maxFiles:100,
             parallelUploads : 100,
             maxFilesize:5,
             init: function() {
                // Update the total progress bar
                this.on("totaluploadprogress", function(progress) {
                   // document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
                 });
                 this.on("queuecomplete", function(progress) {
                   // document.querySelector("#total-progress").style.opacity = "0";
                 });
                this.on("sending", function(file, xhr, formData)
                 {
                  // document.querySelector("#total-progress").style.opacity = "1";
                  formData.append("p_id",document.getElementById('product_id').value); 
                  formData.append('_token', '{{ csrf_token() }}'); 
                 });

               this.on('success', function(file, responseText)
                {                   
                    // alert(responseText);
                });

             this.on("complete", function (file)
                         {
                            // alert(file);
                      if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) 
                          {   
                            alert("image saved successfully");
                            location.reload();
                          }
                        });   
            }

        });
      };
</script>



    <script type="text/javascript">
        $(function() {
            $('a[data-toggle="tab"]').on('click', function(e) {
                window.localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = window.localStorage.getItem('activeTab');
            if (activeTab) {
                $('#myTab a[href="' + activeTab + '"]').tab('show');
                window.localStorage.removeItem("activeTab");
            }
        });
    </script> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


<script>
    
  @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
  @endif
  @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
  @endif
  @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
  @endif
  @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
  @endif


</script>


<script type="text/javascript">  
    

      $(".ssubc_id").on("change",function(){
          var ssubc_id = $(this).val();
          // alert(ssc_id);
          var array = ssubc_id.split(",");
          var ssubc_id = array[0];
          var sser_id = array[1];

          $.post('{{ url('admin/changeservice_category') }}' , {ssubc_id: ssubc_id,sser_id:sser_id, _token: '{{ csrf_token() }}' } , function(data){
                  $("#success").html(data);
                  $('#me').fadeIn('fast').delay(1000).fadeOut('slow');
          }); 
        });
  </script>

  <script type="text/javascript">
        $(function () {
             $('input[type="file"]').change(function () {
                  if ($(this).val() != "") {
                         $(this).css('color', '#333');
                  }else{
                         $(this).css('color', 'transparent');
                  }
             });
        })
    </script>


<script>
    //This is All Just For Logging:
    var debug = true;//true: add debug logs when cloning
    var evenMoreListeners = true;//demonstrat re-attaching javascript Event Listeners (Inline Event Listeners don't need to be re-attached)
    if (evenMoreListeners) {
        var allFleChoosers = $("input[type='file']");
        addEventListenersTo(allFleChoosers);
        function addEventListenersTo(fileChooser) {
            fileChooser.change(function (event) { console.log("file( #" + event.target.id + " ) : " + event.target.value.split("\\").pop()) });
            fileChooser.click(function (event) { console.log("open( #" + event.target.id + " )") });
        }
    }
    (function () {
        var old = console.log;
        var logger = document.getElementById('log');
        console.log = function () {
            for (var i = 0; i < arguments.length; i++) {
                if (typeof arguments[i] == 'object') {
                    logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
                } else {
                    // logger.innerHTML += arguments[i] + '<br />';
                }
            }
            old.apply(console, arguments);
        }
    })();

    var clone = {};

    // FileClicked()
    function fileClicked(event) {
        var fileElement = event.target;
        if (fileElement.value != "") {
            // if (debug) { console.log("Clone( #" + fileElement.id + " ) : " + fileElement.value.split("\\").pop()) }
            clone[fileElement.id] = $(fileElement).clone(); //'Saving Clone'
        }
        //What ever else you want to do when File Chooser Clicked
    }

    // FileChanged()
    function fileChanged(event) {
        var fileElement = event.target;
        if (fileElement.value == "") {
            // if (debug) { console.log("Restore( #" + fileElement.id + " ) : " + clone[fileElement.id].val().split("\\").pop()) }
            clone[fileElement.id].insertBefore(fileElement); //'Restoring Clone'
            $(fileElement).remove(); //'Removing Original'
            if (evenMoreListeners) { addEventListenersTo(clone[fileElement.id]) }//If Needed Re-attach additional Event Listeners
        }
        //What ever else you want to do when File Chooser Changed
    }



        $(function () {
             $('input[type="file"]').change(function () {
                  if ($(this).val() != "") {
                         $(this).css('color', '#333');
                  }else{
                         $(this).css('color', 'transparent');
                  }
             });
        })

        // active tabs keep selected tabs bootstraps
        $('a[data-toggle="tab"]').click(function (e) {
          e.preventDefault()
          $(this).tab('show')
        });
        $(function () {
          // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
          $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('lastTab', $(this).attr('href'));
          });
          // go to the latest tab, if it exists:
          var lastTab = localStorage.getItem('lastTab');
          if (lastTab) {
            $('#myTab li.active,.tab-pane.active').removeClass('active');
            $('#myTab a[href="' + lastTab + '"]').closest('li').addClass('active');
            $(lastTab).addClass('active');
          }
        });
    </script>


<script language="javascript">
    function check_sub_categories(obj_category){ 
       var checkboxes = document.getElementsByClassName(obj_category.className); //checkbox items
         for (i = 0; i < checkboxes.length; i++) {

             checkboxes[i].checked = obj_category.checked;
         }
         for (var i = 0; i < checkboxes.length; i++) {
        checkboxes[i].addEventListener('change', function(e){ //".checkbox" change 
            //uncheck "select all", if one of the listed checkbox item is unchecked
            if(this.checked == false){
                obj_category.checked = false;
            }
            //check "select all" if all checkbox items are checked
            if(document.querySelectorAll('.checkbox:checked').length == checkboxes.length){
                obj_category.checked = true;
            }
        });
    }
  }
</script>

<script type="text/javascript">
    
</script>


    <script type="text/javascript">
       function check_sub_cat(obj_category){ 
       var checkboxes = document.getElementsByClassName(obj_category.className);
            // alert(checkboxes.length);
            var number = null;
           for(var i = 0; i < checkboxes.length; i++) {
                if(checkboxes[i].checked == obj_category.checked)
                {
                    if(obj_category.checked)
                    {
                        number++;
                    }
                }
           }
           if(checkboxes.length == (number+1))
           {
                checkboxes[0].checked = "checked";
           }
           else
           {
                checkboxes[0].checked = "";
           }
        };


// var prodcutColorsArray = []
$(document).ready(function () {
    //@naresh action dynamic childs
    var next = 0;
    $("#add-more").click(function(e){
        e.preventDefault();
        // let elem = document.getElementsByClassName('pcl_code').value();
        // alert(elem);
        // let productId = 
        // let productColorCode = 
        // let productColorValue = 
        // let productColorImage = 
        // let objProductColor = {
        //     pcl_code: productColorCode,
        //     pcl_image: productColorImage,
        //     pcl_value: productColorValue,
        //     p_id:  productId          
        // }
        // this.prodcutColorsArray.push(objProductColor)

        var addto = "#field" + next;
        var addRemove = "#field" + (next);
        next = next + 1;
        var newIn = ' <div id="field'+ next +'" name="field'+ next +'"> <input type="hidden" name="p_id[]" @isset($p_id) value="{{$p_id}}" @endisset> <div class="col-md-3"> <div class="form-group"> <label class="control-label" for="action_name">Product Color Code</label> <input id="pcl_code" required="" name="pcl_code[]" type="text" placeholder="Product Color Code" class="pcl_code form-control input-md" required=""> </div> </div> <div class="col-md-3"> <div class="form-group"> <label class="control-label" for="action_name">Product Color Values (optional)</label> <input id="pcl_value" name="pcl_value[]" type="text" placeholder="Product Color Values" class="form-control input-md"> </div> </div> <div class="col-md-3"> <div class="form-group"> <label class="control-label" for="action_json">Product Color Image (optional)</label> <input type="file" class="form-control" id="pcl_image[]" name="pcl_image[]" class="input-file"> </div> </div></div>';
        var newInput = $(newIn);
        var removeBtn = '<div class="col-md-3"><button id="remove' + (next - 1) + '" class="btn btn-danger remove-me" >Remove</button></div></div></div><div id="field">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#field" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
    });

});


var cnt=1;

var room = 1;
function education_fields() {
 
    room++;
    var objTo = document.getElementById('education_fields')
    var divtest = document.createElement("div");
    divtest.setAttribute("class", "form-group removeclass"+room);
    var rdiv = 'removeclass'+room;
    divtest.innerHTML = '<div class="input-group"><div class="input-group-btn"><input type="hidden" name="p_id[]" @isset($p_id) value="{{$p_id}}" @endisset><div class="col-sm-3 nopadding"> <div class="form-group"> <input type="text" class="form-control" id="pcl_code_'+cnt+' required="" name="pcl_code[]" value="" placeholder="Product Color Code"> </div></div><div class="col-sm-4 nopadding"> <div class="form-group"> <input type="text" class="form-control" id="pcl_value _'+cnt+' name="pcl_value[]" value="" placeholder="Product Color Values (optional)"> </div></div><div class="col-sm-4 nopadding"> <div class="form-group"> </div></div><button class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div><div class="clear"></div>';
    
    objTo.appendChild(divtest)
    cnt=parseInt(cnt)+parseInt(1);
     // alert(cnt);
}
   function remove_education_fields(rid) {
       $('.removeclass'+rid).remove();
       cnt=parseInt(cnt)+parseInt(-1);
   }


function validateForm() {
    // var x = document.forms["myForm"]["pcl_value"].value;
    // var y = document.forms["myForm"]["pcl_image"].value;

    for (i = 0; i<cnt; i++) {
        // alert("oka");
        var x = document.getElementById("pcl_value_"+i).value;
        var y = document.getElementById("pcl_image_"+i).value;
        // alert(x);
            if (x == "" && y == "") {
                alert("Please Enter Product Color value or Upload Product Color Image");
                return false;
            }else{
                return true;
            }
        }         
}

    $(document).ready(function(){
        $("#submit").on('click',function(){
            var p_cost = $("#p_cost").val();
            var p_rrp = $("#p_rrp").val();
            var p_price = $("#p_price").val();
           
            if(eval(p_cost) < eval(p_rrp) && eval(p_cost) < eval(p_price)){
                 if(eval(p_price) > eval(p_cost) && eval(p_price) <= eval(p_rrp)){
                    return true;
                 }
                 else{
                    alert("Always product price greater than product coast and less than or equal to product retail price ");
                    document.getElementById("p_price").focus();
                    return false;
                 }
            }else{
                
                alert("Always product cost less than product retail price and product price ");
                document.getElementById("p_cost").focus();
                return false;
            }
        });
            
             
    });


</script>






@endpush