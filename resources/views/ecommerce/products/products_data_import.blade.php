@extends('salon-magmt.base') 
@section('action-content')

@push("css")
    <style type="text/css">

            input[type="file"]{
                    /*color: transparent;*/
                }

    </style>
@endpush
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Upload Products Data</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-lg-12">
            <div class="box">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" id="myTab">

                        <li id="one" class="@if(Session::get('activemenu') == 'fileupload' || Session::get('activemenu') == 'save_file')  active @endif">
                            <a href="#tab_1" data-toggle="tab" aria-expanded="true">
                                Products 
                            </a>
                        </li>
                                       </ul>
                    <div class="tab-content">
                        <div class="tab-pane @if(Session::get('activemenu') == 'fileupload'|| Session::get('activemenu') == 'save_file') active @endif" id="tab_1">
                            <div class="box-header">
                                <h3 class="box-title"> 
                                    Import Products Data
                                </h3> 
                                <br>
                                <br>
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button> 
                                            <strong>{!! $message !!}</strong>
                                    </div>
                                @endif
                               
                            </div>
                            <div class="box-body">
                                <div>
                                    <div class="">
                                        <form action="{{ url('admin/store_products_import_data') }}" method="POST" role="form" enctype="multipart/form-data">
                                           
                                            {{ csrf_field() }}
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="exampleInputFile">Add File</label>
                                                    <input required="" name="pdi_file_name" type="file" id="exampleInputFile" class="form-control" onclick="fileClicked(event)" onchange="fileChanged(event)">
                                                </div>
                                            </div>
                                          

                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                <a hidden="" class="btn btn-primary" style="margin-left: 10px;" href="{{ url('') }}" >Cancel</a>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    </section>
@endsection

@push("script")
    <script type="text/javascript">
 
            

    </script>

<script>
    //This is All Just For Logging:
    var debug = true;//true: add debug logs when cloning
    var evenMoreListeners = true;//demonstrat re-attaching javascript Event Listeners (Inline Event Listeners don't need to be re-attached)
    if (evenMoreListeners) {
        var allFleChoosers = $("input[type='file']");
        addEventListenersTo(allFleChoosers);
        function addEventListenersTo(fileChooser) {
            fileChooser.change(function (event) { console.log("file( #" + event.target.id + " ) : " + event.target.value.split("\\").pop()) });
            fileChooser.click(function (event) { console.log("open( #" + event.target.id + " )") });
        }
    }
    (function () {
        var old = console.log;
        var logger = document.getElementById('log');
        console.log = function () {
            for (var i = 0; i < arguments.length; i++) {
                if (typeof arguments[i] == 'object') {
                    logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
                } else {
                    logger.innerHTML += arguments[i] + '<br />';
                }
            }
            old.apply(console, arguments);
        }
    })();

    var clone = {};

    // FileClicked()
    function fileClicked(event) {
        var fileElement = event.target;
        if (fileElement.value != "") {
            // if (debug) { console.log("Clone( #" + fileElement.id + " ) : " + fileElement.value.split("\\").pop()) }
            clone[fileElement.id] = $(fileElement).clone(); //'Saving Clone'
        }
        //What ever else you want to do when File Chooser Clicked
    }

    // FileChanged()
    function fileChanged(event) {
        var fileElement = event.target;
        if (fileElement.value == "") {
            // if (debug) { console.log("Restore( #" + fileElement.id + " ) : " + clone[fileElement.id].val().split("\\").pop()) }
            clone[fileElement.id].insertBefore(fileElement); //'Restoring Clone'
            $(fileElement).remove(); //'Removing Original'
            if (evenMoreListeners) { addEventListenersTo(clone[fileElement.id]) }//If Needed Re-attach additional Event Listeners
        }
        //What ever else you want to do when File Chooser Changed
    }



        $(function () {
             $('input[type="file"]').change(function () {
                  if ($(this).val() != "") {
                         $(this).css('color', '#333');
                  }else{
                         $(this).css('color', 'transparent');
                  }
             });
        })
    </script>

    <script type="text/javascript">
        $(function() {
          $('input[type=file]').change(function(){
            var t = $(this).val();
            var labelText = 'File : ' + t.substr(12, t.length);
            $(this).prev('label').text(labelText);
          })
        });


// active tabs keep selected tabs bootstraps
        $('a[data-toggle="tab"]').click(function (e) {
          e.preventDefault()
          $(this).tab('show')
        });
        $(function () {
          // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
          $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('lastTab', $(this).attr('href'));
          });
          // go to the latest tab, if it exists:
          var lastTab = localStorage.getItem('lastTab');
          if (lastTab) {
            $('#myTab li.active,.tab-pane.active').removeClass('active');
            $('#myTab a[href="' + lastTab + '"]').closest('li').addClass('active');
            $(lastTab).addClass('active');
          }
        });

    </script>



@endpush

