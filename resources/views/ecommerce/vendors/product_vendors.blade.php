@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $pc_image_path = $helper->pc_image_display();
   
@endphp

@extends('salon-magmt.base')
@section('action-content')

@push("css")
    <style type="text/css">
        .incons
        {
          border: none; 
          color: white; 
          padding: 5px 10px;
          font-size: 20px;
          margin-left:10px;
          cursor: pointer;"
        }
    </style>
@endpush

<section class="content">
  <div class="row">
    <div class="col-sm-12">
      <h3>Product Vendors</h3>
    </div>
  </div>
    @if(isset($message))
    @if($message==1)
    <div class="alert alert-success">
        <strong>Success!</strong> {{$messageInfo}}
    </div>
    @endif
    @if($message==0)
    <div class="alert alert-danger">
        <strong>Failed!</strong> {{$messageInfo}}
    </div>
    @endif
    @endif
    <div>
        <form role="form" method="get" action="{{route('pv_search')}}" enctype="multipart/form-data">
         
            <div style="margin:0 auto;
                 width:100%;
                 height:50px;
                 " class="div1">
                <div style="margin:0 auto;
                     height:50px;
                     " class="div1">
                     <div class="row">
                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                            Name 
                          </label>
                          <input  type="text" class="form-control"  name="pv_name" maxlength="128" @isset($pv_name) value="{{$pv_name}}" @endisset placeholder="Search By Product Category Name">
                        </div>
                      </div>

                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="pv_status" id="pv_status">
                          <option value="all" @isset($pv_status) @if($pv_status == 'all') selected="selected" @endif @endisset >All</option>
                          <option value="1" @isset($pv_status) @if($pv_status == '1') selected="selected" @endif @endisset>Active</option>
                          <option value="2" @isset($pv_status) @if($pv_status == '2') selected="selected" @endif @endisset>Inactive</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;" name="submit"  class="btn btn-info" >
                          Search
                        </button>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <a href="{{route('add_vendor')}}" style="margin-top: 27px;"  class="btn btn-info" >
                          Add Product Vender
                        </a>
                      </div>
                    </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>
    <div class="box" style="margin-bottom: 500px; margin-top: 40px; margin-left: 0px; padding-left: 5px;">
        <form role="form" method="POST" action="#" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div   class="form-group">
                <table id="salon_tabel" class="table table-hover">
                  @if(count($pv)>0)
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  @foreach($pv as $key => $value)
                  <tr>
                    <td>{{ $value->pv_name }}</td>
                    <td>{{$value->pv_email}}</td>
                    <td>{{$value->pv_contact}}</td>
                    <td>{{ $value->pv_address }}</td>
                    <td>@if($value->pv_status == 1) Active @else Inactive @endif </td>
                    <td style="margin: 0px auto; padding: 0px;"> 
                                           <a href="{{route('edit_vendor',['id' => $value->pv_id])}}" class="btn btn-info"><i class="glyphicon glyphicon-edit"></i></a> 
                      <a href="{{route('delete_vendor',['id' => $value->pv_id])}}" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a>    
                      <a href="{{route('manage_brands',['id' => $value->pv_id])}}" class="btn btn-info">Manage Brands</a>                                    
                    </td>
                  </tr>
                  @endforeach
                  @else
                  <tr>
                    <td><h2 style="text-align: center;">Record not found!</h2></td>
                  </tr>
                  @endif
                </table>
            </div>
        </form>
    </div>
</section>
@endsection
@push("script")
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
<script>
  @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
  @endif
  @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
  @endif
  @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
  @endif
  @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
  @endif
</script>


@endpush