@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $display_cust_img = $helper->display_cust_img();
  
@endphp

@extends('service-mgmt.base')
@section('action-content')

<section class="content" style="margin-top: -40px;">
  <div class="row">
        <div class="col-sm-12">
            <h3>Store Credit</h3><br>
        </div>
    </div>
    <div style="margin-bottom: 30px;">
        <form method="get" action="{{url('admin/credit_admin')}}">
            <div style="margin:0 auto; width:100%; height:50px;" class="div1" >
                <div style="margin:0 auto; height:50px;" class="div1">
                     <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                            Find User
                          </label>
                          <input required="" minlength="4" placeholder="Find By Name or Phone Number"  type="text" class="form-control" value="@isset($find_user) {{$find_user}}  @endisset" name="find_user" maxlength="128">
                        </div>
                      </div>
                                 
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;"  class="btn btn-info" >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>


    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Store Credit Details </h3>
                </div>
                <div class="col-sm-4">
                    {{-- <a class="btn btn-info" href="">Add new Collection</a> --}}
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <tbody>
                    @if(!empty($customers))
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Email </th>
                        <th>Phone</th>
                        <th>Store Credit</th>
                        {{-- <th>Order Id</th> --}}
                        <th>Action</th>
                    </tr>
                    @foreach($customers as $key => $value)
                    <tr>
                        <td>{{ $key++ }}</td>
                        <td>{{$value->cust_name}}</td>
                        <td><img src="{{$display_cust_img.'/'.$value->cust_pic}}" width="100px" height="100px"></td>
                        <td>{{$value->cust_email}}</td>
                        <td>{{$value->cust_phone}}</td>
                        <td>{{$value->cust_store_credit }}</td>
                        <td>
                         <a href="{{url('admin/storecredits'.'/'.$value->cust_id)}}" class="btn btn-info">Manage Credits</a> 
                        </td>
                    </tr>
                    @endforeach
                    @endif
                
                </tbody>
            </table>

            
        </div>
    </div>
</section>
@endsection