@php
    
    use App\Helpers\Helper;

    $helper = new Helper();
    $display_cust_img = $helper->display_cust_img();

  
@endphp

@extends('service-mgmt.base')
@section('action-content')
<style type="text/css">
    .checkbox label:after, 
.radio label:after {
    content: '';
    display: table;
    clear: both;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
</style>

<section class="content" style="margin-top: -40px;">
  <div class="row">
        <div class="col-sm-12">
            <h2>Add Coupons</h2><br>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h2 class="">Coupons </h2>
                </div>
                <div class="col-sm-4">
                    {{-- <a class="btn btn-info" href="">Add new Collection</a> --}}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                 @if (\Session::has('flash_message'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 23px !important; margin: -tio; margin-top: -12px; margin-right: -14px;"><span aria-hidden="true">&times;</span></button> 
                        {{ \Session::get('flash_message') }}
                    </div> 
                @endif
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            
            <div class="row" style="padding:20px;">
                <form method="post" action="{{route('store_coupons')}}">
                   
                    {{csrf_field()}}
                    
                    <div class="col-md-6">
                        <label>How Many Coupons</label>
                        <input required="" type="number"  name="m_coupons" id="m_coupons" class="form-control">
                    </div>

                    <div class="col-md-6">
                        <label>Title</label>
                        <input required="" type="text"  name="pc_title" id="pc_title" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label>Amount</label>
                        <input required="" type="number"  name="pc_max_value" id="pc_max_value" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label>Percentage off</label>
                        <input required="" type="number"  name="pc_percent" id="pc_percent" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label>Percentage off</label>
                        <input required="" type="date"  name="pc_expiry" id="pc_expiry" class="form-control">
                    </div>
            
                    <div class="col-md-12" style="padding:20px;">
                        <input type="submit" class="btn btn-primary btn-lg" value="Save">
                    </div>
                </form>
            </div>

            <div class="row" style="margin-top: 50px; padding: 20px;">
                
            </div>
        </div>
    </div>
</section>
@endsection
@push('script')
<script type="text/javascript">
    $("#order_id").on('change',function(){
        var order_id = $("#order_id").val();
        document.getElementById('o_id').value = order_id; 
    })
</script>

@endpush