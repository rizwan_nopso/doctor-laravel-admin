@extends('salon-magmt.base')
@section('action-content')

@push("css")
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

@php $ar_id = null; @endphp

@endpush
<section class="content">
    
    <div id="success"></div>
    @if(isset($message))
    @if($message==1)
    <div class="alert alert-success">
        <strong>Success!</strong> {{$messageInfo}}
    </div>
    @endif

    @if($message==0)
    <div class="alert alert-danger">
        <strong>Failed!</strong> {{$messageInfo}}
    </div>
    @endif
    @endif
          @if(Session::has('flash_message'))
              {{ Session::get("flash_message") }}
          @endif
          <br><br>
          <div class="container" style="width: 100% !important;">
            @isset($ar_details)
            @foreach($ar_details as $ar_detail)
            {{-- <div class="row">
              <div class="col-md-6 pull-left">
                <p>Doctor Name: {{$ar_detail->sal_name}} </p>
                <p>Phone: {{$ar_detail->sal_phone}}</p>
                <p>Email: {{$ar_detail->sal_email}}</p>
              </div>
              <div class="col-md-3"></div>
              <div class="col-md-3 pull-right">
                <p>
                  Req. Date: 
                    {{date('d-m-Y',strtotime($ar_detail->app_created))}} &nbsp;
                    {{ date("g:i a", strtotime($ar_detail->app_created))}} </a>
                </p>
                <p>Customer: {{$ar_detail->cust_name}}</p>
              </div>   
            </div> --}}
            @endforeach
            @endisset   
            <div class="row">
              <div class="col-sm-12">
                <span class="pull-right">
                  {{-- <a href="{{$return_back}}" class="btn btn-primary">Return Back</a> --}}
                </span>
              </div>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="row container">
              <div class="col-sm-8 col-sm-offset-2">
                <div class="row">
                  <div class="col-sm-12">
                    <h2>Appointment Detail</h2>
                </div>
              </div>
              <br><br>
                <div class="row">
                  <div class="col-sm-6">
                    <h4>
                      <strong> Name: </strong> {{$ar_detail->sal_name}}
                    </h4>
                  </div>
                  <div class="col-sm-6">
                     <h4>
                      <strong>Technician:</strong> {{$ar_detail->tech_name}}
                    </h4>
                  </div>
                  <div class="col-sm-6">
                    <h4>
                      <strong>Email:</strong> {{$ar_detail->sal_email}}
                    </h4>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                     <h4>
                      <strong>Customer:</strong> {{$ar_detail->cust_name}}
                    </h4>
                  </div>
                  <div class="col-sm-6">
                    <h4>
                      <strong>Phone:</strong> {{$ar_detail->sal_phone}}
                    </h4>
                  </div>
                  <div class="col-sm-6">
                    <h4>
                      <strong>Req. Date: </strong> 
                        {{date('d-m-Y',strtotime($ar_detail->app_created))}} &nbsp;
                        {{ date("g:i a", strtotime($ar_detail->app_created))}} 
                    </h4>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-sm-6">
                     <h4>
                      <strong>Start Time:</strong> {{ date("g:i a", strtotime($ar_detail->app_start_time))}} 
                    </h4>
                  </div>
                  <div class="col-sm-6">
                     <h4>
                      <strong>End Time:</strong> {{ date("g:i a", strtotime($ar_detail->app_end_time))}} 
                    </h4>
                  </div>

                  <div class="col-sm-6">
                     <h4>
                      <strong>Status:</strong> {{$ar_detail->app_status}} 
                    </h4>
                  </div>

                  <div class="col-sm-6">
                     <h4>
                      <strong>Updated Date:</strong> {{date('d-m-Y',strtotime($ar_detail->app_last_modified))}} &nbsp;
                        {{ date("g:i a", strtotime($ar_detail->app_last_modified))}} 
                    </h4>
                  </div>
                </div>
                
                
                
                
               
                

                
                

                

                

              </div>
            </div>
              

            </div>
          </div>


      
@endsection
@push("script")
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>

  

 
  {{-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}

    <script type="text/javascript">
     $( "#start" ).datetimepicker(
     
      );
   
    </script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


<script>


  @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
  @endif
  @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
  @endif
  @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
  @endif
  @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
  @endif


</script>

@endpush