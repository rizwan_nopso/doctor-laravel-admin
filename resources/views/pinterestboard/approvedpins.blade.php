@extends('salon-magmt.base')
@section('action-content')
@push("css")
	 <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css'>

 <style type="text/css">

		* {
		  margin: 0;
		  padding: 0;
		  box-sizing: border-box;
		}

		body {
		  font: 14px/1 'Open Sans', sans-serif;
		  color: #555;
		  background: #e5e5e5;
		}

		.gallery {
		  width: 97%;
		  margin: 0 auto;
		  padding: 5px;
		  background: #fff;
		  box-shadow: 0 1px 2px rgba(0,0,0,.3);
		}

		.gallery > div {
		  position: relative;
		  float: left;
		  padding: 5px;
		}

		.gallery > div > img {
		  display: block;
		  width: 230px;
		  transition: .1s transform;
		  transform: translateZ(0); /* hack */
		}

		.gallery > div:hover {
		  z-index: 1;
		}


		.cf:before, .cf:after {
		  display: table;
		  content: "";
		  line-height: 0;
		}

		.cf:after {
		  clear: both;
		}

		h1 {
		  margin: 40px 0;
		  font-size: 30px;
		  font-weight: 300;
		  text-align: center;
		}
		.pagination
		{
			margin-left: 30px !important;
		}
    /*pinterest style core */
.text-muted a {
  color: #989898;
}

.text-muted a:hover {
  color: grey;
}

.fa a:active, a:hover, a:visited {
  text-decoration: none;
}

/* Tool Colors */
.general {
  background-color: #0095af;
}

.general .card-title {
  color: #f3f3f3;
}

.general .card-text {
  color: #f3f3f3;
}

.card-img {
  display: block;
  width: 100%;
  height: auto;
}

.overlay {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: .5s ease;
  background-color: #008CBA;
}

.card-pin:hover .overlay {
  opacity: .5;
  border: 5px solid #f3f3f3;
  transition: ease .5s;
  background-color: #000000;  
  cursor: zoom-in;
}

.more {
  color: white;
  font-size: 20px;
  position: absolute;
  bottom: 0;
  right: 0;
  text-transform: uppercase;
  transform: translate(-20%, -20%);
  -ms-transform: translate(-50%, -50%);
}

.download {
  color: white;
  font-size: 20px;
  position: absolute;
  bottom: 0;
  left: 0;
  margin-left: 20px;
  text-transform: uppercase;
  transform: translate(-20%, -20%);
  -ms-transform: translate(-50%, -50%);
}

.card-pin:hover .card-title {
  color: #ffffff;
  margin-top: 10px;
  text-align: center;
  font-size: 1.2em;
}

.card-pin:hover .more a {
  text-decoration: none;
  color: #ffffff;
}

.card-pin:hover .download a {
  text-decoration: none;
  color: #ffffff;
}

.social {
  position: relative;
  transform: translateY(-50%);
}

.social .fa {
  margin: 0 3px;
}
.navbar{
  display: inherit !important;
}

.change_font_sizes
  {
    font-size: 10px !important;
  }
@media (min-width: 576px)
{
.card-columns {
    -webkit-column-count: 3;
    column-count: 4 !important;
    -webkit-column-gap: 1.25rem;
    column-gap: 3.25rem;
  }
}

/*.add_border_color
{
  border: 5px solid red;
}
.add_border_color:before
{
  position: absolute;
  border: 10px solid orange;
  /*z-index: -1;*/
}*/
</style>

<style id="css">
  @media (min-width: 576px)
{
.card-columns {
    -webkit-column-count: 3;
    column-count: 4 !important;
    -webkit-column-gap: 1.25rem;
    column-gap: 3.25rem;
  }

}
</style>

@endpush
	<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Post Managements</h3><br>
        </div>
    </div>
		<div id="success"></div>
			   <div class="col-lg-12">
            <div id="advance" style="display:;">
                <form method="get" action="{{url('admin/postmanagement')}}">
                    <div class="row">
                        
                        <div class="col-xs-3" style="margin-top: 20px;">
                            <h4>Service Sub-Category</h4>
                            <div class="">
                              <select class="form-control" name="ssc_id" id="ssc_id" style="font-size: 13px; height: 30px;">
                                  <option value="all">All</option>
                                  <optgroup label="Female">
                                    @foreach($ssc as $key => $sscs)
                                      @if($sscs->ssc_gender === 2)
                                    	<option value="{{$sscs->ssc_id}}">{{$sscs->ssc_name}} (F)</option>
                                      @endif
                                    @endforeach
                                  </optgroup>
                                  <optgroup label="Male">
                                    @foreach($ssc as $key => $sscs)
                                      @if($sscs->ssc_gender === 1)
                                      <option value="{{$sscs->ssc_id}}">{{$sscs->ssc_name}} (M)</option>
                                      @endif
                                    @endforeach
                                  </optgroup>
                                  <optgroup label="Both">
                                    @foreach($ssc as $key => $sscs)
                                      @if($sscs->ssc_gender === 3)
                                      <option value="{{$sscs->ssc_id}}">{{$sscs->ssc_name}} (B)</option>
                                      @endif
                                    @endforeach
                                  </optgroup>
                              </select>
                            </div>
                        </div>  

                        <div class="col-xs-3" style="margin-top: 20px;">
                            <h4>Post Status</h4>
                            <div class="">
                              <select class="form-control" name="p_status" id="p_status" style="font-size: 13px; height: 30px;">
                                <option value="">All</option>
                                <option value="1">Approved</option>
                                <option value="0">Un-approved</option>
                                <option value="2">Deleted</option>
                              </select>
                            </div>
                        </div>  

                        <div class="col-xs-3" style="margin-top: 20px;">
                            <h4>Order By</h4>
                            <div class="">
                                <select name="orderby" class="form-control" id="orderby" style="font-size: 13px; height: 30px;">
                                    <option value="DESC">Newest to Oldest</option>
                                    <option value="ASC">Oldest to Newest</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-3">
                            <div class=""> <br>
                                <input type="submit" class="btn btn-info btn-lg" value="Search" style="padding-bottom: 3px; padding-top: 3px; margin-top: 30px; height: 26px;">
                            </div>
                        </div>

                        
                    </div>
                </form>
            </div>
         </div>
         <div class="col-lg-3 col-xs-6" style="margin-top: 30px;">
           <label>Change Style</label>
           <select name="change_style" id="change_style" class="form-control" style="font-size: 13px; height: 30px;">
             <option value="">Please select style</option>
             <option value="3">with 3 images</option>
             <option value="4">with 4 images</option>
             <option value="5">with 5 images</option>
             <option value="6">with 6 images</option>
             <option value="7">with 7 images</option>
           </select>
         </div>
        <div class="clearfix"></div>
        	@if(!empty($ssc_id))
                    @php  
                      if(is_null($ssc_id))
                        {
                          $ssc_id = "";
                        }
                        $urls = ('admin/approvedpins?'.'ssc_id'.'='.$ssc_id.'&'.'p_status'.'='.$p_status.'&'.'orderby'.'='.$orderby.'page='.$pins->currentPage());

                    @endphp
                  @else
                    @php
                       $urls = ('admin/approvedpins?'.'page='.$pins->currentPage());
                    @endphp
                  @endif


      <div class="box" style=" margin-top: 40px; margin-left: 0px; padding-left: 5px;">
        <form role="form" method="POST" action="{{url("admin/approved")}}" enctype="multipart/form-data" id="devel-generate-content-form">
        	{{csrf_field()}}
            <div class="form-group">
              

                <div class="row">
                    @if(count($pins) > 0)
                    <div class="col-xs-9">
                	       <h1 style="text-align: left; margin-left: 30px;">

                          <input type="checkbox" class="selectall" style="zoome:1.5; margin-top: -5px;" id="select_all" onclick="$(this).val(this.checked ? 1 : 0)" value="0" /> 
                          
                    		  <span style="margin-top: 5px; font-size: 18px;">Select All</span>
                        @if($p_status == '2')
                				  <input type="submit" class="btn btn-primary btn-lg" name="un_delete_app" value="Un-Delete Approved Selected" style=" height: 27px;" id="approvedselect_un_delete">
                        @else
                          <input type="submit" class="btn btn-primary btn-lg" name="approved" value="Approved Selected" style=" height: 27px;" id="approvedselect">
                        @endif
                        @if($p_status == '2')
                				<input type="submit" class="btn btn-danger btn-lg" name="un_delete_unapp" value="Un-Delete Un-Approved Selected" style=" height: 27px;" id="un_approvedselect_un_delete">
                        @else
                          <input type="submit" class="btn btn-danger btn-lg" name="delete" value="Delete Selected" style=" height: 27px;" id="approvedselect_delete">
                        @endif
                        @if($p_status == '2')
                        @else
                        <input type="button" class="btn btn-primary btn-lg" id="setas_category_image" value="Set as Category Image" style=" height: 27px;">
                        <input type="button" class="btn btn-primary btn-lg" id="setas_sub_category_image" value="Set as Sub-Category Image" style=" height: 27px;">


                        <input type="button" class="btn btn-primary btn-lg" id="setas_editor_pic" value="Set as Editor Pic" style=" height: 27px;">
                        @endif
                			</div>
                  <div class="col-lg-3 pull-right">
                    <div class="row">
                      <div class="col-lg-12 pull-right">
                    <div class="" style="margin-top: 12px; ">
                      <a style="margin-left: 46px;" href="{{url('add-new-post'.'/'.$ssc_id)}}" class="btn btn-primary btn-lg" >
                        Add New Post <i class="fa fa-plus"></i>
                      </a>
                     <br><br>
                        <span><b>{{$approved_post}}</b> Approved &nbsp;&nbsp;</span>
                          <span style=" margin-right: 0px;"><b>{{$un_approved_post}}</b> Un-Approved  </span>
                      </div>
                    </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="" style="padding: 20px;">
                      <span id="count-checked-checkboxes">0</span> checked
                    </div>
                  </div>
                	   </h1>
                    @else
                        <h1 style="margin-left: 100px;">Data Not found!</h1>

                        <div class="col-md-6 pull-right" style="margin-top: 40px; text-align:right; width:370px">
                      <a href="{{url('add-new-post'.'/'.$ssc_id)}}" class="btn btn-primary btn-lg" >
                        Add New Post <i class="fa fa-plus"></i>
                      </a>
                     
                      </div>
                      </a>
                    @endif
                  </div>
                </div>

            <div class="row">
              <div class="card-columns" style="padding-left: 20px; padding-right: 20px;">
                @foreach($pins as $kye => $pin)
                  {{-- @if($pin->p_status == 2)

                  @else --}}
                  <div class="card card-pin change_font_size" style="border: none;">
                  @if($pin->p_setas_editor_img==1)
                    <div style="border: 3px solid red;">
                  @endif
                  <img class="card-img @if($pin->p_setas_editor_img==1) add_border_color @endif" src="{{$pin->p_imageUrl}}" width="230px" @if($pin->p_status == '1') style="border: 4px solid #0F7B3E;"@endif @if($pin->p_status == '1' ) title="Approved" @elseif($pin->p_status == '1' || $pin->p_setas_editor_img==1) title="Editor image" @else  title="Un-approved" @endif  />
                  @if($pin->p_setas_editor_img==1)
                    </div>
                  @endif
                 {{-- @if($pin->p_status==2) --}}
                 {{-- @else --}}
                  <input type="checkbox" class="checkbox" name="approvedpins[]" value="{{$pin->pp_id}}" style="zoome:1.5;" id="checkboxid" />
                  {{-- @endif --}}
                                  &nbsp;&nbsp;&nbsp;<span>{{$pin->p_keywords}}</span><br>
                                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  <span>
                                     {{date("Y-m-d h:i a", strtotime($pin->update_date))}}
                                  </span>
                  </div>
                {{-- @endif --}}
                @endforeach
              </div>    
             
	
						{!! $pins->appends(['ssc_id'=>isset($ssc_id)? $ssc_id:'','p_status'=>isset($p_status)? $p_status:'','orderby'=>isset($orderby)?$orderby:'','page'=>$pins->currentPage()])->links() !!}
 </div>
                        
                </div>
            </div>
        </form>
      </div>

      {{-- pinterest pins start code here  --}}
          

      {{-- pinterest pins end code here  --}}
      

	</section>
@endsection

@push("script")
<script type="text/javascript">

  $("#approvedselect_un_delete").click(function() {
     var count = $(":checkbox:checked").length;
     if(count==0)
     {
      alert("please select image ");
      return false;
     }
  });
  $("#un_approvedselect_un_delete").click(function() {
     var count = $(":checkbox:checked").length;
     if(count==0)
     {
      alert("please select image ");
      return false;
     }
  });

  $("#approvedselect").click(function() {
     var count = $(":checkbox:checked").length;
     if(count==0)
     {
      alert("please select image ");
      return false;
     }
  });
  $("#approvedselect_delete").click(function() {
     var count = $(":checkbox:checked").length;
     if(count==0)
     {
      alert("please select image ");
      return false;
     }
  });


  $("#setas_category_image").click(function() {
     var count = $(":checkbox:checked").length;
     if(count==0)
     {
      alert("please choose the image ");
      return false;
     }
     if(count==1)
     {
        var checkedValue = $('.checkbox:checked').val();
        
        window.location.href = "{{url('setas_category_image')}}"+"/"+checkedValue;
     }
     if(count>1)
     {
      alert("Please choose the only one image don't choose the more than one image");
     }
  });


  $("#setas_sub_category_image").click(function() {
     var count = $(":checkbox:checked").length;
     if(count==0)
     {
      alert("please choose the image ");
      return false;
     }
     if(count==1)
     {
        var checkedValue = $('.checkbox:checked').val();
        
        window.location.href = "{{url('setas_sub_category_image')}}"+"/"+checkedValue;
     }
     if(count>1)
     {
      alert("Please choose the only one image don't choose the more than one image");
     }
  });

  $("#setas_editor_pic").click(function() {
     var count = $(":checkbox:checked").length;
     if(count==0)
     {
      alert("please choose the image ");
      return false;
     }
     if(count==1)
     {
        var checkedValue = $('.checkbox:checked').val();
        // alert(checkedValue);
        window.location.href = "{{url('set_as_editor_img')}}"+"/"+checkedValue;
     }
     if(count>1)
     {
      alert("Please choose the only one image don't choose the more than one image");
     }
  });
   
</script>
 

	<script type="text/javascript">
		$(".selectall").click(function(){
			$(".individual").prop("checked",$(this).prop("checked"));

      // var le = document.querySelectorAll('input[name="approvedpins[]"]:checked').length;
      // alert(le);
		});

		$("#ssc_id").val("{{$ssc_id}}");

        $("#p_status").val("{{$p_status}}");
	</script>

    @isset($orderby)
        <script type="text/javascript">
            $("#orderby").val("{{$orderby}}");
        </script>
    @endisset

	 <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


<script>


  @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
  @endif
  @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
  @endif
  @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
  @endif
  @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
  @endif


</script>


<script type="text/javascript">
  var select_all = document.getElementById("select_all"); //select all checkbox
  var checkboxes = document.getElementsByClassName("checkbox"); //checkbox items
    //select all checkboxes
    select_all.addEventListener("change", function(e){
        for (i = 0; i < checkboxes.length; i++) { 
            checkboxes[i].checked = select_all.checked;
        }
  });
    for (var i = 0; i < checkboxes.length; i++) {
        checkboxes[i].addEventListener('change', function(e){ //".checkbox" change 
            //uncheck "select all", if one of the listed checkbox item is unchecked
            if(this.checked == false){
                select_all.checked = false;
            }
            //check "select all" if all checkbox items are checked
            if(document.querySelectorAll('.checkbox:checked').length == checkboxes.length){
                select_all.checked = true;
            }
        });
    }

$(function(){
    $(".checkbox").click(function(){

    if($(".checkbox").length == $(".checkbox:checked").length) {
      $("#select_all").attr("checked", "checked");
    } else {
      $("#select_all").removeAttr("checked");
    }
  });
  });



 $('#change_style').on('change',function() {
       var v = $('#change_style').val();
      var styleEl = document.getElementById("css")
      styleEl.innerHTML = ".card-columns  { column-count: "+v+" !important; }";
    });


 $('#change_style').on('change',function() {
       var v = $('#change_style').val();
       if(v == '6' || v == '7'){
          var styleEl = document.getElementById("css");
          styleEl.innerHTML = ".card-columns  { column-count: "+v+" !important; }";
          $('.change_font_size').addClass('change_font_sizes'); 
        }
        else
        {
          $('.change_font_size').removeClass('change_font_sizes'); 
        }
    });


  $(document).ready(function(){

    var $checkboxes = $('#devel-generate-content-form input[type="checkbox"]');
        
    $checkboxes.change(function(){
        var select_all = 0;
       var selectall = document.getElementById("select_all").checked;
       if(selectall == true){
        select_all = 1;
       }
     
        
        var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
          if(countCheckedCheckboxes == 53)
          {
            countCheckedCheckboxes = eval(countCheckedCheckboxes) - eval(select_all);
            $('#count-checked-checkboxes').text(countCheckedCheckboxes);
            $('#edit-count-checked-checkboxes').val(countCheckedCheckboxes);
            // alert(countCheckedCheckboxes);
          }
          else
          {
            countCheckedCheckboxes = eval(countCheckedCheckboxes);
            if(countCheckedCheckboxes == -1){
              countCheckedCheckboxes = eval(countCheckedCheckboxes) - eval(select_all);
              $('#count-checked-checkboxes').text(countCheckedCheckboxes);
            $('#edit-count-checked-checkboxes').val(countCheckedCheckboxes);
            }
            else{
              countCheckedCheckboxes = eval(countCheckedCheckboxes) - eval(select_all);
              $('#count-checked-checkboxes').text(countCheckedCheckboxes);
            $('#edit-count-checked-checkboxes').val(countCheckedCheckboxes);
            }
            
          }
        // $('#count-checked-checkboxes').text(countCheckedCheckboxes);
        
          // $('#edit-count-checked-checkboxes').val(countCheckedCheckboxes);
      });

  });
 

  </script>

@endpush
