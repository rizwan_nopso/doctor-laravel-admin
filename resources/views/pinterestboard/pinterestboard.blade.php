@extends('salon-magmt.base')
@section('action-content')
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Post Category</h3><br>
        </div>
    </div>
    
    <div id="success"></div>
    @if(isset($message))
    @if($message==1)
    <div class="alert alert-success">
        <strong>Success!</strong> {{$messageInfo}}
    </div>
    @endif

    @if($message==0)
    <div class="alert alert-danger">
        <strong>Failed!</strong> {{$messageInfo}}
    </div>
    @endif
    @endif
          @if(Session::has('flash_message'))
              {{ Session::get("flash_message") }}
          @endif
  
    <div>
        <form role="form" method="get" action="{{url('admin/pc-filter')}}" enctype="multipart/form-data">
            {{-- {{ csrf_field() }} --}}
            <div style="margin:0 auto;
                 width:100%;
                 height:50px;
                 " class="div1">
                <div style="margin:0 auto;
                     height:50px;
                     " class="div1">
                     <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                            Post Category
                          </label>
                          <input placeholder="Post Category Name"  type="text" class="form-control" value="@isset($pb_name) {{$pb_name}}  @endisset" name="pb_name" maxlength="128">
                        </div>
                      </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>Service Sub-category</label>
                        <select class="form-control" name="service_sub_cat" id="sscs">
                          <option value="all" @isset($service_sub_cat) @if($service_sub_cat == 'all') selected="" @else @endif @endisset>All</option>
                            @php 
                                $sscss = DB::table("service_sub_categories")
                                             ->where('ssc_status',1)
                                             ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                                             ->where('service_categories.sc_status',1)
                                             ->orderBy("ssc_name","ASC")->get(); 
                            @endphp
                            <optgroup label="Female">
                              @foreach($sscss as $key => $ssc)
                                  @if($ssc->ssc_gender === 1)
                                  <option value="{{ $ssc->ssc_id }}" @isset($service_sub_cat) @if($service_sub_cat == 'all')  @elseif($ssc->ssc_id == $service_sub_cat) selected="" @endif @endisset >
                                    {{ $ssc->ssc_name }} (F)
                                  </option>
                                  @endif
                              @endforeach
                            </optgroup>

                            <optgroup label="Male">
                              @foreach($sscss as $key => $ssc)
                                  @if($ssc->ssc_gender === 2)
                                  <option value="{{ $ssc->ssc_id }}" @isset($service_sub_cat) @if($service_sub_cat == 'all')  @elseif($ssc->ssc_id == $service_sub_cat) selected="" @endif @endisset >
                                    {{ $ssc->ssc_name }} (M)
                                  </option>
                                  @endif
                              @endforeach
                            </optgroup>
                            <optgroup label="Both">
                              @foreach($sscss as $key => $ssc)
                                  @if($ssc->ssc_gender === 3)
                                  <option value="{{ $ssc->ssc_id }}" @isset($service_sub_cat) @if($service_sub_cat == 'all')  @elseif($ssc->ssc_id == $service_sub_cat) selected="" @endif @endisset >
                                    {{ $ssc->ssc_name }} (B)
                                  </option>
                                  @endif
                              @endforeach
                            </optgroup>
                            
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;" name="submit"  class="btn btn-info" >
                          Filter
                        </button>
                      </div>
                    </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>

      @if(!empty($pb_name) OR !empty($service_sub_cat))
          @php  
            if(is_null($service_sub_cat))
              {
                $sscs = "";
              }

            if(empty($pb_name))
              {
                $pb_name = "";
              }
              $urls = ('pc-filter?'.'pb_name'.'='.$service_sub_cat.'&'.'sscs'.'='.$service_sub_cat.'&'.'page='.$pb->currentPage());
          @endphp
        @else
          @php
             $urls = ('post-category?'.'page='.$pb->currentPage());
          @endphp
        @endif
  
    <div class="box" style=" margin-top: 40px; margin-left: 0px; padding-left: 5px;">
        <form role="form" method="POST" action="{{ url('typeservices') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div   class="form-group">
                <table id="salon_tabel" class="table table-hover">
                    <tr>
                      <th>
                        # 
                      </th>
                        <th>
                          Name   
                        </th>
                        <th> Created At</th>
                        <th>
                          Number of Post
                        </th>
                        <th>
                          Last Fetch Date
                        </th>
                        <th> Status</th>
                        <th>
                          Service Sub-Category    
                        </th>
                        <th style=" display: block;
                            margin-left: auto;
                            text-align: center;
                            margin-right: auto;"><u> Actions </u> </th>
                    </tr>                   
                    @foreach($pb as $key => $row)
                            @php $key += 1; @endphp 
                        <tr>
                            <td>
                              {{ $key }}
                            </td>
                            <td>
                              {{ $row->pb_name }}
                            </td>
                            
                            <td>
                              {{ date('Y-m-d' , strtotime($row->pb_created_at)) }} 
                            </td>
                            
                            <td style="text-align: center;">
                                {{ $row->b_total_pins }}
                            </td>
                            <td>
                              {{$row->last_fetch_date }}
                            </td>

                            <td style="">
                              @if($row->pb_status == 1)
                                Active
                              @else
                                In-Active
                              @endif
                            </td>
                            <td>
                              <select style="width: 150px; padding: 2px;" class="ssc_id">
                                @php 
                                    $sscs = DB::table("service_sub_categories")
                                                ->where('ssc_status',1)
                                                ->join('service_categories','service_categories.sc_id','=','service_sub_categories.sc_id')
                                                ->where('service_categories.sc_status',1)
                                                ->orderBy("ssc_name","ASC")->get();  
                                @endphp

                              <optgroup label="Female">
                                @foreach($sscs as $key => $ssc)
                                    @if($ssc->ssc_gender === 1)
                                    <option value="{{ $ssc->ssc_id }}" @isset($service_sub_cat) @if($service_sub_cat == 'all')  @elseif($ssc->ssc_id == $service_sub_cat) selected="" @endif @endisset >
                                      {{ $ssc->ssc_name }} (F)
                                    </option>
                                    @endif
                                @endforeach
                              </optgroup>
                              <optgroup label="Male">
                                @foreach($sscs as $key => $ssc)
                                    @if($ssc->ssc_gender === 2)
                                    <option value="{{ $ssc->ssc_id }}" @isset($service_sub_cat) @if($service_sub_cat == 'all')  @elseif($ssc->ssc_id == $service_sub_cat) selected="" @endif @endisset >
                                      {{ $ssc->ssc_name }} (M)
                                    </option>
                                    @endif
                                @endforeach
                              </optgroup>
                              <optgroup label="Both">
                                @foreach($sscs as $key => $ssc)
                                    @if($ssc->ssc_gender === 3)
                                    <option value="{{ $ssc->ssc_id }}" @isset($service_sub_cat) @if($service_sub_cat == 'all')  @elseif($ssc->ssc_id == $service_sub_cat) selected="" @endif @endisset >
                                      {{ $ssc->ssc_name }} (B)
                                    </option>
                                    @endif
                                @endforeach
                              </optgroup>

                                {{-- @foreach($sscs as $key => $ssc)
                                  <option value="{{$ssc->ssc_id.','.$row->pb_id}}" @if($row->ssc_id == $ssc->ssc_id) selected="" @endif>{{$ssc->ssc_name}}</option>
                                @endforeach --}}
                              </select>
                            </td> 
                            <td>
                                 
                              @if($row->pb_status==0)
                                    <a 
                                      href="{{ url('admin/ajax_activate'.'/'.$row->pb_id) }}" 
                                      style="border: none;
                                       color: red;
                                       background: #52C1F0;
                                       padding: 5px 10px;
                                       font-size: 20px;
                                       cursor: pointer;" 
                                       onclick="Activate({{ $row->pb_id }})"
                                       class="btn" title="De-Active">
                                        <i class="fa fa-close fa-lg"></i>
                                    </a>
                                    @endif
                                    @if($row->pb_status==1)
                                  <a
                                    href="{{ url('admin/ajax_deactivate'.'/'.$row->pb_id) }}" 
                                    style="border: none;
                                       color: white;
                                       background: #52C1F0;
                                       padding: 5px 10px;
                                       font-size: 20px;
                                       cursor: pointer;"
                                       disabled="disabled"
                                       class="btn" title="Active">
                                        <i class="fa fa-check fa-lg"></i>
                                    </a>
                                    @endif
                            </td>

                                  <div class="modal fade" class="modal-default">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Status Change</h4>
                                          </div>
                                          <div class="modal-body">
                                            <p>Are you sure you want to update status&hellip;</p>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                          </div>
                                        </div>
                                        <!-- /.modal-content -->
                                      </div>
                                      <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal --> 

                        </tr>
                    @endforeach
                </table>
                     {!! $pb->appends(['pb_name'=>isset($pb_name)? $pb_name:'','service_sub_cat'=>isset($service_sub_cat)? $service_sub_cat:'','page'=>$pb->currentPage()])->links() !!}
            </div>
        </form>
    </div>
</section>
@endsection
@push("script")
      <script type="text/javascript">
          jQuery("#is_active").on("change",function(){
          // var id = jQuery(this).val();
          alert(id);
        });
      </script>
    @isset($is_reviewed)
      <script type="text/javascript">
        $("#sal_is_reviewed").val("{{$is_reviewed}}");
      </script>
    @endisset

    @isset($is_active)
      <script type="text/javascript">
        $("#is_active").val("{{$is_active}}");
      </script>
    @endisset

     <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


  <script type="text/javascript">  
      $(".ssc_id").on("change",function(){
          var ssc_id = $(this).val();
          var array = ssc_id.split(",");
          var s_id = array[0];
          var pb_id = array[1];
          $.post('{{ url('admin/changessc') }}' , {s_id: s_id,pb_id:pb_id, _token: '{{ csrf_token() }}' } , function(data){
                  $("#success").html(data);
                  $('#me').fadeIn('fast').delay(1000).fadeOut('slow');
          }); 
        });
  </script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


<script>
    
  @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
  @endif
  @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
  @endif
  @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
  @endif
  @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
  @endif


</script>

@endpush