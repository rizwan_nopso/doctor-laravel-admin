@php 
    $locations = '';
    if($salon->sal_register_as == '1'){
        $locations = 'Location';
    }else{
        $locations = 'Doctor';
    }


@endphp

@extends('salon-magmt.base') 
@section('action-content')
@push("css")

    <link rel="stylesheet" href="{{asset('/')}}bower_components/select2/dist/css/select2.min.css">

    <style type="text/css">
        .incons{
            border: none; 
            color: white; 
            padding: 5px 10px;
            font-size: 20px;
            margin-left:10px;
            cursor: pointer;"

        }

        input[type="file"]{
                    /*color: transparent;*/
                }
    .collapsing {
          transition: none !important;
          -webkit-transition: none;
            transition: none;
            display: none;
        }
        .box{
            position: relative;
            border-radius: 3px;
            background: #ffffff;
             border-top: 0px !important; 
            margin-bottom: 20px;
            width: 100%;
             box-shadow: 0 !important; 
        }
        .select2-container--default .select2-selection--multiple .select2-selection__choice{
            background-color: #3C8DBC !important;
            border: 1px solid #aaa;
            border-radius: 4px;
            cursor: default;
            float: left;
            margin-right: 5px;
            margin-top: 5px;
    padding: 0 5px;
        }
    </style>

@endpush
<section class="content">
    <div class="row">
    <div class="col-sm-6">
      <h3>Manage {{$locations}} Details</h3><br>
    </div>
    <div class="col-sm-6 pull-right" style="text-align: right; padding-top: 20px;">
        <a class="btn btn-primary" href="{{$returnback}}" style="text-align: right;">Return Back </a>
    </div>
  </div>
<div id="success"></div>
    <div class="row">
        <div class="col-sm-12 col-lg-12">
            <div class="box">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" id="myTab">
                        
                        <li class="@if(Session::get('editsalon')=='salon-info') active @endif">
                            <a href="#tab_1" data-toggle="tab" role="tab" aria-controls="tab_1" aria-expanded="false">
                                {{$locations}} Info
                            </a>
                        </li>
                        <li id="three" class="">
                            <a href="#tab_2" data-toggle="tab" class="@if(Session::get('editsalon')=='services_enable') active @endif" role="tab" aria-controls="tab_2" aria-expanded="false">
                                {{$locations}} Management
                            </a>
                        </li>

                    </ul>
                    <div class="tab-content">
                      

                        <div class="tab-pane @if(Session::get('editsalon')=='salon-info') active @endif" id="tab_1">
                            <form role="form" method="POST" action="{{ url('updateSalon') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}


                                <table class="table table-hover">
                                    <tbody>
                                        
                                        <tr>
                                            <th>Name </th>
                                            <th>Email</th>
                                        </tr>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_name }}" name="sal_name" style="width: 50%;">
                                    </td>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_email }}" name="sal_email" style="width: 50%;">
                                    </td>
                                    <tr>
                                        <th>Phone</th>
                                        <th>
                                            Status  
                                             
                                        </th>
                                    </tr>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_phone }}" name="sal_phone" style="width: 50%;">
                                    </td>
                                    <td>
                                      
                                        <select name="is_active" id="is_active" style="width: 50%;">
                                            <option value="1">Active</option>
                                            <option value="0">In-Active</option>
                                        </select>

                                        @push("script")
                                            <script type="text/javascript">
                                                $("#is_active").val("{{$salon->is_active}}");
                                            </script>
                                        @endpush
                                    </td>

                                    <tr>
                                        <th>City</th>
                                        <th>Addresss</th>
                                    </tr>

                                    <td>
                                        <input type="text" value="{{ $salon->sal_city }}" name="sal_city" style="width: 50%;">
                                    </td>

                                    <td>
                                        <input type="text" value="{{ $salon->sal_address }}" name="sal_address" style="width: 50%;">
                                    </td>
                                    <tr>
                                        <th>Area</th>
                                        <th>Specialty</th>
                                    </tr>

                                    <td>
                                        <input type="text" value="{{ $salon->sal_area }}" name="sal_area" style="width: 50%;">

                                    </td>
                                   <td>
                                        <input type="text" value="{{ $salon->sal_specialty }}"  style="width: 50%;" readonly="">
                                    
                                    </td>

                                    <tr>
                                        <th>Postcode</th>
                                        <th>Speciality</th>
                                    </tr>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_zip }}" name="sal_zip" style="width: 50%;">
                                    </td>
                                    <td>
                                        <select style="width: 50%;" class=" select2" multiple="multiple" data-placeholder="Select a Speciality"
                                                style="width: 100%;" name="sal_specialty[]" id="sal_specialty">
                                            @php
                                                $sal_spe = explode(",",$salon->sal_specialty);
                                            @endphp
                                            @foreach($sal_spe as $keys => $value)
                                            @foreach($sp as $key => $spv)
                                                <option @if($value == $spv->sp_name) selected="" @else @endif  value="{{$spv->sp_name}}">{{$spv->sp_name}}</option>
                                            @endforeach
                                            @endforeach
                                        </select>
                                      
                                    </td>

                                    <tr>
                                        <th>Facebook</th>
                                        <th>Instagram</th>
                                    </tr>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_facebook }}" name="sal_facebook" style="width: 50%;">
                                    </td>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_instagram }}" name="sal_instagram" style="width: 50%;">
                                    </td>

                                    <tr>
                                        <th>Sal_24clock </th>
                                        <th>Website</th>

                                    </tr>
                                    <td>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Yes</label>
                                                    <input type="checkbox" @if($salon->sal_24clock == 1 ) checked="" @endif  name="sal_24clock" style="width: 50%;">
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" value="{{ $salon->sal_website }}" name="sal_website" style="width: 50%;">
                                    </td>
                                    {{-- add some extra filed from salon table update  --}}
                                    <tr>
                                        <th>Sal Contact Person</th>
                                        <th>Sal Password</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" value="{{ $salon->sal_contact_person }}" name="sal_contact_person" style="width: 50%;">
                                        </td>
                                            
                                    
                                        <td>
                                            <input type="password" value="{{ $salon->sal_password }}" name="sal_password" style="width: 50%;" id="myInput">  <input type="checkbox" onclick="myFunction()">Show Password
                                        </td> 
                                    </tr>
                                    <tr>
                                        <th>Sal Latitude</th>
                                        <th>Sal Longitude</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" value="{{ $salon->sal_lat }}" name="sal_lat" style="width: 50%;">
                                        </td>
                                            
                                    
                                        <td>
                                            <input type="text" value="{{ $salon->sal_lng }}" name="sal_lng" style="width: 50%;">
                                        </td> 
                                    </tr>
                                    <tr>
                                        <th>Sal Auto Accept App</th>
                                        <th>Sal Future App Days</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select name="sal_auto_accept_app" style="width: 50%; height: 30px;" id="sal_auto_accept_app">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </td>
                                            @push("script")
                                                <script type="text/javascript">
                                                    $("#sal_auto_accept_app").val("{{$salon->sal_auto_accept_app}}");
                                                </script>
                                            @endpush
                                        <td>
                                            <input type="text" value="{{ $salon->sal_future_app_days }}" name="sal_future_app_days" style="width: 50%;">
                                        </td> 
                                    </tr>
                                    <tr>
                                        <th>Sal Twitter</th>
                                        <th>Sal Weekly Offs</th>
                                    </tr>
                                    <tr>
                                        <td>
                                           <input type="text" value="{{ $salon->sal_twitter }}" name="sal_twitter" style="width: 50%;">
                                        </td>
                                        <td>
                                            <input type="text" value="{{ $salon->sal_weekly_offs }}" name="sal_weekly_offs" style="width: 50%;">
                                        </td> 
                                    </tr>

                                    <tr>
                                        
                                        <th>Salon Type </th>
                                        <th>Biography</th>
                                    </tr>
                                    <tr>

                                        <td>
                                            <select name="sty_id" style="width: 50%; height: 30px;" id="sty_id">
                                                @foreach($salontypes as $key => $st)
                                                    <option value="{{$st->sty_id}}" @if($st->sty_id == $salon->sty_id) selected="checked" @endif >{{$st->sty_name}}</option>
                                                @endforeach
                                            </select>
                                        </td> 
                                    

                                        <td>
                                            <textarea name="sal_biography" class="form-control" style="width: 50%;">{{ $salon->sal_biography }}</textarea>
                                        </td> 
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            Description
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <textarea class="form-control" rows="8" name="sal_description">@isset($salon->sal_description){{$salon->sal_description}}@endisset</textarea>
                                        </td>
                                    </tr>
                                    

                                    <td>

                                        <input type="hidden" value="{{ $salon->sal_id }}" name="sal_id" style="width: 50%;">
                                    </td>
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group"> 
                                        <label>Sal Pic</label>
                                        <br>      
                                                    
                                            <img src="{{$cate_images}}{{$salon->sal_pic}}" width="100%" height="250px" id="blah">
                                            <br>
                                            <br>
                                            <input type="file" accept="image/*" name="sal_pic" id="imgInp" class="form-control" onclick="fileClicked(event)" onchange="fileChanged(event)" >
                                        @push('script')
                                            <script type="text/javascript">
                                                function readURL(input) {
                                                  if (input.files && input.files[0]) {
                                                    var reader = new FileReader();

                                                    reader.onload = function(e) {
                                                      $('#blah').attr('src', e.target.result);
                                                    }

                                                    reader.readAsDataURL(input.files[0]);
                                                  }
                                                }

                                                $("#imgInp").change(function() {
                                                  readURL(this);
                                                });
                                            </script>
                                        @endpush
                                                
                                    </div>

                                    <div class="form-group">
                                        <label> Sal Profile Pic </label>
                                            <br>
                                      
                                            <img src="{{$cate_images}}{{$salon->sal_profile_pic}}" id="sal_profile" width="50%" height="250px" >
                                      
                                        <br>
                                        <br>
                                        <input type="file" accept="image/*" name="sal_profile_pic" onchange="testFunction();" class="form-control" onclick="fileClicked(event)" >
                                          @push('script')
                                            <script type="text/javascript">
                                                    
                                                  var loadFile = function(event) {
                                                    var output = document.getElementById('sal_profile');
                                                    output.src = URL.createObjectURL(event.target.files[0]);
                                                  };

                                                  function testFunction() {
                                                            fileChanged(event);
                                                            loadFile(event);
                                                            
                                                        }
                                            </script>
                                          @endpush          
                                    </div>
                                </div>
                                <div class="col-sm-6">    
                                    <div class="col-sm-6">
                                        <div class="box-header with-border">
                                          <h3 class="box-title">Salon Time Table </h3>
                                        </div>
                                     
                                         @php
                                            $hour = $salon->sal_hours;
                                            
                                                $gethours = explode(',', $hour);
                                        @endphp

                                        <div class="box-body">
                                          <table class="table table-bordered">
                                            <tbody><tr>
                                              <th style="width: 10px">#</th>
                                              <th>Day </th>
                                              <th>Start Time</th>
                                              <th>End Time</th>
                                            </tr>
                                            <tr>
                                              <td>1.</td>
                                              <td>Monday</td>
                                              @isset($gethours[0])
                                                  @php 
                                                    $monday = explode('&', $gethours[0]);
                                                  @endphp
                                              @endisset
                                              {{-- @php dd($monday[0]); @endphp --}}
                                              <td>
                                                @if(!empty($monday))
                                                    <input type="time" name="mondaystart" class="form-control" value="@isset($monday[0]){{$monday[0]}}@endisset" required="">
                                                @else
                                                    <input type="time" name="mondaystart" class="form-control" value="" required="">
                                                @endif
                                              </td>

                                              <td>
                                                @if(!empty($monday))
                                                    <input type="time" name="mondayend" class="form-control" value="@isset($monday[1]){{$monday[1]}}@endisset" required="">
                                                @else
                                                    <input type="time" name="mondayend" class="form-control" value="" required="">
                                                @endif
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>2.</td>
                                              <td>Tuesday</td>
                                              @isset($gethours[1])
                                                  @php 
                                                    $tuesday = explode('&', $gethours[1]);
                                                  @endphp
                                              @endisset
                                              <td>
                                                @if(!empty($tuesday))
                                                    <input type="time" name="tuesdaystart" class="form-control" value="@isset($tuesday[0]){{$tuesday[0]}}@endisset" required="">
                                                @else 
                                                    <input type="time" name="tuesdaystart" class="form-control" value="" required="">
                                                @endif
                                              </td>
                                              <td>
                                                @if(!empty($tuesday))
                                                    <input type="time" name="tuesdayend" class="form-control" value="@isset($tuesday[1]){{$tuesday[1]}}@endisset" required="">
                                                @else
                                                    <input type="time" name="tuesdayend" class="form-control" value="" required="">
                                                @endif
                                              </td>
                                            </tr>

                                            <tr>
                                              <td>3.</td>
                                              <td>Wednesday</td>
                                              @isset($gethours[2])
                                                  @php 
                                                    $wednesday = explode('&', $gethours[2]);
                                                  @endphp
                                              @endisset
                                              <td>
                                                @if(!empty($wednesday))
                                                    <input type="time" name="wednesdaystart" class="form-control" value="@isset($wednesday[0]){{$wednesday[0]}}@endisset" required="">
                                                @else
                                                    <input type="time" name="wednesdaystart" class="form-control" value="" required="">
                                                @endif
                                              </td>
                                              <td>
                                                @if(!empty($wednesday))
                                                    <input type="time" name="wednesdayend" class="form-control" value="@isset($wednesday[1]){{$wednesday[1]}}@endisset" required="">
                                                @else
                                                    <input type="time" name="wednesdayend" class="form-control" value="" required="">
                                                @endif
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>4.</td>
                                              <td>Thursday</td>
                                              @isset($gethours[3])
                                                  @php 
                                                    $thursday = explode('&', $gethours[3]);
                                                  @endphp
                                              @endisset
                                              <td>
                                                @if(!empty($thursday))
                                                    <input type="time" name="thursdaystart" class="form-control" value="@isset($thursday[0]){{$thursday[0]}}@endisset" required="">
                                                @else
                                                    <input type="time" name="thursdaystart" class="form-control" value="" required="">
                                                @endif
                                              </td>
                                              <td>
                                                @if(!empty($thursday))
                                                    <input type="time" name="thursdayend" class="form-control" value="@isset($thursday[1]){{$thursday[1]}}@endisset" required="">
                                                @else
                                                    <input type="time" name="thursdayend" class="form-control" value="" required="">
                                                @endif
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>5.</td>
                                              <td>Friday</td>
                                              @isset($gethours[4])
                                                  @php 
                                                    $friday = explode('&', $gethours[4]);
                                                  @endphp
                                              @endisset
                                              <td>
                                                @if(!empty($friday))
                                                    <input type="time" name="fridaystart" class="form-control" value="@isset($friday[0]){{$friday[0]}}@endisset" required="">
                                                @else
                                                    <input type="time" name="fridaystart" class="form-control" value="" required="">
                                                @endif

                                              </td>
                                              <td>
                                                @if(!empty($friday))
                                                    <input type="time" name="fridayend" class="form-control" value="@isset($friday[1]){{$friday[1]}}@endisset" required="">
                                                @else
                                                    <input type="time" name="fridayend" class="form-control" value="" required="">
                                                @endif
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>6.</td>
                                              <td>Saturday</td>
                                              @isset($gethours[5])
                                                  @php 
                                                    $saturday = explode('&', $gethours[5]);
                                                  @endphp
                                              @endisset
                                              <td>
                                                @if(!empty($saturday))
                                                    <input type="time" name="saturdaystart" class="form-control" value="@isset($saturday[0]){{$saturday[0]}}@endisset" required="">
                                                @else 
                                                    <input type="time" name="saturdaystart" class="form-control" value="" required="">
                                                @endif
                                                
                                              </td>
                                              <td>
                                                @if(!empty($saturday))
                                                    <input type="time" name="saturdayend" class="form-control" value="@isset($saturday[1]){{$saturday[1]}}@endisset" required="">
                                                @else
                                                    <input type="time" name="saturdayend" class="form-control" value="" required="">
                                                @endif
                                              </td>
                                            </tr>
                                            <tr>
                                              <td>7.</td>
                                              <td>Sunday</td>
                                              @isset($gethours[6])
                                                  @php 
                                                    $sunday = explode('&', $gethours[6]);
                                                  @endphp
                                              @endisset
                                              <td>
                                                @if(!empty($sunday))
                                                    <input type="time" name="sundaystart" class="form-control" value="@isset($sunday[0]){{$sunday[0]}}@endisset" required="">
                                                @else
                                                    <input type="time" name="sundaystart" class="form-control" value="" required="">
                                                @endif
                                              </td>
                                              <td>
                                                @if(!empty($sunday))
                                                    <input type="time" name="sundayend" class="form-control" value="@isset($sunday[1]){{$sunday[1]}}@endisset" required="">
                                                @else
                                                    <input type="time" name="sundayend" class="form-control" value="" required="">
                                                @endif
                                              </td>
                                            </tr>
                                          </tbody>
                                      </table>
                                    </div>
                                </div>
                                </div>
                            </div>
                                    
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button style=" margin-left: 14px; margin-right: 10px;" type="reset" class="btn btn-primary">Reset</button>
                                <a style=" margin-left: 8px; margin-right: 8px;" class="btn btn-primary" href="{{ $returnback }}">
                                    Cancel
                                </a>

                            </form>
                        </div>

                        <!-- close form here -->



                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="box-header">
                                <h3 class="box-title"> {{ $salon->sal_name }} </h3>
                                <!--<p>All fields are required* </p>-->
                            </div>
                            <style type="text/css">
  .small-img {
      height: 80px;
      width: 80px;
      border-radius: 50%;
    }
</style>

<section class="content">
 
    <div class="box">
        
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <tbody>
                  @if(count($technicians) > 0 ) 
                  <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Salon Name</th>
                        <th>Email</th>
                        {{-- <th>Image</th> --}}
                        <th>Phone</th>
                        <th>Status</th>
                        <th>Is Active</th>
                        <th>@if(Session::get('change_db_connection')=="2") Staff @elseif(Session::get('change_db_connection')=="3") Staff @elseif(Session::get('change_db_connection')=="1") Doctor @endif DateTime</th>
                        <th>Action</th>
                    </tr>
                   
                    <?php foreach ($technicians as $key => $tech) {  $key += 1;  ?>
                        <tr>
                            <td>{{$key}}</td>
                            <td>
                              <img class="small-img" src="{{$display_tech_img}}{{$tech->tech_pic ? $tech->tech_pic : 'placeholder.png'}}">
                              <?php echo $tech->tech_name ?>
                            </td>
                            <td>{{$tech->sal_name}}</td>
                            <td>{{$tech->sal_email}}</td>
                            <td>
                              {{$tech->tech_phone}}
                            </td>
                            <td>
                              @if($tech->tech_status==1)
                                Active
                              @else
                                In-Active
                              @endif  
                            </td>
                            <td>
                              @if($tech->tech_is_active==1)
                                Yes
                              @else
                                No
                              @endif  
                            </td>
                            <td>
                              {{date('d-m-Y',strtotime($tech->tech_modify_datetime))}}<br>
                              {{date('g:i a',strtotime($tech->tech_modify_datetime))}}
                            </td>
                            <td> 
                                  @if( $tech->tech_status ==0)
                                      <a href="{{ url('admin/enable_tech', ['id' => $tech->tech_id]) }}"
                                         class="btn btn-danger incons" title="Inactive">   
                                          <i class="glyphicon glyphicon-remove"></i> 
                                      </a> 
                                  @else
                                      <a href="{{ url('admin/disable_tech', ['id' => $tech->tech_id]) }}"
                                         class="btn incons" style="background: #52C1F0;" title="Active">   
                                          <i class="glyphicon glyphicon-ok"></i> 
                                      </a> 
                                  @endif
                            </td>

                        </tr>
                    <?php } ?>
                    @else
                      
                        <h3 style="text-align: center;">Record Not Found</h2>
                      
                    @endif
                </tbody>
            </table>

            {!! $technicians->links() !!}
            
        </div>
        <!-- /.box-body -->
    </div>
</section>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    

       
       
    </section>
@endsection

@push('script')

   {{--  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css"> --}}



    <script type="text/javascript">
        $(function() {
            $('a[data-toggle="tab"]').on('click', function(e) {
                window.localStorage.setItem('activeTab', $(e.target).attr('href'));
            });
            var activeTab = window.localStorage.getItem('activeTab');
            if (activeTab) {
                $('#myTab a[href="' + activeTab + '"]').tab('show');
                window.localStorage.removeItem("activeTab");
            }
        });
    </script> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


<script>
    
  @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
  @endif
  @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
  @endif
  @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
  @endif
  @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
  @endif


</script>

<script src="{{asset('/')}}bower_components/select2/dist/js/select2.full.min.js"></script>

<script type="text/javascript">  
    
    $('.select2').select2();

      $(".ssubc_id").on("change",function(){
          var ssubc_id = $(this).val();
          // alert(ssc_id);
          var array = ssubc_id.split(",");
          var ssubc_id = array[0];
          var sser_id = array[1];

          $.post('{{ url('admin/changeservice_category') }}' , {ssubc_id: ssubc_id,sser_id:sser_id, _token: '{{ csrf_token() }}' } , function(data){
                  $("#success").html(data);
                  $('#me').fadeIn('fast').delay(1000).fadeOut('slow');
          }); 
        });
  </script>

  <script type="text/javascript">
        $(function () {
             $('input[type="file"]').change(function () {
                  if ($(this).val() != "") {
                         $(this).css('color', '#333');
                  }else{
                         $(this).css('color', 'transparent');
                  }
             });
        })
    </script>


<script>
    //This is All Just For Logging:
    var debug = true;//true: add debug logs when cloning
    var evenMoreListeners = true;//demonstrat re-attaching javascript Event Listeners (Inline Event Listeners don't need to be re-attached)
    if (evenMoreListeners) {
        var allFleChoosers = $("input[type='file']");
        addEventListenersTo(allFleChoosers);
        function addEventListenersTo(fileChooser) {
            fileChooser.change(function (event) { console.log("file( #" + event.target.id + " ) : " + event.target.value.split("\\").pop()) });
            fileChooser.click(function (event) { console.log("open( #" + event.target.id + " )") });
        }
    }
    (function () {
        var old = console.log;
        var logger = document.getElementById('log');
        console.log = function () {
            for (var i = 0; i < arguments.length; i++) {
                if (typeof arguments[i] == 'object') {
                    logger.innerHTML += (JSON && JSON.stringify ? JSON.stringify(arguments[i], undefined, 2) : arguments[i]) + '<br />';
                } else {
                    logger.innerHTML += arguments[i] + '<br />';
                }
            }
            old.apply(console, arguments);
        }
    })();

    var clone = {};

    // FileClicked()
    function fileClicked(event) {
        var fileElement = event.target;
        if (fileElement.value != "") {
            // if (debug) { console.log("Clone( #" + fileElement.id + " ) : " + fileElement.value.split("\\").pop()) }
            clone[fileElement.id] = $(fileElement).clone(); //'Saving Clone'
        }
        //What ever else you want to do when File Chooser Clicked
    }

    // FileChanged()
    function fileChanged(event) {
        var fileElement = event.target;
        if (fileElement.value == "") {
            // if (debug) { console.log("Restore( #" + fileElement.id + " ) : " + clone[fileElement.id].val().split("\\").pop()) }
            clone[fileElement.id].insertBefore(fileElement); //'Restoring Clone'
            $(fileElement).remove(); //'Removing Original'
            if (evenMoreListeners) { addEventListenersTo(clone[fileElement.id]) }//If Needed Re-attach additional Event Listeners
        }
        //What ever else you want to do when File Chooser Changed
    }



        $(function () {
             $('input[type="file"]').change(function () {
                  if ($(this).val() != "") {
                         $(this).css('color', '#333');
                  }else{
                         $(this).css('color', 'transparent');
                  }
             });
        })

        // active tabs keep selected tabs bootstraps
        $('a[data-toggle="tab"]').click(function (e) {
          e.preventDefault()
          $(this).tab('show')
        });
        $(function () {
          // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
          $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            // save the latest tab; use cookies if you like 'em better:
            localStorage.setItem('lastTab', $(this).attr('href'));
          });
          // go to the latest tab, if it exists:
          var lastTab = localStorage.getItem('lastTab');
          if (lastTab) {
            $('#myTab li.active,.tab-pane.active').removeClass('active');
            $('#myTab a[href="' + lastTab + '"]').closest('li').addClass('active');
            $(lastTab).addClass('active');
          }
        });
    </script>


<script language="javascript">
    function check_sub_categories(obj_category){ 
       var checkboxes = document.getElementsByClassName(obj_category.className); //checkbox items
         for (i = 0; i < checkboxes.length; i++) {

             checkboxes[i].checked = obj_category.checked;
         }
         for (var i = 0; i < checkboxes.length; i++) {
        checkboxes[i].addEventListener('change', function(e){ //".checkbox" change 
            //uncheck "select all", if one of the listed checkbox item is unchecked
            if(this.checked == false){
                obj_category.checked = false;
            }
            //check "select all" if all checkbox items are checked
            if(document.querySelectorAll('.checkbox:checked').length == checkboxes.length){
                obj_category.checked = true;
            }
        });
    }
  }
</script>




    <script type="text/javascript">
       function check_sub_cat(obj_category){ 
       var checkboxes = document.getElementsByClassName(obj_category.className);
            // alert(checkboxes.length);
            var number = null;
           for(var i = 0; i < checkboxes.length; i++) {
                if(checkboxes[i].checked == obj_category.checked)
                {
                    if(obj_category.checked)
                    {
                        number++;
                    }
                }
           }
           if(checkboxes.length == (number+1))
           {
                checkboxes[0].checked = "checked";
           }
           else
           {
                checkboxes[0].checked = "";
           }
        };

    function myFunction() {
      var x = document.getElementById("myInput");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
        
    </script>

@endpush