@extends('salon-magmt.base')

@push("css")
    {{-- <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
    <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/bootstrap-3.min.css"> --}}
@endpush
@section('action-content')
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Update Service </h3><br>
        </div>
    </div>

    <div class="box">
        <form action="{{ url('updateservicename')}}" method="POST" role="form" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="box-body">
                <table id="salon_tabel" class="table table-hover">
                    <tr>


                        <th  style="border: 1px;">
                            <input type="text" 
                                   style=" width: 50%; padding:6px;  "
                                   value="{{ $services->ser_name }}"
                                   name="ser_name">  
                            <input 
                                hidden
                                type="text" 

                                value="{{ $services->ser_id }}"
                                name="ser_id">  
                           

                        </th>
                        <th style="border: 1px;">
                           
                        </th> 
                    </tr>

                </table>
                <button  style="margin-left: 10px; margin-bottom: 10px; margin-top: 20px;" type="submit" class="btn btn-primary">Update </button>

                            <a href="{{$returnback}}" style="margin-top: 20px; margin-left: 20px; font-size: 15px; margin-bottom: 10px; "  class="btn btn-primary" >
                                Cancel
                            </a>
        </form>
    </div>
</div>
</section>
@endsection


@push("script")

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


<script>
    toastr.options = {
            "closeButton": true,
            "debug": false,
            // "positionClass": "toast-bottom-full-width",
            "onclick": null,
            "showDuration": "1000",
            "hideDuration": "0",
            "timeOut": "0",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
// toastr.error(message, title);


  @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
  @endif
  @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
  @endif
  @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
  @endif
  @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
  @endif


</script>
@endpush
