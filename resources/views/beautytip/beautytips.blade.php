@extends('service-mgmt.base')
@section('action-content')

<style type="text/css">
  .small-img {
      height: 80px;
      width: 80px;
      border-radius: 50%;
    }
    .content-header{
      margin-top: 30px !important;
    }
</style>

<section class="content">

    <div style="margin-bottom: 30px;">
        <form role="form" method="get" action="{{ url('admin/beauty_tips') }}">
            <div style="margin:0 auto;
                 width:100%;
                 height:50px;
                 " class="div1">
                <div style="margin:0 auto;
                     height:50px;
                     " class="div1">
                     <div class="row">
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                              Status  
                          </label>
                          <select class="form-control" name="bt_status" id="bt_status">
                            <option value='all_active' @isset($bt_status) @if($bt_status == "all_active") selected="checked" @endif @endisset>All</option>
                            <option value='1' @isset($bt_status) @if($bt_status == 1) selected="checked" @endif @endisset >Active</option>
                            <option value='2' @isset($bt_status) @if($bt_status == 2) selected="checked" @endif @endisset>Inactive</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                        <div class="form-group">
                          <label></label>
                          <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;" name="submit"  class="btn btn-info" >
                            Search
                          </button>
                        </div>
                      </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>


    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Beauty Tips</h3>
                </div>
                <div class="col-sm-4">
                    
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

          {{-- data table start code here  --}}
              <div class="row">
                <div class="col-md-12" style="margin-top: 50px;">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                      <thead>
                          <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Is Urdu Title</th>
                            <th>Is Urdu Short Description</th>
                            <th>Is Urdu Description</th>
                            <th>Status</th>
                            <th>Modified DateTime</th>
                          </tr>
                      </thead>
                      <tbody>
                          @if(!empty($beautytips))
                          @foreach($beautytips as $key => $beautytip)
                            @php $key += 1; @endphp
                              <tr>
                                <td>{{$key}}</td>
                            <td>
                              <a href="{{url('admin/beautytip_detail'.'/'.$beautytip->bt_id)}}" >
                              {{$beautytip->bt_title}}</a>
                            </td>
                            <td>
                              @if($beautytip->bt_title_urdu !== '' and $beautytip->bt_title_urdu !== null)
                                Yes
                              @else
                                No
                              @endif
                            </td>
                            <td>
                              @if($beautytip->bt_short_description_urdu !== '' and $beautytip->bt_short_description_urdu !== null)
                                Yes
                              @else
                                No
                              @endif
                            </td>
                            <td>
                              @if($beautytip->bt_description_urdu !== '' and $beautytip->bt_description_urdu !== null)
                                Yes
                              @else
                                No
                              @endif
                            </td>
                            <td>
                              @if($beautytip->bt_status==1)
                                Active
                              @else
                                Inactive
                              @endif
                            </td>
                            <td>
                                
                                {{date('Y-m-d',strtotime($beautytip->bt_modified_date))}} &nbsp;
                                {{ date("g:i a", strtotime($beautytip->bt_modified_date))}} 
                            </td>
                            </tr>
                        @endforeach
                        @endif

                         
                      </tbody>
                      <tfoot>
                          <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Is Urdu Title</th>
                            <th>Is Urdu Short Description</th>
                            <th>Is Urdu Description</th>
                            <th>Status</th>
                            <th>Modified DateTime</th>
                          </tr>
                      </tfoot>
                  </table>
                </div>
              </div>
        </div>
    </div>
</section>
@endsection

@push("script")
  <script type="text/javascript">
       $(document).ready(function() {
          $('#example').DataTable();
        } );
  </script>
@endpush