
@extends('service-mgmt.base')
@section('action-content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

<link rel="stylesheet" type="text/css" href="{{asset('css/multiple_selected.css')}}">

<style type="text/css">
    .filter-option{
        border:1px solid #ccc !important;
    }
    .btn-group{
        width: 100% !important;
    }
    .multiselect{
        width: 100% !important;
    }
    .multiselect-container{
    background-color: rgb(234, 234, 234) !important;
    height: 200px !important;
    overflow-y: scroll !important;
    scroll-behavior: smooth !important;
    width: 100% !important;
    }
    .multiselect-all{
        display: none;
    }
    #cke_23{
        display: none;
    }
    #cke_31{
        display: none;
    }
    #cke_36{
        display: none;
    }
    #cke_50{
        display: none;
    }
    #cke_119{
        display: none;
    }
    #cke_127{
        display: none;
    }
    #cke_132{
        display: none;
    }
    #cke_146{
        display: none;
    }
    #cke_156{
        display: none;
    }
    #cke_60{
        display: none;
    }
</style>

<script type="text/javascript">
        if ( location.hostname == 'sdk.ckeditor.com' ) {
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-271067-15', 'auto');
            ga('send', 'pageview');
        }
</script>


<section class="content" >
    <div class="row">
        <div class="col-sm-12">
            <h3>Add Beauty Tips </h3><br>
        </div>
    </div>
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Add new post</h3>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('toasts'))
                      @foreach(Session::get('toasts') as $toast)
                        <div class="alert alert-{{ $toast['level'] }}">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                          {{ $toast['message'] }}
                        </div>
                      @endforeach
                    @endif
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form action="{{ url('add_beauty_tips') }}" method="POST" role="form" enctype="multipart/form-data">
                {{ csrf_field() }}

                <input type="hidden" name="bt_id" @isset($bt_id) value="{{$bt_id}}" @endisset>
                <div class="box-body">

                    <div class="col-sm-6">
                         <div class="form-group">
                            <label for="exampleInputPassword1">Beauty Tip Title English</label>
                            <input required="" type="text" placeholder="Beauty Tips Title" class="form-control" name="bt_title" @isset($beauty_tips->bt_title) value="{{$beauty_tips->bt_title}}" @endisset>
                        </div>
                    </div>

                     <div class="col-sm-6">
                         <div class="form-group">
                            <label for="exampleInputPassword1">Beauty Tip Title Urdu</label>
                            <input type="text" placeholder="Beauty Tips Title" class="form-control" name="bt_title_urdu" @isset($beauty_tips->bt_title_urdu) value="{{$beauty_tips->bt_title_urdu}}" @endisset style="text-align: right; direction: rtl;">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Beauty Tip Category </label>
                            <select class="form-control" name="btc_id" id="btc_id">
                                @isset($btc_tips)
                                    @foreach($btc_tips as $key => $btc)
                                        <option value="{{$btc->btc_id}}" @if($btc->btc_id == $btc_id) selected="checked"  @else @isset($beauty_tips->btc_id) @if($btc->btc_id == $beauty_tips->btc_id) selected="checked" @endif @endisset @endif >{{$btc->btc_name}}</option>
                                    @endforeach
                                @endisset
                            </select>

                        </div>
                    </div>

                    <div class="col-sm-6">
                         <div class="form-group">
                            <label for="exampleInputPassword1">Service sub-category </label>
                            @php $myArray = array(); @endphp
                              @isset($beauty_tips->ssc_id) 
                                    @php 
                                        $bt_ssc_id = $beauty_tips->ssc_id; 
                                        $myArray = explode(',', $bt_ssc_id);
                                    @endphp
                                @endisset
                            <select name="ssc_id[]" class="form-control" required id="multiselect" multiple="multiple">
                                 <optgroup label="Female">
                                 @foreach($sscs as $ssc)
                                    @if($ssc->ssc_gender == '2')
                                        @php $ssc_id = $ssc->ssc_id; @endphp
                                        @if(in_array($ssc_id,$myArray))
                                        <option value="{{ $ssc->ssc_id }}" selected="selected"> {{ $ssc->ssc_name }} (F)</option>
                                        @else
                                        <option value="{{ $ssc->ssc_id }}"> {{ $ssc->ssc_name }} (F)</option>
                                        @endif
                                    @endif
                                 @endforeach
                                   </optgroup>
                               <optgroup label="Male">
                                 @foreach($sscs as $ssc)
                                    @if($ssc->ssc_gender == '1')
                                        <option value="{{ $ssc->ssc_id }}"> {{ $ssc->ssc_name }} (M)</option>
                                    @endif
                                 @endforeach
                                 </optgroup>
                            </select>
                        </div>
                        @push("script")
                        @isset($ssc_name)
                        <script type="text/javascript">
                            $("#cat_name").val("{{$ssc_name}}");
                        </script>
                        @endisset
                        @endpush
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Beauty Tip Type</label>
                            <select name="bt_type" class="form-control" required>
                                <option value="">Select Beauty Tip Type</option>
                                <option value="1" @isset($beauty_tips->bt_type) @if($beauty_tips->bt_type=='1') selected="selected" @endif @endisset>Article</option>
                                <option value="2" @isset($beauty_tips->bt_type) @if($beauty_tips->bt_type=='2') selected="selected" @endif @endisset>Tip</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Beauty Tip Status</label>
                            <select name="bt_status" class="form-control" required>
                                <option value="1" @isset($beauty_tips->bt_status) @if($beauty_tips->bt_status == 1) selected="checked" @endif @endisset >Active</option>
                                <option value="0" @isset($beauty_tips->bt_status) @if($beauty_tips->bt_status == 0) selected="checked" @endif @endisset >Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Source Beauty Tip URL (optional)</label>
                            <input type="text" name="bt_URL" class="form-control" id="exampleInputEmail1" placeholder="Source BeautyTip URL (optional)" @isset($beauty_tips->bt_URL) value="{{$beauty_tips->bt_URL}}"  @endisset>
                        </div>
                    </div>
                    
                   <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Beauty Tip image URL</label>
                            <input required="" type="text" name="bt_imageUrl" class="form-control" id="exampleInputEmail1" placeholder="BeautyTip image URL" @isset($beauty_tips->bt_imageUrl) value=" {{$beauty_tips->bt_imageUrl}}" @endisset>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">BeautyTip Short Description English</label>
                            <textarea rows="5" required  name="bt_short_description_english" class="form-control" id="exampleInputEmail1" placeholder="BeautyTip Short Description English">@isset($beauty_tips->bt_short_description_english){!! $beauty_tips->bt_short_description_english !!}@endisset</textarea>
                        </div>
                    </div> 

                     <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">BeautyTip Short Description Urdu</label>
                            <textarea style="text-align: right; direction: rtl;" rows="5"   name="bt_short_description_urdu" class="form-control" id="exampleInputEmail1" placeholder="BeautyTip Short Description Urdu">@isset($beauty_tips->bt_short_description_urdu){!! $beauty_tips->bt_short_description_urdu !!}@endisset</textarea>
                        </div>
                    </div>

                      <div class="col-sm-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">BeautyTips Keywords </label>
                            <textarea rows="2" required  name="bt_keywords" class="form-control" id="exampleInputEmail1" placeholder="BeautyTips Keywords">@isset($beauty_tips->bt_keywords){!! $beauty_tips->bt_keywords !!}@endisset</textarea>
                        </div>
                    </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Beauty Tip Description English</label>
                            <textarea required class="form-control" id="summary-ckeditor" name="bt_description">@isset($beauty_tips->bt_description){{$beauty_tips->bt_description}}@endisset</textarea>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Beauty Tip Description Urdu</label>
                            <textarea style="text-align: right; direction: rtl;" class="form-control" id="summary-ckeditor_urdu" name="bt_description_urdu">@isset($beauty_tips->bt_description_urdu) {{$beauty_tips->bt_description_urdu}} @endisset</textarea>
                            
                        </div>
                    </div>
                    
                </div>
                <input type="hidden" name="returnback" value="{{$returnback}}">

                <!-- /.box-body -->
                @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                @endif
                <div class="box-footer">
                    
                    @if($bt_id)
                        <input type="submit" name="save" class="btn btn-primary" value="Update">
                    @else
                        <input name="save" type="submit" class="btn btn-primary" value="Submit">
                    @endif
                   
                     <button style="margin-left: 10px;" type="reset" class="btn btn-primary">Reset</button>
                     @if(isset($add_btips_listingUrl))
                    <a style="margin-left: 10px;" href="{{ url($add_btips_listingUrl)}}" class="btn btn-primary" >Cancel</a>
                    @else
                        <!-- <a style="margin-left: 10px;" href="#" class="btn btn-primary" >Cancel</a> -->
                    @endif
                    @if(isset($add_btips_listingUrl))
                   <input type="hidden" name="add_btips_listingUrl" value="{{$add_btips_listingUrl}}">
                   @endif
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection

@push("script")

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>

<script type="text/javascript" src="{{asset('js/multi_selected.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">



            <script data-sample="1">
                CKEDITOR.replace( 'summary-ckeditor', {
                    height: 300,

                    // Configure your file manager integration. This example uses CKFinder 3 for PHP.
                    filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                    filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
                    filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                    filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images'
                } );
            </script>

            <script data-sample="2">
                CKEDITOR.replace( 'summary-ckeditor', {
                    extraPlugins: 'uploadimage,image2',
                    height: 300,

                    // Upload images to a CKFinder connector (note that the response type is set to JSON).
                    uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',

                    // Configure your file manager integration. This example uses CKFinder 3 for PHP.
                    filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                    filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
                    filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                    filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

                    // The following options are not necessary and are used here for presentation purposes only.
                    // They configure the Styles drop-down list and widgets to use classes.

                    stylesSet: [
                        { name: 'Narrow image', type: 'widget', widget: 'image', attributes: { 'class': 'image-narrow' } },
                        { name: 'Wide image', type: 'widget', widget: 'image', attributes: { 'class': 'image-wide' } }
                    ],

                    // Load the default contents.css file plus customizations for this sample.
                    contentsCss: [ CKEDITOR.basePath + 'contents.css', 'assets/css/widgetstyles.css' ],

                    // Configure the Enhanced Image plugin to use classes instead of styles and to disable the
                    // resizer (because image size is controlled by widget styles or the image takes maximum
                    // 100% of the editor width).
                    image2_alignClasses: [ 'image-align-left', 'image-align-center', 'image-align-right' ],
                    image2_disableResizer: true
                } );
            </script>


<script src="{{asset('ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('ckeditor/samples/js/sample.js')}}"></script>


<script>


    CKEDITOR.replace( 'summary-ckeditor' );

        $("form").submit( function(e) {
            var messageLength = CKEDITOR.instances['summary-ckeditor'].getData().replace(/<[^>]*>/gi, '').length;
            if( !messageLength ) {
                alert( 'Please enter BeautyTips Description' );
                    $("#summary-ckeditor").focus();
                    return false;
                e.preventDefault();
            }
        });


        CKEDITOR.replace( 'summary-ckeditor_urdu' );

        $("form").submit( function(e) {
            var messageLength = CKEDITOR.instances['summary-ckeditor_urdu'].getData().replace(/<[^>]*>/gi, '').length;
           
        });



    $(document).ready(function () {
    $('#multiselect').multiselect({
        buttonWidth: '160px',
        includeSelectAllOption: true,
        nonSelectedText: 'Select an Option' });

});

function getSelectedValues() {
    var selectedVal = $("#multiselect").val();
    for (var i = 0; i < selectedVal.length; i++) {var
        innerFunc = function innerFunc(i) {
            setTimeout(function () {
                location.href = selectedVal[i];
            }, i * 2000);
        };
        innerFunc(i);
    }
}
</script>

<script type="text/javascript">
    $("#btc_id").val("{{$btc_id}}");
</script>


<script>
    initSample();
</script>



@endpush