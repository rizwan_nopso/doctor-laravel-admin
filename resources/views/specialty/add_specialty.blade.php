@php
    
    use App\Helpers\Helper;

    $helper = new Helper();
    $display_cust_img = $helper->display_cust_img();

  
@endphp

@extends('service-mgmt.base')
@section('action-content')
<style type="text/css">
    .checkbox label:after, 
.radio label:after {
    content: '';
    display: table;
    clear: both;
}

.checkbox .cr,
.radio .cr {
    position: relative;
    display: inline-block;
    border: 1px solid #a9a9a9;
    border-radius: .25em;
    width: 1.3em;
    height: 1.3em;
    float: left;
    margin-right: .5em;
}

.radio .cr {
    border-radius: 50%;
}

.checkbox .cr .cr-icon,
.radio .cr .cr-icon {
    position: absolute;
    font-size: .8em;
    line-height: 0;
    top: 50%;
    left: 20%;
}

.radio .cr .cr-icon {
    margin-left: 0.04em;
}

.checkbox label input[type="checkbox"],
.radio label input[type="radio"] {
    display: none;
}

.checkbox label input[type="checkbox"] + .cr > .cr-icon,
.radio label input[type="radio"] + .cr > .cr-icon {
    transform: scale(3) rotateZ(-20deg);
    opacity: 0;
    transition: all .3s ease-in;
}

.checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
.radio label input[type="radio"]:checked + .cr > .cr-icon {
    transform: scale(1) rotateZ(0deg);
    opacity: 1;
}

.checkbox label input[type="checkbox"]:disabled + .cr,
.radio label input[type="radio"]:disabled + .cr {
    opacity: .5;
}
</style>

<section class="content" style="margin-top: -40px;">
  <div class="row">
        <div class="col-sm-12">
            <h2>Add Specialty</h2><br>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h2 class="">Specialty  </h2>
                </div>
                <div class="col-sm-4">
                    {{-- <a class="btn btn-info" href="">Add new Collection</a> --}}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                 @if (\Session::has('flash_message'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="padding: 23px !important; margin: -tio; margin-top: -12px; margin-right: -14px;"><span aria-hidden="true">&times;</span></button> 
                        {{ \Session::get('flash_message') }}
                    </div> 
                @endif
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            
            <div class="row" style="padding:20px;">
                @if(!empty($sp))
                                <form method="post" action="{{url('update_specialty'.'/'.$sp->sp_id)}}" enctype="multipart/form-data">
                            @else
                                <form method="post" action="{{url('store_specialty')}}" enctype="multipart/form-data">
                            @endif
                                {{csrf_field()}}

                                <div class="row clearfix">
                                   
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="sp_name" required="" class="form-control" placeholder="Name" @isset($sp) value="{{$sp->sp_name}}" @endisset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select name="sp_status" required="" id="sp_status" class="form-control">
                                                    <option value="">Please Select Status</option>
                                                    <option @isset($sp) @if($sp->sp_status == '1') selected="" @else @endif  @endisset value="1">Active</option>
                                                    <option @isset($sp) @if($sp->sp_status == '0') selected="" @else @endif  @endisset value="0">Inactive</option>
                                                </select>
                                              
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="file" name="sp_icon" class="form-control" placeholder="sp_icon" @if(isset($sp)) @else required="" @endif>
                                            </div>
                                        </div>
                                    </div>
                                 
                                
                               
                                <div class="col-lg-12 p-t-20 text-center">

                                    <button type="submit" class="btn btn-primary waves-effect m-r-15">
                                        @if(!empty($sp))
                                            Update
                                        @else
                                            Submit
                                        @endif
                                    </button>
                                    <button type="clear" class="btn btn-danger waves-effect">Cancel</button>
                                </div>
                            </form>
            </div>

            <div class="row" style="margin-top: 50px; padding: 20px;">
                
            </div>
        </div>
    </div>
</section>
@endsection
@push('script')


@endpush