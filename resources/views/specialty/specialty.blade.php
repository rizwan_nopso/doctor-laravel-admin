@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $display_salonImages = $helper->display_salonImages();
   
@endphp

@extends('service-mgmt.base')
@section('action-content')

<style type="text/css">
  .small-img {
      height: 80px;
      width: 80px;
      border-radius: 50%;
    }
</style>

<section class="content">
  <div class="row">
        <div class="col-sm-12">
            <h3>Specialty Detail</h3><br>
        </div>
    </div>


    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Specialty</h3>
                </div>
                <div class="col-sm-4">
                    <a class="btn btn-info" href="{{ url('add_specialty') }}">Add new Specialty</a> 
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <tbody><tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Icon</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    @if(count($sp)>0)
                    <?php foreach ($sp as $key => $value) {  $key += 1;  ?>
                        <tr>
                            <td>{{$key}}</td>
                            <td>
                             {{ $value->sp_name }}
                            </td>

                            <td>
                                @if($value->sp_icon == '')
                                    <img src="{{$display_salonImages}}/{{$value->sp_icon}}" width="80px" height="80">
                                @else
                                    <img src="{{$display_salonImages}}/no-image.png" width="80px" height="80">
                                @endif
                            </td>
                           
                            <td>
                              @if($value->sp_status == '1') Active @else Inactive @endif
                            
                            </td>
                            <td>
                                 <a href="{{url('edit_specialty'.'/'.$value->sp_id)}}" class="btn btn-info"><i class="glyphicon glyphicon-edit"></i></a>
                                    <a href="{{url('detele_specialty'.'/'.$value->sp_id)}}" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></a>  
                            </td>
                           
                        </tr>
                    <?php } ?>
                    @endif
                </tbody>
            </table>

         
            
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection