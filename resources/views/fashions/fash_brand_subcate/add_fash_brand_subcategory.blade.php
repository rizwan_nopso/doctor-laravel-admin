
@extends('service-mgmt.base')
@section('action-content')

@push("css")
  <link rel="stylesheet" type="text/css" href="{{asset('css/dropzone.css')}}">
    <style type="text/css">
        input[type="file"]
            {
                /*color: transparent;*/
            }
        .checkbox label:after, 
        .radio label:after {
            content: '';
            display: table;
            clear: both;
        }

        .checkbox .cr,
        .radio .cr {
            position: relative;
            display: inline-block;
            border: 1px solid #a9a9a9;
            border-radius: .25em;
            width: 1.3em;
            height: 1.3em;
            float: left;
            margin-right: .5em;
        }

        .radio .cr {
            border-radius: 50%;
        }

        .checkbox .cr .cr-icon,
        .radio .cr .cr-icon {
            position: absolute;
            font-size: .8em;
            line-height: 0;
            top: 50%;
            left: 20%;
        }

        .radio .cr .cr-icon {
            margin-left: 0.04em;
        }

        .checkbox label input[type="checkbox"],
        .radio label input[type="radio"] {
            display: none;
        }

        .checkbox label input[type="checkbox"] + .cr > .cr-icon,
        .radio label input[type="radio"] + .cr > .cr-icon {
            transform: scale(3) rotateZ(-20deg);
            opacity: 0;
            transition: all .3s ease-in;
        }

        .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
        .radio label input[type="radio"]:checked + .cr > .cr-icon {
            transform: scale(1) rotateZ(0deg);
            opacity: 1;
        }

        .checkbox label input[type="checkbox"]:disabled + .cr,
        .radio label input[type="radio"]:disabled + .cr {
            opacity: .5;
        }
        #checked_featured{
            display: none;
        }
        #blah{
            display: none;
        }
    </style>
@endpush

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Add Fashion Brand Collection</h3><br>
        </div>
    </div>
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Add Brand Collection</h3>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {{-- <form id="addform" action="" method="POST" role="form" enctype="multipart/form-data"> --}}
            <form id="addform" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1"> Name</label>
                            <input required="" type="text" name="fbc_name" class="form-control" id="exampleInputEmail1" placeholder="Enter brand collection name" value="@isset($fbc) @if($fbc->fbc_name) {{$fbc->fbc_name}} @endif @endisset">
                        </div>
                    </div>
                    <div class="col-sm-6">
                         <div class="form-group">
                            <label for="exampleInputPassword1">Select Fashion Collection</label>
                            <select name="fc_id" class="form-control" id="cat_name">
                                 @foreach($fc as $value)
                                  <option value="{{$value->fc_id}}"> {{$value->fc_title}} </option>
                                 @endforeach 
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Status</label>
                            <select name="fbc_status" class="form-control">
                                <option value="1" @isset($fbc) @if($fbc->fbc_status == '1') selected="checked" @endif @endisset>Active</option>
                                <option value="0" @isset($fbc) @if($fbc->fbc_status == '0') selected="checked" @endif @endisset>In Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Fashion Brand Collection URL</label>
                            <input required="" type="text" name="fbc_link" class="form-control" id="exampleInputEmail1" placeholder="Enter Posts URL" value="@isset($fbc) @if($fbc->fbc_link) {{$fbc->fbc_link}} @endif @endisset">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Gender</label>
                            <select class="form-control" name="fbc_gender" id="fbc_gender">
                                <option value="1" @isset($fbc) @if($fbc->fbc_gender == '1') selected="selected" @endif @endisset>Male</option>
                                <option value="2" @isset($fbc) @if( $fbc->fbc_gender == '2') selected="selected" @endif @endisset>Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Icon Image</label>
                            <input type="file" name="fbc_image" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="checkbox">
                          <label style="font-size: 1.5em">
                              <input type="checkbox" value="0" name="fbc_featured" id="fb_featured" onclick="$(this).attr('value', this.checked ? 1 : 0)" >
                              <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                              Featured  <span style="font-size: 16px !important;" id="checked_featured">[Recommended image size 350px x 150px]</span>
                          </label> 
                      </div>
                  </div>

                    <div class="col-sm-12">
                      <div class="form-group gallery">
                        <label>Brand Collection Images</label>
                          <div class="form-group">      
                          <div class="form-group dropzone" id="dropzone">
                          </div>       
                          </div>
                      </div>
                    </div>
                </div>
                

                <!-- /.box-body -->
                @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                @endif
                <div class="box-footer">
                    <button type="submit" id="btn_submit" class="btn btn-primary">Submit</button>
                   
                     <button style="margin-left: 10px;" type="reset" class="btn btn-primary">Reset</button>
                     
                    <a style="margin-left: 10px;" href="{{$returnback}}" class="btn btn-primary" >Cancel</a>
                    
                </div>  

                <input type="hidden" name="fbc_id" id="fbc_id">
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>

    
@endsection

@push('script')

<link rel="stylesheet" type="text/css" href="{{asset('css/dropzone.css')}}">
<script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>


<script type="text/javascript">
    $('#addform').submit(function(e){
      if (!myDropzone.files || !myDropzone.files.length) {
          alert("Please Insert Images!");
          return false;
        }
        var button = $('#btn_submit');
        button.prop('disabled', true);

        e.preventDefault();
        formData=$(this).serialize();
        //alert('data got ');    
          $.ajax({
            type:"POST",
            url: "{{url('/admin/store_fashbrandcollection'.'/'.$id)}}",
          data:new FormData(this),
          contentType:false,
          cache:false,
          processData:false,
          success:function(data)
          {
            document.getElementById("fbc_id").value = data;
            // alert(data);
            //document.getElementById('hidden_one').value = data;
            myDropzone.processQueue();
          }
        });
      });

       if(typeof Dropzone != 'undefined')
       {
           Dropzone.autoDiscover = false;
           var myDropzone = new Dropzone("#dropzone", {
             url: "{{url('/admin/add_fbc_images/')}}/"+$("#fbc_id").val(),
             autoProcessQueue:false,
             required:true,
             acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
             addRemoveLinks: true,
             maxFiles:30,
             parallelUploads : 100,
             maxFilesize:10,
             init: function() {
                // Update the total progress bar
               
                this.on("totaluploadprogress", function(progress) {
                   // document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
                 });

                 this.on("queuecomplete", function(progress) {
                   // document.querySelector("#total-progress").style.opacity = "0";
                 });

            this.on("sending", function(file, xhr, formData)
             {
              // document.querySelector("#total-progress").style.opacity = "1";

              formData.append("fbc_id",$("#fbc_id").val()); 
              formData.append('_token', '{{ csrf_token() }}'); 
            
             });

               this.on('success', function(file, responseText)
                {                   

                });

             this.on("complete", function (file)
                         {
                      if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) 
                          {    

                             // swal({
                             //  title: 'Property Updated',
                             //  type:'success'
            
                             // }).then(function(){
                              window.location.href='{{url('admin/fashionBrand')}}';
                             // });    

                          }
                        });   
            }

        });
      };


</script>

<script type="text/javascript">
  $('#fb_featured').click(function() {
            if( $(this).is(':checked')) {
                $("#checked_featured").show();
            } else {
                $("#checked_featured").hide();
            }
        }); 

    // // add color images in databse 
    // $('#ajax-upload').on('submit', function(e){
    //     e.preventDefault();
    //     var form = e.target;
    //     var data = new FormData(form);
    //     // alert(data);
    //     $.ajax({
    //       url: form.action,
    //       method: form.method,
    //       processData: false,
    //       contentType: false,
    //       data: data,
    //       processData: false,
    //       success: function(data){
    //         // $('#result').text(data)
    //         document.getElementById("fbc_id").value = data;
    //         myDropzone.processQueue();
    //       }
    //     });
    //   });

    //    if(typeof Dropzone != 'undefined')
    //    {
    //        Dropzone.autoDiscover = false;
    //        var myDropzone = new Dropzone("#dropzone", {
    //         alert(myDropzone);
    //          url: "{{url('admin/add_fbc_images/')}}",
    //          method:"POST",
    //          autoProcessQueue:false,
    //          required:true,
    //          acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
    //          addRemoveLinks: true,
    //          maxFiles:100,
    //          parallelUploads : 100,
    //          maxFilesize:5,
    //          init: function() {
    //             // Update the total progress bar
    //             this.on("totaluploadprogress", function(progress) {
    //                // document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
    //              });
    //              this.on("queuecomplete", function(progress) {
    //                // document.querySelector("#total-progress").style.opacity = "0";
    //              });
    //             this.on("sending", function(file, xhr, formData)
    //              {
    //               formData.append("fbc_id",document.getElementById('fbc_id').value); 
    //               formData.append('_token', '{{ csrf_token() }}'); 
    //              });

    //            this.on('success', function(file, responseText)
    //             {                   
    //                 // alert(responseText);
    //             });
    //          this.on("complete", function (file)
    //                      {
    //                         // alert(file);
    //                   if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) 
    //                       {   
    //                         alert("image saved successfully");
    //                         location.reload();
    //                       }
    //                     });   
    //         }

    //     });
    //   };



</script>

@endpush
