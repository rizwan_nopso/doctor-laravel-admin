
@extends('service-mgmt.base')
@section('action-content')

@push("css")
  <link rel="stylesheet" type="text/css" href="{{asset('css/dropzone.css')}}">
    <style type="text/css">
        input[type="file"]
            {
                /*color: transparent;*/
            }
        .checkbox label:after, 
        .radio label:after {
            content: '';
            display: table;
            clear: both;
        }

        .checkbox .cr,
        .radio .cr {
            position: relative;
            display: inline-block;
            border: 1px solid #a9a9a9;
            border-radius: .25em;
            width: 1.3em;
            height: 1.3em;
            float: left;
            margin-right: .5em;
        }

        .radio .cr {
            border-radius: 50%;
        }

        .checkbox .cr .cr-icon,
        .radio .cr .cr-icon {
            position: absolute;
            font-size: .8em;
            line-height: 0;
            top: 50%;
            left: 20%;
        }

        .radio .cr .cr-icon {
            margin-left: 0.04em;
        }

        .checkbox label input[type="checkbox"],
        .radio label input[type="radio"] {
            display: none;
        }

        .checkbox label input[type="checkbox"] + .cr > .cr-icon,
        .radio label input[type="radio"] + .cr > .cr-icon {
            transform: scale(3) rotateZ(-20deg);
            opacity: 0;
            transition: all .3s ease-in;
        }

        .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
        .radio label input[type="radio"]:checked + .cr > .cr-icon {
            transform: scale(1) rotateZ(0deg);
            opacity: 1;
        }

        .checkbox label input[type="checkbox"]:disabled + .cr,
        .radio label input[type="radio"]:disabled + .cr {
            opacity: .5;
        }
        #checked_featured{
            display: none;
        }
        #blah{
            display: none;
        }
    </style>
@endpush

<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <h3>Add Fashion Brand Collection</h3><br>
        </div>
    </div>
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Add Brand Collection</h3>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {{-- <form id="addform" action="" method="POST" role="form" enctype="multipart/form-data"> --}}
            <form id="addform" method="post" action="{{url('admin/updatedBrand_collection'.'/'.$fbc->fbc_id.'/'.$fb_id)}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="returnback" value="{{$returnback}}">
                <div class="box-body">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1"> Name</label>
                            <input required="" type="text" name="fbc_name" class="form-control" id="exampleInputEmail1" placeholder="Enter brand collection name" @isset($fbc) @if($fbc->fbc_name) value="{{$fbc->fbc_name}}" @endif @endisset>
                        </div>
                    </div>
                    <div class="col-sm-6">
                         <div class="form-group">
                            <label for="exampleInputPassword1">Select Fashion Collection</label>
                            <select name="fc_id" class="form-control" id="fc_id">
                                 @foreach($fc as $value)
                                  <option value="{{$value->fc_id}}"> {{$value->fc_title}} </option>
                                 @endforeach 
                            </select>
                        </div>
                    </div>
                    @push('script')
                        <script type="text/javascript">
                            $("#fc_id").val("{{$fc_id}}");
                        </script>
                    @endpush
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Status</label>
                            <select name="fbc_status" class="form-control">
                                <option value="1" @isset($fbc) @if($fbc->fbc_status == '1') selected="checked" @endif @endisset>Active</option>
                                <option value="0" @isset($fbc) @if($fbc->fbc_status == '0') selected="checked" @endif @endisset>In Active</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Fashion Brand Collection URL</label>
                            <input required="" type="text" name="fbc_link" class="form-control" id="exampleInputEmail1" placeholder="Enter Posts URL" @isset($fbc) @if($fbc->fbc_link) value="{{$fbc->fbc_link}}" @endif @endisset>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Gender</label>
                            <select class="form-control" name="fbc_gender" id="fbc_gender">
                                <option value="1" @isset($fbc) @if($fbc->fbc_gender == '1') selected="selected" @endif @endisset>Male</option>
                                <option value="2" @isset($fbc) @if( $fbc->fbc_gender == '2') selected="selected" @endif @endisset>Female</option>
                                {{-- <option value="3" @isset($fbc) @if( $fbc->fbc_gender == '3') selected="selected" @endif @endisset>Both</option> --}}
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Icon Image</label>
                            <input type="file" name="fbc_image" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="checkbox">
                          <label style="font-size: 1.5em">
                              <input type="checkbox" value="0" name="fbc_featured" id="fb_featured" onclick="$(this).attr('value', this.checked ? 1 : 0)" >
                              <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                              Featured  <span style="font-size: 16px !important;" id="checked_featured">[Recommended image size 350px x 150px]</span>
                          </label> 
                      </div>
                  </div>

                    
                </div>
                

                <!-- /.box-body -->
                @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                @endif
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                   
                     <button style="margin-left: 10px;" type="reset" class="btn btn-primary">Reset</button>
                     
                    <a style="margin-left: 10px;" href="{{$returnback}}" class="btn btn-primary" >Cancel</a>
                    
                </div>  

                <input type="hidden" name="fbc_id" id="fbc_id">
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>

    
@endsection

@push('script')

<link rel="stylesheet" type="text/css" href="{{asset('css/dropzone.css')}}">
<script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>


<script type="text/javascript">
   


</script>

<script type="text/javascript">
  $('#fb_featured').click(function() {
            if( $(this).is(':checked')) {
                $("#checked_featured").show();
            } else {
                $("#checked_featured").hide();
            }
        }); 

</script>

@endpush
