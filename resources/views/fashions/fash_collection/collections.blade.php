@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $fb_image_path = $helper->fashion_image_display();
  
@endphp

@extends('service-mgmt.base')
@section('action-content')

<section class="content">
  <div class="row">
        <div class="col-sm-12">
            <h3>Collections</h3><br>
        </div>
    </div>
    <div style="margin-bottom: 30px;">
        <form method="get" action="{{ url('admin/fashionCollection') }}">
            <div style="margin:0 auto; width:100%; height:50px;" class="div1">
                <div style="margin:0 auto; height:50px;" class="div1">
                     <div class="row">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;">
                        <div class="form-group">
                          <label>
                            Title
                          </label>
                          <input placeholder="Faction Collection Title"  type="text" class="form-control" value="@isset($fc_title) {{$fc_title}}  @endisset" name="fc_title" maxlength="128">
                        </div>
                      </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="padding-right: 0px;">
                      <div class="form-group">
                        <label>
                            Status
                        </label>
                        <select class="form-control" name="fc_status" id="fc_status">
                          <option value='all_active'@isset($fc_status) @if($fc_status == "all_active") selected="checked" @endif @endisset >All</option>
                          <option value='1'@isset($fc_status) @if($fc_status == 1) selected="checked" @endif @endisset>Yes</option>
                          <option value='2'@isset($fc_status) @if($fc_status == 2) selected="checked" @endif @endisset>No</option>
                        </select> 
                      </div>
                    </div>
                    
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-right: 0px;">
                      <div class="form-group">
                        <label></label>
                        <button style="padding:5px; margin-top: 27px; margin-left: 5px; margin-right: 5px; width: 100px;"  class="btn btn-info" >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>       
                </div>
            </div>
        </form>
    </div>

       {{--  @if(!empty($service_cat) OR !empty($is_active))
                    @php

                      if(is_null($service_cat))
                        {
                          $service_cat = "";
                        } 
                        if(is_null($is_active))
                        {
                          $is_active = "";
                        }
                        $urls = ('servicecategories?'.'service_cat'.'='.$service_cat.'&'.'is_active'.'='.$is_active.'&'.'page='.$categories->currentPage());

                    @endphp
                  @else
                    @php
                       $urls = ('servicecategories?'.'page='.$categories->currentPage());
                    @endphp
                @endif --}}


    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    <h3 class="box-title">Categories</h3>
                </div>
                <div class="col-sm-4">
                    <a class="btn btn-info" href="{{ url('admin/addFashCollection') }}">Add new Collection</a>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <tbody><tr>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Gender</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    <?php foreach ($fc as $key => $value) { ?>
                        <tr>
                            <td><?php echo $value->fc_title ?></td>
                            <td><img src="{{$fb_image_path}}{{$value->fc_image}}" width="100px" height="100px" ></td>
                            <td>
                              @if($value->fc_gender == 1)
                                Male
                              @elseif($value->fc_gender == 2)
                                Female
                              @elseif($value->fc_gender == 3)
                                Both
                              @endif
                            </td>
                            <td>
                              @if($value->fc_status == 1)
                                Active
                              @elseif($value->fc_status == 0)
                                Inactive
                              @endif
                            </td>
                            
                            <td>
                             <a href="{{route('update_collection',['id' => $value->fc_id])}}" class="btn btn-info"><i class="glyphicon glyphicon-edit"></i></a> 
                                <a style="margin-left: 10px;" href="{{route('deleteCollection',['id' => $value->fc_id])}}" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></a> 
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
             {!! $fc->appends(['fc_title'=>isset($fc_title)?$fc_title:'','fc_status'=>isset($fc_status)?$fc_status:'','page'=>$fc->currentPage()])->links() !!} 
            {{-- {!! $fc->links() !!} --}}
            
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection