@php
    use App\Helpers\Helper;

    $helper = new Helper();
    $pc_image_path = $helper->pc_image_display();
  
@endphp

@extends('service-mgmt.base')
@section('action-content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<link rel="stylesheet" type="text/css" href="{{asset('css/multiple_selected.css')}}">
<style type="text/css">
    .filter-option{
        border:1px solid #ccc !important;
    }
    .btn-group{
        width: 100% !important;
    }
    .multiselect{
        width: 100% !important;
    }
    .multiselect-container{
    background-color: rgb(234, 234, 234) !important;
    height: 200px !important;
    overflow-y: scroll !important;
    scroll-behavior: smooth !important;
    width: 100% !important;
    }
    .multiselect-all{
        display: none;
    }
    .content-header{
        margin-top: 50px !important;
    }
</style>
<section class="content" style="">
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-sm-8">
                    {{-- <h3 class="box-title">Add new post</h3> --}}
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('toasts'))
                      @foreach(Session::get('toasts') as $toast)
                        <div class="alert alert-{{ $toast['level'] }}">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                          {{ $toast['message'] }}
                        </div>
                      @endforeach
                    @endif
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            @if(isset($fc))
                 <form action="{{route('updatedCollection',['id' => $fc->fc_id])}}" method="POST" role="form" enctype="multipart/form-data">
                  
            @else
                <form action="{{route('store_factionColletion')}}" method="POST" role="form" enctype="multipart/form-data">
            @endif
                {{ csrf_field() }}
                <input type="hidden" name="return_back" value="">
                <div class="box-body">
                    <div class="col-sm-6">
                         <div class="form-group">
                            <label for="exampleInputPassword1">Title</label>
                            <input required="" type="text" placeholder="Fashion Collection " class="form-control" name="fc_title" @isset($fc->fc_title) value="{{$fc->fc_title}}" @endisset>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Status</label>
                            <select name="fc_status" class="form-control" required="">
                                <option value="1" @isset($fc) @if($fc->fc_status == '1') selected="checked" @endif  @endisset>Active</option>
                                <option value="0" @isset($fc) @if($fc->fc_status == '0') selected="checked" @endif  @endisset>Inactive</option>
                            </select>
                        </div>
                    </div> 
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Gender</label>
                            <select name="fc_gender" class="form-control" required="">
                                <option value="1" @isset($fc) @if($fc->fc_gender == '1') selected="checked" @endif  @endisset>Male</option>
                                <option value="2" @isset($fc) @if($fc->fc_gender == '2') selected="checked" @endif  @endisset>Female</option>
                                <option value="3" @isset($fc) @if($fc->fc_gender == '3') selected="checked" @endif  @endisset>Both</option>
                            </select>
                        </div>
                    </div> 
                    <div class="col-sm-6">
                         <div class="form-group">
                            <label for="exampleInputPassword1">Title</label>
                            <input @if(isset($fc)) @else required="" @endif type="file" placeholder="Fashion Collection " class="form-control" name="fc_image">
                        </div>
                    </div>
                   
                </div>
                <input type="hidden" name="returnback" value="">
                @if(Session::has('flash_message'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                @endif
                <div class="box-footer">
                    
                    @if(isset($pv->pv_id))
                        <input type="submit" name="save" class="btn btn-primary" value="Update">
                    @else
                        <input name="save" type="submit" class="btn btn-primary" value="Submit">
                    @endif
                    <input name="save" type="reset" class="btn btn-primary" value="Clear">
                    <a style="margin-left: 10px;" href="{{$returnback}}" class="btn btn-primary" >Cancel</a>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
</section>
@endsection

@push("script")
<link rel="stylesheet" type="text/css" href="{{asset('css/dropzone.css')}}">
<script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>


<script type="text/javascript">
    // $('#addform').submit(function(e){

    //     var button = $('#submit');
    //     button.prop('disabled', true);

    //     e.preventDefault();
    //     formData=$(this).serialize();
    //     //alert('data got ');    
    //       $.ajax({
    //         type:"POST",
    //         url: "{{url('/addproperty/')}}",
    //       data:new FormData(this),
    //       contentType:false,
    //       cache:false,
    //       processData:false,
    //       success:function(data)
    //       {
    //         document.getElementById("record_id").value = data;
    //         // alert(data);
    //         //document.getElementById('hidden_one').value = data;
    //         myDropzone.processQueue();
    //       }
    //     });
    //   });

    //    if(typeof Dropzone != 'undefined')
    //    {
    //        Dropzone.autoDiscover = false;
    //        var myDropzone = new Dropzone("#dropzone", {
    //          url: "{{url('/add_property_images/')}}",
    //          autoProcessQueue:false,
    //          required:true,
    //          acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
    //          addRemoveLinks: true,
    //          maxFiles:8,
    //          parallelUploads : 100,
    //          maxFilesize:5,
    //          init: function() {
    //             // Update the total progress bar
               
    //             this.on("totaluploadprogress", function(progress) {
    //                document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
    //              });

    //              this.on("queuecomplete", function(progress) {
    //                document.querySelector("#total-progress").style.opacity = "0";
    //              });

    //         this.on("sending", function(file, xhr, formData)
    //          {
    //           document.querySelector("#total-progress").style.opacity = "1";

    //           formData.append("data",$("#record_id").val()); 
    //           formData.append('_token', '{{ csrf_token() }}'); 
            
    //          });

    //            this.on('success', function(file, responseText)
    //             {                   

    //             });

    //          this.on("complete", function (file)
    //                      {
    //                   if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) 
    //                       {    

    //                          swal({
    //                           title: 'Property Updated',
    //                           type:'success'
            
    //                          }).then(function(){
    //                           window.location.href='{{url('/')}}';
    //                          });    

    //                       }
    //                     });   
    //         }

    //     });
    //   };

</script>



@endpush